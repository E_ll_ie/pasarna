import sys
from collections import defaultdict
from sklearn import tree
from sklearn.ensemble import AdaBoostRegressor
import numpy as np
from scipy.spatial.distance import cdist
from matplotlib import pyplot as plt
import logging

import bayesloop as bl
import gc
import const
logging.basicConfig(level=logging.WARNING)
lbf = logging.getLogger("basic-filter")
mbf = logging.getLogger("inside-bayes")

def basic_filters(changepoints, covs, profile, ex_end, upRegionLength = 100, upRegionThresh = 5):
    updated = []
    counter  = -1
    log = False

    for site_ind in range(len(changepoints)):

        site = changepoints[site_ind]
        flag = True
        counter +=1
        if site < 50 :
            flag = False
            lbf.debug("<50 {}".format(site))
            continue
        ####PartA
        if const.PARTA:
            dist_to_next = 100
            if counter == 0:
                #len(covs[site : site + dist_to_next])
                #len(covs[max(0, site - dist_to_next) : site]):
                if sum(covs[site : site + dist_to_next])/len(covs[site : site + dist_to_next]) > \
                sum(covs[max(0, site - dist_to_next) : site])/len(covs[max(0, site - dist_to_next) : site]):
                    lbf.debug("First one Bad one {}".format(site))
                    flag = False
                    counter -= 1
                    continue
                elif sum(covs[site : site + 20]) > sum(covs[site - 20 : site]) \
                and sum(covs[site-20 : site ]) > sum(covs[site - 40 : site - 20]):
                    lbf.debug("First one1 Bad one {}".format(site))
                    flag = False
                    counter -= 1
                    continue
        #if max(profile[max(0,site-10) : site+10]) < 3:
        #    flag = False
        ### partA
        if site < 50 :
            flag = False
            lbf.debug("<50 {}".format(site))
            continue

        nonz = [float(i) for i in covs[max(0,site-100):site] if i > 0]

        #if sum(covs[site-20:site]) < 1:
        #    flag = False
        #    if log:
        #        print(site, "upstream zero",sum(covs[site-20:site]) )
        #    continue
        if len(nonz) == 0:
            lbf.debug("nonZeroes {}".format(site))
            flag = False
            continue

        elif sum(nonz)/len(nonz) < 1.5:
            lbf.debug("nonZeroes 2 {}".format(site))
            flag = False
            continue
        '''
        if max(covs[ : site]) < 5:
            flag = False
            lbf.debug("Low Max Cov Upstream {} with max {}".format(site, max(covs[ : site])))
            continue

        if max(covs[max(0, site - 200) : site]) < 5:
            flag = False
            lbf.debug("Low Max Cov 200 Upstream {} with max {}".format(site, max(covs[max(0, site - 200) : site])))
            continue

        if  sum(covs[: site]) < upRegionThresh :
            flag = False
            lbf.debug("Sum of Upstream Cov {}".format(site))
            continue


        if sum(covs[max(0, site-upRegionLength): site])/upRegionLength < upRegionThresh/upRegionLength :
            flag = False
            lbf.debug("Sum of Interval Upstream Cov {}".format(site))
            continue
        '''
        #if np.count_nonzero(covs[max(0, site-upRegionLength): site] == 0) > 0.90 * upRegionLength:
        #    flag = False
        #    lbf.debug("Number of Zeroes1 {}".format(site))
        #    continue

        if site < ex_end:
            if np.count_nonzero(covs[: site]== 0) > 0.90 * site:
                flag = False
                lbf.debug("Number of Zeroes2 {}".format(site))
                continue
        else:
            if np.count_nonzero(covs[ex_end: site]== 0) > 0.90 * (site-ex_end):
                flag = False
                lbf.debug("Number of (intron)Zeroes1 {}".format(site))
                continue

        '''
        #if abs(1.0*(sum(covs[site-50: site ]) - sum(covs[site:site+50])))/ (sum(covs[site:site+50])+1) < 0.05 :
        #    flag = False
        #    lbf.debug("No Change in Trend {}".format(site))
        #    continue
        if 1.0*(sum(covs[site-50: site ]) - sum(covs[site:site+50]))/ (sum(covs[site:site+50])+1) < -0.1 :
            flag = False
            lbf.debug("Increasing Trend {}".format(site))
            continue
        '''
        if flag:
            updated.append(site)
    return updated


def merge_consec_changepoints_simple(changepoints, strand, minDist = 100):
    if strand == "+":
        changepoints = sorted(changepoints)
    else:
        changepoints = sorted(changepoints, reverse=True)
    blocks = []
    updated = []
    if len(changepoints) > 1:
        pre = changepoints[0]
        for site in changepoints[1:]:
            if abs(site-pre) < minDist:
                blocks.append(pre)
            pre = site

        for site in changepoints:
            if site not in blocks:
                updated.append(site)
        return updated
    else:
        return changepoints

def merge_consec_changepoints_simpleboth(changepoints_1, changepoints_2, strand, minDist = 200):
    if strand == "+":
        changepoints_1 = sorted(changepoints_1)
        changepoints_2 = sorted(changepoints_2)
    else:
        changepoints_1 = sorted(changepoints_1, reverse=True)
        changepoints_2 = sorted(changepoints_2, reverse=True)


    blocks = []
    updated = []

    if len(changepoints_1) >=1 and len( changepoints_2) >=1 :
        for site1 in changepoints_1:
            for site2 in changepoints_2:
                if abs(site1-site2) < minDist:
                    downstream = max(site1, site2)
                    downstream = int(0.5*(site1+site2))
                    if downstream not in updated:
                        updated.append(downstream)
                    break
    #else:
    #    for site1 in changepoints_1:
    #        updated.append(site1)
    #    for site2 in changepoints_2:
    #        updated.append(site2)

    return merge_consec_changepoints_simple(updated, strand)


def merge_consec_changepoints(changepoints, covs, maxMergeDist = 100,
                                                         regSizeforRatio = 100):


    if len(changepoints) <= 1:
        return changepoints
    temp_filtered_ones = set([changepoints[0]])
    filtered_ones = []
    pre = changepoints[0]
    toberemoved = []

    for site in changepoints[1:]:
        newsite = site
        if site-pre < 2*maxMergeDist and site-pre > 0:
            incflag = []
            decflag = []
            # Upstream - Downstream
            siteratio = sum(covs[site - regSizeforRatio : site]) - sum(covs[site : site + regSizeforRatio])
            preratio =  sum(covs[pre - regSizeforRatio : pre]) - sum(covs[pre : pre + regSizeforRatio])

            # Coverage
            sitevalue = covs[site]
            prevalue = covs[pre]

            if (siteratio > 0 and preratio > 0) :
                decflag.append(1)
                incflag.append(0)
            elif (siteratio < 0 and preratio < 0):
                incflag.append(1)
                decflag.append(0)

            elif sitevalue != 0 :
                if siteratio < 0 and preratio > 0 and site-pre< 2*maxMergeDist :
                    decflag.append(0)
                    incflag.append(1)
                elif siteratio > 0 and preratio < 0 and site-pre< 2*maxMergeDist:
                    decflag.append(1)
                    incflag.append(0)
                elif abs(100*(sitevalue - prevalue)/sitevalue)< 10:
                    decflag.append(1)
                    incflag.append(0)
                else:
                    decflag.append(0)
                    incflag.append(0)
            else:
                decflag.append(0)
                incflag.append(0)
            #newsite = int(0.5*(site+pre))
            if sum(decflag) >= sum(incflag):
                newsite = site
                toberemoved += [pre]

            elif sum(incflag) > sum(decflag):
                newsite = pre
        #for passite in passites:
        #    if abs(newsite-passite) < 50:
        #        toberemoved += [newsite]
        #        newsite = passite
        #        break
        pre = newsite
        temp_filtered_ones.add(newsite)
    for site in temp_filtered_ones:
        if site not in toberemoved:
            filtered_ones.append(site)
    return filtered_ones


def merge_changepoints_sets():
    merged_sites_info = defaultdict(list)
    merged_sites = []
    for expr in filtered_ones:
        for this_s in filtered_ones[expr]:
            if this_s not in merged_sites:
                merged_sites.append(this_s)
            merged_sites_info[this_s].append(expr)
    return merged_sites, merged_sites_info

def find_closest(alist, target):
    return min(alist, key=lambda x:abs(x-target))

def connect_points(points_s1, points_s2, maxDist = 100):
    """
    This just connect the closest ones
    If we are lucky they all match
    if not the remaining ones should be kept
    """
    pairs = []
    remaining_2 = []
    remaining_1 = []
    used_1 = []
    for p2 in points_s2:
        p1 = find_closest(points_s1, p2)
        if abs(P2-P1) < maxDist :
            pairs.append([p1, p2])
            used_1.append(p1)
        else:
            remaining_2.append(p2)
    for p1 in points_s1:
        if p1 not in used_1:
            remaining_1.append(p1)

    return pairs, remaining_1, remaining_2



def connect_peaks_and_points(points, peaks, segment_length):
    """
    This adds up the peak wights which are associate with the same change_points
    """
    points = [-1] + points + [segment_length]
    percs = {}
    for p_ind in range(1,len(points)):
        for mp in peaks:
            if mp < points[p_ind] and mp > points[p_ind-1]:
                if points[p_ind] not in percs:
                    percs[points[p_ind]] = 0
                percs[points[p_ind]] += mp.weight

    return percs

def use_bayes(signal):

    print("we are in changestudy")
    maxx = max(signal)
    signal = 100*signal//maxx

    ## Scenario 1
    '''
    S = bl.ChangepointStudy()
    S.load(np.array(signal[::10]))
    L = bl.om.Poisson('signal', bl.oint(min(signal), max(signal), 1000))
    T = bl.tm.ChangePoint('change_point', 'all')
    S.set(L,T, silent=True)
    S.fit(silent=True)
    param, prob = S.getHPD('change_point')
    onechangepoint, prob = np.argmax(prob)*10, max(prob)
    print("1", bayes, onechangepoint, prob)
    '''
    ## Scenario 2
    print("Scenario2")
    changepoints2 = []
    S = bl.ChangepointStudy(silent=True)
    S.load(np.array(signal[::10]), silent=True)
    L = bl.om.Poisson('signal',bl.oint(min(signal), max(signal), 1000))
    T = bl.tm.CombinedTransitionModel(bl.tm.ChangePoint('t_1', 'all'),
                                  bl.tm.ChangePoint('t_2', 'all'))
    S.set(L,T, silent=True)
    S.fit(silent=True)
    a = S.getJHPD(['t_1', 't_2'])[2]
    maxes = np.unravel_index(np.argmax(a, axis=None), a.shape)
    print( "jhpd", maxes,a[maxes])

    param1, prob1 = S.getHPD('t_1')
    onechangepoint1, prob1 = np.argmax(prob1)*10, max(prob1)
    print("2", "bayes", onechangepoint1, prob1)
    param2, prob2 = S.getHPD('t_2')
    onechangepoint2, prob2 = np.argmax(prob2)*10, max(prob2)
    print("2", "bayes", onechangepoint2, prob2)
    changepoints2 = [onechangepoint1, onechangepoint2]
    '''
    ##Scenario 3
    changepoints3 = []
    S = bl.ChangepointStudy()
    S.load(np.array(signal[::10]))
    L = bl.om.Poisson('signal', bl.oint(min(signal), max(signal), 1000))
    T = bl.tm.CombinedTransitionModel(bl.tm.ChangePoint('t_1', 'all'),
                                  bl.tm.ChangePoint('t_2', 'all'),
                                  bl.tm.ChangePoint('t_3', 'all'))
    S.set(L,T, silent=True)
    S.fit(silent=True)
    param1, prob1 = S.getHPD('t_1')
    onechangepoint1, prob1 = np.argmax(prob1)*10, max(prob1)
    print("3", "bayes", onechangepoint1, prob1)
    param2, prob2 = S.getHPD('t_2')
    onechangepoint2, prob2 = np.argmax(prob2)*10, max(prob2)
    print("3", "bayes", onechangepoint2, prob2)
    param3, prob3 = S.getHPD('t_3')
    onechangepoint3, prob3 = np.argmax(prob3)*10, max(prob)
    print("3", "bayes", onechangepoint3, prob3)
    changepoints3 = [onechangepoint1, onechangepoint2, onechangepoint3]
    '''
    return changepoints2


#Used value for log10Min -100 chosen experimentally
#Threshold_ratio (between 0 and 1) to filter small peaks (seens at the end) ~0.2
def findChangepointsByBl(coverage,log10pMin = -100 ,thresh_ratio = 0.001):

    mbf.debug("Inside Bayesian")
    factor = 1
    #print("inside bayes")
    if max(coverage) == 0:
        return []

    maxx = max(coverage)
    coverage = 100*coverage//maxx

    #if len(coverage) >= 10000:
    #    factor  = 1
    #    coverage = np.array(list(coverage)[::factor])


    S = bl.Study(silent=True)
    S.load(coverage, silent=True)
    L = bl.om.Poisson('coverage', bl.oint(min(coverage), max(coverage), 1000))
    #T = bl.tm.RegimeSwitch('log10pMin', log10pMin)
    trycounter = 0
    while True:
        try:
            T = bl.tm.RegimeSwitch('log10pMin', log10pMin)
            S.set(L,T, silent=True)
            S.fit(silent=True)
            break
        except IndexError:
            if trycounter == 3:
                return []
                break
            log10pMin = log10pMin/2
            trycounter += 1


    inp = S.getParameterMeanValues('coverage')

    '''
    #nonzeros = np.argwhere(inp_diff )
    tpc = []
    for nz in nonzeros:
        tpc.append(nz[0])

    #print(tpc)
    return tpc
    '''
    inp_diff = np.diff(inp,n=1)
    nonzeros = np.argwhere(np.abs(inp_diff)>thresh_ratio)
    mbf.debug("After Batesian Model {}".format(nonzeros))
    neg_indexes = np.empty(0)		#To hold indexes with a negative jump
    pos_indexes = np.empty(0)		#To hold indexes with a positive jump
    vals = np.empty(0)				#To hold the total negative jump height
    neg_diff_sum = 0
    changepoint_index = list()
    last_index = -1
    last_negative_ind = -1
    #print("tight before")
    ###return [oo[0] for oo in nonzeros]
    if len(nonzeros) > 0:
        for i in nonzeros:
            if last_index>0:
                if inp_diff[i]>0 and inp_diff[last_index]<0:
                    neg_indexes = np.append(neg_indexes,last_index+1)
                    pos_indexes = np.append(pos_indexes,i+1)
                    vals = np.append(vals,neg_diff_sum)
                    neg_diff_sum=0;

            if inp_diff[i]<0:
                neg_diff_sum = neg_diff_sum + inp_diff[i]
                last_negative_ind = i
            last_index = i
        if last_negative_ind > 0:
            pos_indexes = np.insert(pos_indexes,0,0)
            neg_indexes = np.append(neg_indexes,last_negative_ind+1)
            vals = np.append(vals,neg_diff_sum)
            out = np.zeros(inp.shape)
            neg_indexes =list(map(int,neg_indexes))
            pos_indexes = list(map(int,pos_indexes))
            sum_total = np.sum(inp)

            for n,i in enumerate(neg_indexes):
                sum = np.sum(inp[pos_indexes[n]:i])
                if sum/sum_total>thresh_ratio or abs(out[i])/max(abs(out))>thresh_ratio:
                	changepoint_index.append(i*factor)
    gc.collect()
    #print("tight after")
    return changepoint_index



###############################################################################
###############################################################################
###############################################################################
def evaluate_changepoints(changepoints, covs, startnt):
    merged_sites  = []
    merged_sites_info = defaultdict(list)
    for expr in changepoints:
        for this_s in changepoints[expr]:
            if this_s not in merged_sites:
                merged_sites.append(this_s)
            merged_sites_info[this_s].append(expr)
    merged_sites = sorted(merged_sites)
    temp_filtered_ones = [merged_sites[0]]
    filtered_ones = []
    pre = merged_sites[0]
    toberemoved = []
    for site in merged_sites:
        newsite = site
        tempinfo = merged_sites_info[site]
        if site-pre < 50 and site-pre > 0:
            incflag = []
            decflag = []
            for expr in covs:
                siteratio = sum(covs[expr][site-startnt-50:site-startnt]) - sum(covs[expr][site-startnt:site-startnt+50])
                preratio =  sum(covs[expr][pre-startnt-50:pre-startnt]) - sum(covs[expr][pre-startnt:pre-startnt+50])
                sitevalue = covs[expr][site]
                prevalue = covs[expr][pre]


                if (siteratio > 0 and preratio > 0):
                    decflag.append(1)
                elif (siteratio < 0 and preratio < 0):
                    incflag.append(1)
                elif abs(100*(sitevalue - prevalue)/sitevalue)< 0.05:
                    decflag.append(1)
                else:
                    decflag.append(0)
                    incflag.append(0)
            #newsite = int(0.5*(site+pre))
            if sum(decflag) > sum(incflag):
                newsite = site
                toberemoved += [pre]
                tempinfo += merged_sites_info[pre]

            elif sum(incflag) > sum(decflag):
                newsite = pre
                tempinfo += merged_sites_info[newsite]
        #for passite in passites:
        #    if abs(newsite-passite) < 50:
        #        toberemoved += [newsite]
        #        newsite = passite
        #        break
        pre = newsite
        temp_filtered_ones.append(newsite)
        merged_sites_info[newsite] = tempinfo
    for site in temp_filtered_ones:
        if site not in toberemoved:
            filtered_ones.append(site)

    return filtered_ones, merged_sites_info


def find_the_steepdrop(listofnums, gap):
    """

    I look for the maximum slop
    """
    maxslop = -100
    slopes = []
    for k in range(len(listofnums)-gap):
        slopes.append((k, listofnums[k] - listofnums[k + gap]))
    slopes = sorted(slopes, key = lambda x : x[1], reverse=True)
    return [i[0] for i in slopes]


def make_sure_about_last_one(start, end, strand, profile):
    drops = []
    #I'm just going the other way and focusing on the last one here"
    if strand == "+":
        real_start = start
    elif strand =="-":
        real_start = -1*end
    clf3 = AdaBoostRegressor(tree.DecisionTreeRegressor(max_depth=3), n_estimators = 10)
    clf = clf3.fit(np.expand_dims(range(len(profile)), axis = 1), profile[::-1])
    Ytree = clf.predict(np.expand_dims(range(len(profile)), axis = 1))
    pre = Ytree[0]
    pre_pos = 0
    for pos in range(1,len(Ytree)):
        if Ytree[pos] != pre:
            drops.append((pos, abs(pos - pre_pos), Ytree[pos]))
            pre = Ytree[pos]
            pre_pos = pos
    final_drops = sorted(drops, key = lambda x: x[2])[:2]
    if len(final_drops) == 2:
        if abs(1 - final_drops[0][1]/final_drops[1][1]) < 0.3 and abs(final_drops[0][2]/final_drops[1][2] -1) < 0.1:
            final_drop = min(final_drops[0][0], final_drops[1][0])
            return abs(real_start+ (end - start - final_drop))
        elif abs(final_drops[0][2]/final_drops[1][2] -1) < 0.3 :
            if final_drops[0][0] < final_drops[1][0]:
                pre = Ytree[final_drops[0][0]]
                for k in Ytree[final_drops[0][0]+1: final_drops[1][0]]:
                    if k > 1.2* pre:
                        return abs(real_start+ end - start - final_drops[0][0])
        return abs(real_start+ end - start - final_drops[1][0])
    elif len(final_drops) == 1:
        return abs(real_start+ end - start - final_drops[0][0])
    else:
        return None






def group_the_drops(drops, maxDistPos = 100):
    if len(drops) == 0:
        return []
    if len(drops) == 1:
        return drops[0][0]
    else:
        pre_pos = drops[0][0]
        pre_ratio = drops[0][1]
        for drop in drops:
            pos = drops[0]
            ratio = drops[1]
            if (pos - pre_pos)< maxDistPos:
                if abs((pre_ratio*100/pre_pos) - 100) < 20:
                    blocked.append((pre_pos, pre_ratio))
            pre_pos = drop[0]
            pre_ratio = drop[1]
        for drop in drops:
            if drop not in blocked:
                updated_drops.append(drop[0])

    return updated_drops



def find_only_lastone(start, end, strand, profiles):
    """
    This function currently marks drops, slops and minimums.
    There is a threshold or maxmimun number for the numbers
    """
    if strand == "+":
        real_start = start
    elif strand =="-":
        real_start = -1*end
    #keep candidates if there is no obvious one
    minns = []
    drops = []
    maxslops = []
    # afterbefore ratio way
    """
    Minns keep the all after to all before ratios
    We stop as soon as we reach a threshold right now
    There is also a number of positions that we keep.
    """
    """
    CONST_AFTER_BEFORE_STOP_RATIO = 0.1
    CONST_AFTER_BEFORE_RATIO_NUMS  = 5
    beforemean = 0
    aftermean = sum(profiles)
    ratios = []
    for k in range(len(profiles)):
        beforemean += profiles[k]
        aftermean -= profiles[k]
        if beforemean != 0:
            ratios.append((k,aftermean/beforemean))
            if aftermean/beforemean < CONST_AFTER_BEFORE_STOP_RATIO:
                break
    ratios = sorted(ratios, key = lambda x: x[1])[:CONST_AFTER_BEFORE_RATIO_NUMS]

    minns = ratios
    """
    """
    We can also use a ratio from decision tree
    We have to choose a maximum depth for the tree
    Also we need to know how many to use.
    """
    CONST_AFTER_BEFORE_TREE_NUMS = 5
    # I'm also trying the decision tree thing
    clf3 = AdaBoostRegressor(tree.DecisionTreeRegressor(max_depth=2), n_estimators = 10)
    clf = clf3.fit(np.expand_dims(range(len(profiles)), axis = 1), profiles)
    Ytree = clf.predict(np.expand_dims(range(len(profiles)), axis = 1))
    pre = Ytree[0]
    for pos in range(len(Ytree)):
        if Ytree[pos] != pre:
            drops.append((pos, (Ytree[pos]+1)/(pre+1)))
            pre = Ytree[pos]
    drops = sorted(drops, key = lambda x: x[1])[:CONST_AFTER_BEFORE_TREE_NUMS]

    """
    I also find the drops to find the steepest one
    Seems like I'm keeping two right now.
    """
    """
    CONST_STEEPDROP_NUM = 5
    # Last thing is to try the slope
    maxslops = [stdrop for stdrop in find_the_steepdrop(profiles, 10)[:CONST_STEEPDROP_NUM]]
    """
    return minns, drops, maxslops

def compare_changepoints_withdrops(tempcps, minns, drops, maxslopes):
    """
    We have a list of change point candidates
    These change points are being compared with a list of drops
    And if they're close enough to one of the drops, they're being kept.
    I also have minn values and max slopes but right now there not being used
    """
    #TODO:
    """

    see how we can use the other factors.
    """
    MAXDISTFROMDROPS =  100
    keepflag = False
    newcps = []
    for cp in tempcps:
        keepflag = False
        dropslist = []
        for expr in drops:
            dropslist += drops[expr]
        for minnpos in dropslist:
            if abs(cp - minnpos[0]) < MAXDISTFROMDROPS:
                keepflag = True
        if keepflag:
            newcps.append(cp)
    return newcps

def merge_candidates(these_minns, these_drops, these_slopes):
    filteredlayer1 = defaultdict(list)
    filteredlayer2 = defaultdict(list)
    filteredlayer3 = defaultdict(list)
    filteredlayer4 = defaultdict(list)
    finalpos = {}
    for expr in these_minns:
        finalpos[expr] =-1



        for minnpos in these_minns[expr]:
            for droppos in these_drops[expr]:
                if abs(minnpos[0] - droppos[0]) < 200:
                    #filteredlayer1[expr].append(int((minnpos[0]+droppos[0])/2))
                    filteredlayer1[expr].append(max(minnpos[0],droppos[0]))
        for filpos in filteredlayer1[expr]:
            for sloppos in these_slopes[expr]:
                if abs(filpos - sloppos) < 200:
                    #filteredlayer2[expr].append(int((filpos+sloppos)/2))
                    filteredlayer2[expr].append(max(filpos,sloppos))
        for minnpos in these_minns[expr]:
            for sloppos in these_slopes[expr]:
                if abs(minnpos[0] - sloppos) < 200:
                    #filteredlayer3[expr].append(int((minnpos[0]+sloppos)/2))
                    filteredlayer3[expr].append(max(minnpos[0],sloppos))

        for droppos in these_drops[expr]:
            for sloppos in these_slopes[expr]:
                if abs(droppos[0] - sloppos) < 200:
                    #filteredlayer4[expr].append(int((droppos[0]+sloppos)/2))
                    filteredlayer4[expr].append(max(droppos[0],sloppos))


        if len(filteredlayer2[expr]) >= 0 and len(filteredlayer1[expr]) > 0:
            finalpos[expr] = max(filteredlayer1[expr])
        #elif len(filteredlayer2[expr]) > 0:
        #    finalpos[expr] = max(filteredlayer2[expr])
        elif len(filteredlayer2[expr]) == 0 and len(filteredlayer1[expr]) == 0 \
                            and len(filteredlayer3[expr]) > 0 :
            finalpos[expr] = max(filteredlayer3[expr])
        elif len(filteredlayer2[expr]) == 0 and len(filteredlayer1[expr]) == 0 \
                            and len(filteredlayer4[expr]) > 0 :
            finalpos[expr] = max(filteredlayer4[expr])
        elif len(these_minns[expr]) > 0:
            finalpos[expr] = max([x[0] for x in these_minns[expr]])
        elif len(these_drops[expr]) > 0:
            finalpos[expr] = max([x[0] for x in these_drops[expr]])
        else:
            finalpos[expr] = max(these_slopes[expr])


    return finalpos
