import re
import const
import pybedtools
from collections import defaultdict
import subprocess
from joblib import Parallel, delayed
import numpy as np
'''
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Convolution1D, MaxPooling1D
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.advanced_activations import PReLU
from keras.layers.normalization import BatchNormalization
'''
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn import linear_model
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from scipy import interp

from sklearn.metrics import precision_recall_curve
from sklearn.metrics import f1_score
from sklearn.metrics import auc
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
import h5py


global graph

class CNN_MODEL():
    def __init__(self):
        self.model = None
        self.graph = None

    def parse_thiscov_forgene_bigwig_insideloop(self, region, covfile, line_ind):
        fds = str(region).split()
        region = fds[0] + " " + fds[1] + " " + fds[2]
        genename = fds[3]
        this_command = "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/bigWigSummary " +  str(covfile)  + " " + region  + " " +  "1" + " 2> templog"
        output = str(subprocess.Popen(this_command, shell=True, stdout=subprocess.PIPE).stdout.read(), "utf-8").strip()
        if "n/a" in output:
            output = output.replace("n/a", "0")
        if len(output) < 1:
            temp = 0
        else:
            temp = int(float(output))
        if line_ind % 2000 == 0:
            print(line_ind)
        return  (genename,temp)

    def extract_readcounts_around_knownsites(self, bedfiledir, covfilesdir):

        coveredsites = set()
        bedfile = pybedtools.example_bedtool(bedfiledir)
        sbedfile = bedfile.sort()
        extendedbedfile = sbedfile.slop(b=200, g=const.chrsize)
        notpolyA = pybedtools.example_bedtool("/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/nonhuman.PAS.polyADB.bed")
        #notpolyA = extendedbedfile.shuffle(g=const.chrsize, excl=extendedbedfile, chrom=True, noOverlapping = True)
        upstreambedfile = sbedfile.slop(l=200, r = 0 ,s=True, g=const.chrsize)
        downstreambedfile = sbedfile.slop(l=0, r = 200 ,s=True, g=const.chrsize)
        if True:
            for sample in covfilesdir:
                print(" We are getting initial read counts for known sites")
                thiscovfile = covfilesdir[sample][0]
                #covbams = pybedtools.example_bedtool(thiscovfile)
                #dscovs =  covbams.coverage(downstreambedfile, s=True, counts = True)
                #uscovs =  covbams.coverage(upstreambedfile, s=True, counts = True)
                counter = 0
                templist = Parallel(n_jobs=8, backend="multiprocessing", mmap_mode="r+", temp_folder="/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/joblibtemp/")(
                          delayed(unwrap_parse_thiscov_forgene_bigwig_insideloop)((self, region, thiscovfile, line_ind))
                          for line_ind, region in enumerate(upstreambedfile))

                for reg, count in templis:
                    if count > 0:
                        coveredsites.add(reg)
                    counter +=1
                    if counter % 10000:
                        print(counter, "init coverage")
            print(sample, " coverages were calculated!")


            used = ""
            notused = ""
            for line in extendedbedfile:
                fds = str(line).strip().split()
                if fds[3] in coveredsites:
                    used += line + "\n"
                else:
                    notused += line + "\n"

            usedbed = pybedtools.BedTool(used, from_string=True)
            notusedbed = pybedtools.BedTool(notused, from_string=True)


            usedseqs = usedbed.sequence(fi=genomefa, s=True, name=True, tab=True)
            ntusedseqs = notusedbed.sequence(fi=genomefa, s=True, name=True, tab=True)
            notpolyaseqs = notpolyA.sequence(fi=genomefa, s=True, name=True, tab=True)


        return (usedseqs, notusedseq, notpolyaseqs)

    def polya_data_pipeline(self, sites_ext):
            conved_seq = self.onehot_withcons(sites_ext)
            return np.array(conved_seq)

    def sep_bychr(self, bedfile, num):
            tests = []
            trains = []
            for line in bedfile:
                    fds = line.split()
                    if fds[0].split(":")[0] in  ["chr3", "chr5", "chr7", "chr9"]:
                            tests.append(line)
                    else:
                            trains.append(line)
            test = self.polya_data_pipeline(tests)
            test_y = np.array([[num] for k in range(len(test))])
            train = self.polya_data_pipeline(trains)
            train_y = np.array([[num] for k in range(len(train))])
            return  train, train_y, test, test_y


    #def make_a_model(usedseqs, notusedseq, notpolyaseqs):
    def make_a_model(self, bedfiledir, bamfiledirs):
            usedseqs, notusedseq, notpolyaseqs = self.extract_readcounts_around_knownsites(bedfiledir, bamfiledirs)
            print("Sequence files were generated")
            NUM = 10000
            target_file = usedseqs[:NUM]
            target_train, target_train_y, target_test, target_test_y = \
            self.sep_bychr(target_file, 1)

            ntarget_file = notpolyaseqs[:NUM]
            ntarget_train, ntarget_train_y, ntarget_test, ntarget_test_y = \
            self.sep_bychr(ntarget_file, 0)

            train_x = np.concatenate((target_train,ntarget_train),axis=0)
            test_x = np.concatenate((target_test,ntarget_test),axis=0)


            train_y = np.concatenate((target_train_y,ntarget_train_y),axis=0)
            test_y = np.concatenate((target_test_y,ntarget_test_y),axis=0)

            with h5py.File('thisX-poly.h5', 'w') as hf:
                    hf.create_dataset('trainX', data=train_x)
                    hf.create_dataset('testX', data=test_x)

            with h5py.File('thisy-poly.h5', 'w') as hf:
                    hf.create_dataset('trainy',  data=train_y)
                    hf.create_dataset('testy',  data=test_y)



            in_file_X = h5py.File('thisX-poly.h5', 'r')
            train_X = in_file_X.get('trainX')
            test_X = in_file_X.get('testX')

            in_file_y = h5py.File('thisy-poly.h5', 'r')
            train_y = in_file_y.get('trainy')
            test_y = in_file_y.get('testy')




            self.train_by_conv_3(train_X, train_y, test_X, test_y)


    def one_hot_bed(self, seq, leng = 200):
            if len(seq) < 200:
                seq = seq + "N"*(200-len(seq))
            seq = seq[:leng]
            if len(seq) == leng:
                    seqlist=[]
                    for ind in range(len(seq)):
                            l = seq[ind]
                            score = 1
                            if l=="a" or l=="A":
                                    seqlist.append([score,0,0,0])
                            elif l=="c" or l=="C":
                                    seqlist.append([0,score,0,0])
                            elif l=="g" or l=="G":
                                    seqlist.append([0,0,score,0])
                            elif l=="t" or l=="T":
                                    seqlist.append([0,0,0,score])
                            elif l=="n" or l=="N":
                                    seqlist.append([0,0,0,0])
            return seqlist


    def onehot_withcons(self, regions):
            convedseq = []
            counter = 0
            for temp in regions:
                    counter += 1
                    this_seq = temp.split()[1]
                    for start_ind in range(50,150,10):
                            this_seq_c = this_seq[start_ind:start_ind+200]
                            coded_seq = self.one_hot_bed(this_seq_c, this_seq_c)
                            convedseq.append(coded_seq)
                    if counter % 1000 == 0:
                            print(counter)
            return convedseq



    def train_by_conv_3(self, X_train, y_train, X_test, y_test):
            print(X_train.shape, X_test.shape, y_train.shape, y_test.shape)
            self.model = Sequential()
            self.model.add(Convolution1D(input_shape=(200, 4),
                                    filters=200,
                                    kernel_size=11,
                                    padding="valid",
                                    activation="relu",
                                    strides=1))
            self.model.add(MaxPooling1D(strides=2, pool_size=2))
            self.model.add(Convolution1D(input_shape=(190*100, 4),
                                    filters=200,
                                    kernel_size=5,
                                    padding="valid",
                                    activation="relu",
                                    strides=1))
            self.model.add(MaxPooling1D(strides=4, pool_size=4))
            self.model.add(Flatten())
            self.model.add(Dense(input_dim=2*18996*4*25, units=1))
            self.model.add(Activation('sigmoid'))
            print('compiling model')
            #print(model.summary())
            self.model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=["accuracy"])
            print('running at most 5 epochs')
            checkpointer = ModelCheckpoint(filepath="cnnmodel_1.hdf5", verbose=0, save_best_only=True)
            earlystopper = EarlyStopping(monitor='val_loss', patience=10, verbose=1)
            self.model.fit(X_train, y_train, batch_size=100, epochs=5, shuffle=True, validation_split=0.1 , callbacks=[checkpointer,earlystopper])

            tresults = self.model.evaluate(X_test, y_test)
            y_prob = self.model.predict_proba(X_test)
            y_pred = self.model.predict(X_test)
            precision, recall, thresholds = precision_recall_curve(y_test, y_prob)
            prauc = sklearn.metrics.auc(recall, precision)
            ap = average_precision_score(y_test, y_prob)
            rocauc = roc_auc_score(y_test, y_prob)
            print('roc_auc=%.3f pr_auc=%.3f ap=%.3f' % (rocauc, prauc, ap))

            print(tresults)


    def load_a_model(self,modelfile):
        print("We are loading a pre-built cnn model")
        self.model = load_model(modelfile)
        self.model._make_predict_function()
        graph = theano.get_default_graph()
        print("Loading Done!")

    def test_by_trained_model(self, seqs):
        seqlist = []
        #print(seqs)
        for seq in seqs:
            seq_subs = [seq[50:250], seq[100:300], seq[150:350], seq[200:400], seq[250:450]]
            for ss in seq_subs:
                seq_in_num = self.one_hot_bed(ss)

                seqlist.append(seq_in_num)
        score = self.model.predict(np.array(seqlist))

        return score


def one_hot_bed(seq, leng = 200):
        if len(seq) < 200:
            seq = seq + "N"*(200-len(seq))
        seq = seq[:leng]
        if len(seq) == leng:
                seqlist=[]
                for ind in range(len(seq)):
                        l = seq[ind]
                        score = 1
                        if l=="a" or l=="A":
                                seqlist.append([score,0,0,0])
                        elif l=="c" or l=="C":
                                seqlist.append([0,score,0,0])
                        elif l=="g" or l=="G":
                                seqlist.append([0,0,score,0])
                        elif l=="t" or l=="T":
                                seqlist.append([0,0,0,score])
                        elif l=="n" or l=="N":
                                seqlist.append([0,0,0,0])
        return seqlist

def load_a_model(modelfile):
    from keras.models import load_model
    from keras import backend as K
    import theano as to

    print("We are loading a pre-built cnn model")
    thismodel = load_model(modelfile)
    #thismodel._make_predict_function()
    #graph = to.get_default_graph()
    print("Loading Done!")
    return thismodel

def test_by_trained_model(thismodel, seqs, seqp):
    seqlist = []
    #print(seqs)
    for seq in seqs:
        if seqp == 20:

            seq_subs = [seq[50:250], seq[60:260], seq[70:270], seq[80:280], seq[90:290],
                        seq[100:300], seq[110:310], seq[120:320], seq[130:330], seq[140:340],
                        seq[140:350], seq[160:360], seq[170:370], seq[180:380], seq[190:390],
                        seq[200:400], seq[210:410], seq[220:420], seq[230:430], seq[240:440]]
            #seq_subs = [seq[50:250], seq[100:300], seq[150:350], seq[200:400], seq[250:450]]
        elif seqp == 3:
            seq_subs = [seq[100:300], seq[150:350], seq[200:400]]
        for ss in seq_subs:
            seq_in_num = one_hot_bed(ss)

            seqlist.append(seq_in_num)

    #print(np.array(seqlist).shape)
    #with graph.as_default():
    score = thismodel.predict(np.array(seqlist), verbose=0)
    #print(score)
    #K.clear_session()
    return score

def unwrap_parse_thiscov_forgene_bigwig_insideloop(arg, **kwarg):
    return CNN_MODEL.parse_thiscov_forgene_bigwig_insideloop(*arg, **kwarg)
