

chrsize = "/cbcl/forouzme/Projects/PolyA/Analysis2/Data/human19.chr.size"
genomefa = "/cbcl/forouzme/Projects/PolyA/Analysis2/Data/human19.fa"

PAS = ['AATAAA','ATTAAA','AAGAAA','AAAAAG','AATACA','TATAAA','ACTAAA','AGTAAA','GATAAA'\
,'AATATA','CATAAA','AATAGA']
known = ['TGT[AT]','[ACT][CT]TTT[CT]T|TCTT','[TC]CA[TC]','GGGA','CC[AT][AT][ACT]CC|CC[CT][CT]CC[ACT]',\
'TGGG[AG]A[AGT]','A[AG]AAGA','G[AG]A[CG]G[AG]|ACGCGCA|GAAGAAC|AGGACAGAGC',\
'TGTGT|GTGTT|GTTGT/GTGTA'] #['TGTA']
nm = ['CFI', 'PTBP1', 'NOVA', 'hnRNPF', 'PCBP1,PCBP2', 'ESRP2', 'RABPN1', 'SRSFs', 'CstF64']
motifs = [PAS+known, PAS+known, PAS+known, PAS+known, PAS+known, PAS+known]

fullgenedectree=False
fulltreechangepoint=False
CP_METHOD = "Binary"
PR_COLORS = {"sample1" : "darkcyan" , "sample2" : "olive"}
showflag = False
GENEC = 500

currentpath = "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/"


##profiling
GENEXT = 10000

TESTR = False
VRB = 0

EVAL_MIN = 500
TRIMTRESH = 0
SAMPLES = []


## Junctions
SJMAXDIST = 100
SJRATIO = 1

### Segment process
BASICFILT = True
PARTA = False


MERGCONSEC = True

KNOWNSITEADD = True
KNOWNSITEADJUST = True
KNOWNSJADJUST = True
USEAAA = False
SEQCHECK = True
SEQCHECKTHRESH = 0.8 #0.8


### detect_round
DOUBLEDETECT = True
DETECTTHRESH = 0

#analyze_sites
CT_ONESEG = 0 #20
CT_ALLSEG = 0 #10

#Merge two sites
MERGEMINDIST = 100

#Merge with known polyA site
MERGEKNOWNDIST = 100

MERGEMINDISTREG = 100
MERGEMINDISTPRE = 100

UTRREGIONS = False

inBoth = True
inBoth_OneSampLastEx_PERC = 0.5
inBoth_OneSampUpEx_PERC = 0.9 # 0.9


GENEBATCH = 10
NUMCORES = 48
