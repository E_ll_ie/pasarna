## This is for analyzing AAA reads
from itertools import islice
import sys
import string
from collections import defaultdict #this will make your life simpler
from itertools import groupby
from collections import Counter
import pybedtools
import const

def fasta_iter(fasta_name):
    """
    given a fasta file. yield tuples of header, sequence
    """
    d=dict()
    fh=fasta_name
    faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
    for header in faiter:
        header = header.next().split()[0][1:].strip()
        seq = "".join(s.strip() for s in faiter.next())
        d[header]=seq
    return d



def trim_readsforAAA(readfile, trimmedfile):
    N = 4
    outfile = open(trimmedfile,'w')
    badcount = 0
    goodcount = 0
    with open(readfile,'r') as f:
        while True:
            next_n_lines = list(islice(f, N))
            if not next_n_lines:
                break
            else:
                seq = next_n_lines[1].split('\n')[0];
                idx = seq.find('AAAAAAAAAAAAAAA')
                if(idx == -1):
                    badcount += 1
                elif(idx > 24):
                    trimseq = seq[0:idx];
                    name = next_n_lines[0].split(' ')[0] #+"_"+seq[0:4];
                    l3 = next_n_lines[2].split('\n')[0];
                    l4 = next_n_lines[3].split('\n')[0][0:idx];
                    goodcount+=1
                    outfile.write(name+"\n"+trimseq+"\n"+l3+"\n"+l4+"\n");
                else:
                    badcount += 1;
    print(goodcount, "reads had AAAA, which is ", goodcount*100/(goodcount+badcount),
          " percent of the reads in this sample!")

def remove_internal_priming(mappedbed, ref, mappedbedfiltered):

    outfile = open(sys.argv[2],'w')
    badcount = 0;
    goodcount = 0
    complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'U':'A'}
    ref = open(ref,'r')

    genome=fasta_iter(ref)

    for line in open(mappedbed, 'r'):
        chrom, start, stop, gene, score, strand = line.strip().split('\t')[:6]
        stop=int(stop)-1
        start=int(start)-1
        if strand == '+':
            L=min(10, len(genome[chrom])-int(stop))
            sequence = str(genome[chrom][int(stop)+1:int(stop)+L+1]).upper()
        else:
            L= min(10, start)
            n=str(genome[chrom][int(start)-L:int(start)]).upper()
            sequence = "".join(complement.get(base, base) for base in reversed(n))

        sequence = sequence.upper()
        if(sequence.find('AAAAAA')>=0):
            badcount += 1;
        elif(sequence.count('A')>=7):
            badcount +=1
        else:
            goodcount +=1
            outfile.writelines(line);
    print(goodcount, "reads had real AAAA tail, which is ", goodcount*100/(goodcount+badcount),
          " percent of the reads in the ones with AAA!")
    outfile.close()




def extend_genes(gfiledir, gtempdir, gextfile):
    with open(gfiledir, 'r') as gfile:
        for line in gfile:
            temp = "\t".join(line.split()[:6])
            gtempfile.write(temp + "\n")
    gtempfile.close()

    ginitfile = pybedtools.example_bedtool(gtempdir)
    sginitfile = ginitfile.sort()
    extensiondict = {}
    #find closest genes downstream
    closestdown = sginitfile.closest(sginitfile, iu=True, io=True, s=True, D="a", d=True)
    with open(gextfile, "w") as gef:
        for pair in closestdown:
            gene1chrom, gene1start, gene1end, gene1name, gene1info, gene1strand,
            gene2chrom, gene2start, gene2end, gene2name, gene2info, gene2strand, dist = pair.split()

            if gene2chrom != "." :
                dist = min(dist, 5000)
            else:
                dist = 5000
            extensiondict[gene1name] = dist
            if gene1strand == "+":
                extgene = "\t".join(gene1chrom, gene1start, str(int(gene1end) + dist),
                            gene1name, gene1info, gene1strand )
            else:
                extgene = "\t".join(gene1chrom, str(int(gene1start)-dist), gene1end,
                            gene1name, gene1info, gene1strand)
            gef.write(extgene+"\n")


    return entensiondict


def use_AAA_reads(mappedbedfiltered, bedwithcounts, gextfiledir):
    '''
    You need to first find these AAA reads
    Reads with tails
    separate tails
    map
    look for internal priming
    keep the end pos
    Lets say we have a bed file here
    '''
    mappedbedfilteredfile = open(mappedbedfiltered, 'r')
    initlistofreads = []
    for line in mappedbedfilteredfile:
        chrom, start, stop, gene, score, strand = line.strip().split('\t')[:6]
        if strand == "+" :
            this_read = chrom + strand + stop
        else:
            this_read = chrom + strand + start
        initlistofreads.append(this_read)


    listwithcounts = Counter(initlistofreads)
    counter = -1
    with open(bedwithcounts, "w") as bwc:
        for pos in listwithcounts:
            counter += 1
            if "+" in pos:
                chrm, loc = pos.split("+")
                newline = "\t".join([chrm, loc, str(int(loc)+1), "AAAread_" + str(counter), str(listwithcounts[pos]), "+"] )
            else:
                chrm, loc = pos.split("-")
                newline = "\t".join([chrm, loc, str(int(loc)+1), "AAAread_" + str(counter), str(listwithcounts[pos]), "-" ])
            bwc.write(newline+"\n")

    ainitfile = pybedtools.example_bedtool(bedwithcounts)
    sainitfile = ainitfile.sort()
    ginitfile = pybedtools.example_bedtool(gextfiledir)
    sginitfile = ginitfile.sort()
    # Intersection of aaas and genes
    aaa_on_g = sainitfile.intersect(sginitfile, wo=True, s=True)
    aaadict = defaultdict(dict)
    for pair in aaa_on_g :
        a1chrom, a1start, a1end, a1name, a1info, a1strand, gene2chrom, gene2start, gene2end, \
                                      gene2name, gene2info, gene2strand, dist = str(pair).split()
        if gene2strand == "+":
            this_aa_pos = int(a1start) - int(gene2start)
            this_aa_freq = int(a1info)
            if gene2name not in aaadict:
                aaadict[gene2name] = {}
            aaadict[gene2name][this_aa_pos] =  this_aa_freq

    return aaadict

def aaa_process(mappedbedfiltered, geneextdir):
    allaaadict = defaultdict(dict)
    for sample in const.SAMPLES:
        allaaadict[sample] = defaultdict(dict)
        #print(mappedbedfiltered[sample])
        # # TODO:
        # Think about the replicates
        allaaadict[sample] = use_AAA_reads(mappedbedfiltered[sample][0], mappedbedfiltered[sample][0] + ".u.txt", geneextdir)

    return allaaadict
