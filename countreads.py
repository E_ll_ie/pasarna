import sys
from collections import defaultdict
import const
def change_coord_to_rel(site, start, end, strand):
    if strand == "+":
        rel_site = site - start
    else:
        rel_site = end - site

    return rel_site

# This should be done after site annotation
# For each gene
def analyze_sites(sites, profiles, ct = 0):

    spt = set()
    counts = defaultdict(dict)
    num_nums = 0
    for segid in sites:
        if len(sites[segid]) > 0:
            num_nums += len(sites[segid])
            spt.add(segid)

    spt = list(spt)
    if len(spt) == 1:
        these_counts = count_read_basedonsites_lastexon_m1_scaledbyl(sites[spt[0]], profiles[spt[0]], ct=const.CT_ONESEG)
        for sitep in these_counts:
            counts[sitep] = these_counts[sitep]
    elif len(spt) >= 2:
        sub_sites = []
        for segid in spt:
            sub_sites = sites[segid]

            these_counts = count_read_basedonsites_lastexon_m1_scaledbyl(sub_sites, profiles[segid], ct=const.CT_ONESEG)
            for sitep in these_counts:
                counts[sitep] = these_counts[sitep]

    blacklist = []
    onlysites = []
    summ_s = {}
    summ_p = {}
    numm_p = {}
    ratiotable = defaultdict(dict)
    samples = const.SAMPLES
    for sample in samples:
        summ_s[sample] = 1
    #print("counts", counts)
    if len(counts.keys()) > 0 :
        for site_info in counts:
            site = site_info[1]
            ratiotable[site_info] = {}
            for sample in samples:
                summ_s[sample] += counts[site_info][sample]

        for site_info in counts:
            site = site_info[1]
            dis_site = site_info[0]
            for sample in samples:
                ratiotable[site_info][sample] = counts[site_info][sample]*100/summ_s[sample]
                if site_info not in summ_p:
                    summ_p[site_info] = 0
                    numm_p[site_info] = (0,0)
                summ_p[site_info] += ratiotable[site_info][sample]
                numm_p[site_info] = (counts[site_info]["sample1"], counts[site_info]["sample2"])

            ## TODO
            ##print("inanalyze", site_info, summ_p[site_info])
            if summ_p[site_info] < const.CT_ALLSEG or min(numm_p[site_info]) < 500 :
                blacklist.append(site_info)

                #new_counttables[site_info] =  counttables[site]
        for b in blacklist:
            del counts[b]

    #counttables_up = {}
    #ccounts = counts.values()
    #ccounts_sorted = sorted(ccounts, reverse = True)[:4]
    for csite in counts:
        #if counts[csite] > ccounts_sorted[-1]:
        #    counttables_up[csite] = counttables[csite]
        onlysites.append(csite[0])
    #if num_nums > 0:
    #    print("analyze",num_nums, len(onlysites))
    return onlysites, counts

###Most Basic####
def count_forexon_scaledbyl_justonesite(site, profile, exon_end, pos_tp, interval=False, interval_len = 200):
    """
    ----|------|-----|---
      A     B     C    D
    A/LA - B/LB
    B/LB - C/LC
    C/LC - D/LD
    Here I want to assign the reads upstream of each site to that site
    Of course it's upstream of the site to the previous one - down stream to next one
    In this function I do not put any limit on the interval around the site.

    I assume these sites are relative to start of this segment
    """
    count = 0

    if pos_tp == "Last" or pos_tp == "Upstream":
        pre = 0
    elif pos_tp == "Intron":
        pre = exon_end
    if not interval:
        relative_counts  = sum(profile[pre:site])/(0.001*(max(1,len(profile[pre:site])))) - sum(profile[site:])/(0.001*(max(1,len(profile[site:]))))
    elif interval:
        relative_counts  = sum(profile[max(0,site-interval_len):site])/(0.001*(interval_len)) - sum(profile[site:site+interval_len])/(0.001*(len(profile[site:interval_len])))

    count = max(relative_counts,0)
    return count


def count_read_basedonsites_lastexon_m1_scaledbyl(ppolyasites, profiles, ct=20):
    """
    ----|------|-----|---
      A     B     C    D
    A/LA - B/LB
    B/LB - C/LC
    C/LC - D/LD
    Here I want to assign the reads upstream of each site to that site
    Of course it's upstream of the site to the previous one - down stream to next one
    In this function I do not put any limit on the interval around the site.

    I assume these sites are relative to start of this segment
    """
    done = False
    samples = list(profiles.keys())
    while True:
        #print("in this loop")
        blacklist = []
        counttables = defaultdict(dict)
        ratiotable = defaultdict(dict)
        pre = 0
        site_counter = 0
        #print(ppolyasites)
        #print("here", [iii[0] for iii in ppolyasites])
        for site_info in ppolyasites:
            site = site_info[1]
            counttables[site_info] = {}
            ratiotable[site_info] = {}
            if len(ppolyasites) > site_counter+1 :
                next_s = ppolyasites[site_counter+1][1]
            else:
                next_s = len(profiles[samples[0]])
            #print(site, site_info, pre, next_s)
            for sample in profiles:
                #if site != pre:
                relative_counts  = sum(profiles[sample][pre:site])/(0.001*(max(site-pre,1))) - sum(profiles[sample][site:next_s])/(0.001*(max(next_s-site,1)))
                counttables[site_info][sample] = max(relative_counts,0)
            site_counter+=1
            pre = site
            #print(site, site_info, pre, next_s)
        summ_s = {}
        summ_p = {}
        ratio_p = {}
        if len(ppolyasites) <= 1:
            return counttables
        if len(ppolyasites) > 0 :
            for site_info in ppolyasites:
                site = site_info[1]
                for sample in profiles:
                    if sample not in summ_s:
                        summ_s[sample] = 1
                    summ_s[sample] += counttables[site_info][sample]

            for site_info in ppolyasites:
                site = site_info[1]
                dis_site = site_info[0]
                for sample in profiles:
                    ratiotable[site_info][sample] = counttables[site_info][sample]*100/summ_s[sample]
                    if site_info not in summ_p:
                        summ_p[site_info] = 0
                        ratio_p[site_info] = 0
                    ratio_p[site_info] += counttables[site_info][sample]
                    summ_p[site_info] += ratiotable[site_info][sample]
                ## TODO
                ##print("inreadcount", site_info, ratio_p[site_info], summ_p[site_info])
                if summ_p[site_info] < ct or ratio_p[site_info] < 0 :
                    blacklist.append(site_info)

                    #new_counttables[site_info] =  counttables[site]
            if len(blacklist) == 0:
                counttables_up = {}
                ccounts = counttables.values()
                #if len(ccounts) > 2:
                #    ccounts_sorted = sorted(ccounts, reverse = True)[:2]
                #else:
                return counttables
                #for csite in counttables:
                #    if counttables[csite] > ccounts_sorted[-1]:
                #        counttables_up[csite] = counttables[csite]
                #return counttables_up
            for b in blacklist:
                ppolyasites.remove(b)
    #return counttables

def count_read_basedonsites_lastexon_m1_scaledbyl_percout(ppolyasites, profiles, ct=20):
    """
    ----|------|-----|---
      A     B     C    D
    A/LA - B/LB
    B/LB - C/LC
    C/LC - D/LD
    Here I want to assign the reads upstream of each site to that site
    Of course it's upstream of the site to the previous one - down stream to next one
    In this function I do not put any limit on the interval around the site.

    I assume these sites are relative to start of this segment
    """
    done = False
    samples = list(profiles.keys())
    while True:
        #print("in this loop")
        blacklist = []
        counttables = defaultdict(dict)
        ratiotable = defaultdict(dict)
        pre = 0
        site_counter = 0
        #print(ppolyasites)
        #print("here", [iii[0] for iii in ppolyasites])
        for site_info in ppolyasites:
            site = site_info
            counttables[site_info] = {}
            ratiotable[site_info] = {}
            if len(ppolyasites) > site_counter+1 :
                next_s = ppolyasites[site_counter+1]
            else:
                next_s = len(profiles[samples[0]])
            #print(site, site_info, pre, next_s)
            for sample in profiles:
                #if site != pre:
                relative_counts  = sum(profiles[sample][pre:site])/(0.001*(max(site-pre,1))) - sum(profiles[sample][site:next_s])/(0.001*(max(next_s-site,1)))
                counttables[site_info][sample] = max(relative_counts,0)
            site_counter+=1
            pre = site
            #print(site, site_info, pre, next_s)
        summ_s = {}
        summ_p = {}
        ratio_p = {}
        if len(ppolyasites) == 1:
            return {ppolyasites[0]:(100,100)}
        elif len(ppolyasites) == 0:
            return {}

        if len(ppolyasites) > 0 :
            for site_info in ppolyasites:
                site = site_info
                for sample in profiles:
                    if sample not in summ_s:
                        summ_s[sample] = 1
                    summ_s[sample] += counttables[site_info][sample]

            for site_info in ppolyasites:
                site = site_info
                for sample in profiles:
                    ratiotable[site_info][sample] = counttables[site_info][sample]*100/summ_s[sample]
                    if site_info not in summ_p:
                        summ_p[site_info] = (0,0)
                        ratio_p[site_info] = 0
                    ratio_p[site_info] += counttables[site_info][sample]
                summ_p[site_info] = (ratiotable[site_info]["sample1"], ratiotable[site_info]["sample2"] )
                ## TODO
                ##print("inreadcount", site_info, ratio_p[site_info], summ_p[site_info])
                if sum(summ_p[site_info]) < ct or ratio_p[site_info] < 0 :
                    blacklist.append(site_info)

                    #new_counttables[site_info] =  counttables[site]
            if len(blacklist) == 0:
                return summ_p
            for b in blacklist:
                ppolyasites.remove(b)
    #return counttables

#################################################################################################################################
#################################################################################################################################
#################################################################################################################################
#################################################################################################################################
#################################################################################################################################
# DO not use this
def count_read_basedonsites_lastexon_m1(ppolyasites, profile):
    """
    ----|------|-----|---
      A     B     C    D
    A - (B + C + D)
    B - (C + D)
    C - D
    Here I want to assign the reads upstream of each site to that site
    Of course it's upstream of the site to the previous one - down stream
    In this function I do not put any limit on the interval around the site.

    I assume these sites are relative to start of this segment
    """
    ## THIS RIGHT NOW DOES NOT MAKE SENSE

    counttables = {}
    pre = 0
    for site in ppolyasites:
        relative_counts  = sum(profile[pre:site]) - sum(profile[site:])
        counttables[site] = max(relative_counts,0)
        pre = site
    return counttables



# This seems like what we need
def count_read_basedonsites_lastexon_m1_scaledbyl_22(ppolyasites, profile):
    """
    ----|------|-----|---
      A     B     C    D
    A/LA - B/LB
    B/LB - C/LC
    C/LC - D/LD
    Here I want to assign the reads upstream of each site to that site
    Of course it's upstream of the site to the previous one - down stream to next one
    In this function I do not put any limit on the interval around the site.

    I assume these sites are relative to start of this segment
    """
    counttables = {}
    pre = 0
    site_counter = 0
    for site in ppolyasites:
        if len(ppolyasites) > site_counter+1 :
            next_s = ppolyasites[site_counter+1]
        else:
            next_s = len(profile)
        relative_counts  = sum(profile[pre:site])/(0.001*(site-pre)) - sum(profile[site:next_s])/(0.001*(next_s-site))
        counttables[site] = max(relative_counts,0)
        site_counter+=1
        pre = site
    return counttables


def count_read_basedonsites_lastexon_m2(ppolyasites, profile, interval=200):
    """
    ----|------|-----|----
        S1     S2
    sum(S1-200:S1) - site(S1:S1+200)
    Here I want to assign the reads upstream of each site to that site
    Here I do 200 nt upstream - 200 nt downstream

    I assume these sites are relative to start of this segment
    """
    counttables = {}
    pre = 0

    for site in ppolyasites:
        if interval > site - pre:
            interval = site - pre
        relative_counts  = sum(profile[max(0, site - interval) : site]) - sum(profile[site : site + interval])
        counttables[site] = max(relative_counts,0)
        pre = site
    return counttables


def count_read_basedonsites_intron_m1(ppolyasites, profile, exon_end):
    """
    Starting from the end of exon and doing something similar to lastexon_m1
    """
    counttables = {}
    pre = exon_end
    site_counter = 0
    for site in ppolyasites:
        if len(ppolyasites) > site_counter+1 :
            next_s = ppolyasites[site_counter+1]
        else:
            next_s = len(profile)
        relative_counts  = sum(profile[pre:site])/(0.001*(site-pre)) - sum(profile[site:next_s])/(0.001*(next_s-site))
        counttables[site] = max(relative_counts,0)
        site_counter+=1
        pre = site
    return counttables


def count_read_basedonsites_intron_m2(ppolyasites, profile, interval=300):
    """
    ----|------|-----|----
        S1     S2
    sum(S1-200:S1) - site(S1:S1+200)
    Here I want to assign the reads upstream of each site to that site
    Here I do 200 nt upstream - 200 nt downstream

    I assume these sites are relative to start of this segment
    """
    counttables = {}
    pre = 0
    for site in ppolyasites:
        if interval > site - pre:
            interval = site-pre
        relative_counts  = sum(profile[max(0, site - interval) : site]) - sum(profile[site : site + interval])
        counttables[site] = max(relative_counts,0)
        pre = site
    return counttables
