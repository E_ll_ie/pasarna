from __future__ import print_function
import sys
from collections import defaultdict, namedtuple
import gzip
import numpy
import math
import re
from joblib import Parallel, delayed
import multiprocessing
import gc
import random
import logging

random.seed(7)


import subprocess
from matplotlib import pyplot as plt
from sklearn import tree
from sklearn.ensemble import AdaBoostRegressor
import find_changepoint
from pybedtools import BedTool
import pybedtools
import pandas
#from tensorly.decomposition import robust_pca
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, Matern, ExpSineSquared
sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/')
import ruptures as rpt
sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/changepy/')
from changepy import pelt
from changepy.costs import *
from changepy.costs import poisson
from scipy import stats
from scipy.interpolate import UnivariateSpline
from scipy.signal import wiener, filtfilt, butter, gaussian, freqz
from scipy.ndimage import filters
import scipy.optimize as op
from sklearn.mixture import GaussianMixture
from sklearn.mixture import BayesianGaussianMixture
from scipy.stats import pearsonr, spearmanr
import pymc3 as pm
#sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/matlabengine/lib/python3.6/site-packages/')
#import matlab.engine
#eng = matlab.engine.start_matlab()

import genes
import polya
import const
import sequence
import changepoints as changepointsmethods
import evaluate
import countreads
import segment_process
import segment
import evaluate
import cnn_model
import generate_profile

def detect_round(gene_list, allgenecoverage, coverages, segmentfeats, totalreads, utrsegments, AAAreadsdict_sg = None,
                sites_known = None, apa_known = None, GMM = True, MPeaks = True, tsalg ="BottomUp",
                sjsets_sg = None, usedpas_sg = None, seqmodel = None):
    this_loop_list = []
    evalMaybe = {}
    for _sample in const.SAMPLES:
        evalMaybe[_sample] = evaluate.EVAL()
    all_segment_yesset =  defaultdict(dict)
    all_segment_maybeset =  defaultdict(dict)
    for gene in gene_list:
        num_exons = len(segmentfeats[gene])
        chrom = segmentfeats[gene][0][0]
        strand = segmentfeats[gene][0][-1]

        # TODO :
        # Make sure about read coverage threshold
        if sites_known is None:
            sites_known[gene] = defaultdict(list)

        num_segments = max(segmentfeats[gene].keys()) + 1


        for sample in allgenecoverage:

            # Initialization of dictionaries
            if GMM:
                if sample not in gmmpeaks:
                    gmmpeaks[sample] = defaultdict(dict)

                if gene not in gmmpeaks[sample]:
                    gmmpeaks[sample][gene] = defaultdict(dict)
            if MPeaks:
                if sample not in mmpeaks:
                    mmpeaks[sample] = defaultdict(dict)

                if gene not in mmpeaks[sample]:
                    mmpeaks[sample][gene] = defaultdict(dict)

            if sample not in all_segment_yesset:
                all_segment_yesset[sample] = defaultdict(dict)

            if sample not in all_segment_maybeset:
                all_segment_maybeset[sample] = defaultdict(dict)

            if gene not in all_segment_maybeset[sample]:
                #print("gene in maybe")
                all_segment_yesset[sample][gene] = defaultdict(list)
                all_segment_maybeset[sample][gene] = defaultdict(list)

            totalreads[sample][gene] = max(0.01 , sum(allgenecoverage[sample][gene])/100.0)

            ## This is for tes

            ## This is for adding info from database of sites
            for thisid in range(num_segments):


                if thisid not in sjsets_sg[sample][gene]:
                    sjsets_sg[sample][gene][thisid] = []

                if thisid not in AAAreadsdict_sg[sample][gene]:
                    AAAreadsdict_sg[sample][gene][thisid] = set()

                if thisid not in sites_known[gene]:
                    sites_known[gene][thisid] = set()

                if max(coverages[sample][gene][thisid]) > 1 and sum(coverages[sample][gene][thisid]) > 3 and thisid == (num_segments -1):
                    this_loop_list.append((sample, gene, thisid))



    num_cores = 16 #multiprocessing.cpu_count()
    #all_segment_maybeset =  defaultdict(dict)

    these_segs_info = []
    templist = Parallel(n_jobs=num_cores, verbose=const.VRB, backend="multiprocessing", mmap_mode="r+", temp_folder="/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/joblibtemp/")(delayed(segment_process.process_segment_1)(sample, gene, segmentid,
                                    segmentfeats[gene][segmentid],
                                    coverages[sample][gene][segmentid],
                                    sjsets_sg[sample][gene][segmentid],
                                    AAAreadsdict_sg[sample][gene][segmentid],
                                    sites_known[gene][segmentid],
                                    totalreads[sample][gene],
                                    max(segmentfeats[gene].keys()) + 1,
                                    tsalg,
                                    GMM,
                                    simple=True)
                                    for (sample, gene, segmentid) in this_loop_list)

    these_segs_info += templist
    #del templist[:]

    #print("Initial signal analysis for this batch is done.")
    gc.collect()
    these_segs_info_nosample = []
    for seg in these_segs_info:
        #print(seg)
        segid = seg[0]
        thissample = seg[1]
        tgene = seg[2]
        all_segment_maybeset[sample][tgene][seg[0]]= seg[3]
        if (tgene, segid) not in these_segs_info_nosample:
            these_segs_info_nosample.append((tgene, segid))

    upcov = defaultdict(dict)
    table_rc = defaultdict(dict)
    for seg in these_segs_info_nosample:
        segid = seg[1]
        tgene = seg[0]
        num_segments = len(segmentfeats[tgene].keys())
        all_merged_temp = []
        if tgene not in upcov:
            upcov[tgene] = defaultdict(dict)
            table_rc[tgene] = defaultdict(list)
        upcov[tgene][seg[0]] = defaultdict(list)
        table_rc[tgene][seg[0]] = []
        for sample in allgenecoverage:
            all_merged_temp += all_segment_maybeset[sample][tgene][segid]
            this_profile = coverages[sample][tgene][segid]
            if strand == "-":
                this_profile = this_profile[::-1]
            upcov[tgene][segid][sample] = this_profile

        exon_start = segmentfeats[tgene][segid][1]
        exon_end = segmentfeats[tgene][segid][2]
        segment_end =  segmentfeats[tgene][segid][3]
        exon_strand = segmentfeats[tgene][segid][-1]
        # # TODO:
        ## FIXME:
        if const.inBoth:
            all_merged = changepointsmethods.merge_consec_changepoints_simpleboth(list(set(all_segment_maybeset["sample1"][tgene][segid])), all_segment_maybeset["sample2"][tgene][segid] ,segmentfeats[tgene][0][-1], minDist = const.MERGEMINDIST)
            rel_merged_temp = [countreads.change_coord_to_rel(site, exon_start, exon_end, exon_strand) for site in list(set(all_merged_temp))]
            maybe_table_rc_perc = countreads.count_read_basedonsites_lastexon_m1_scaledbyl_percout(sorted(rel_merged_temp), upcov[tgene][segid], ct=const.CT_ONESEG)
            additions = []
            for prc_site in maybe_table_rc_perc:
                if max(maybe_table_rc_perc[prc_site]) > const.inBoth_OneSampLastEx_PERC:
                    ##This is only for +
                    additions.append(generate_profile.realcoord(exon_start,prc_site, exon_strand))
            all_merged = changepointsmethods.merge_consec_changepoints_simple(all_merged+additions, exon_strand, minDist = const.MERGEMINDIST )
        else:
            #all_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(all_merged_temp)),segmentfeats[tgene][0][-1], minDist = const.MERGEMINDIST)
            all_merged = list(set(all_merged_temp))
        #print(all_merged_temp, all_merged)
        for dis_site in all_merged:
            rel_site = countreads.change_coord_to_rel(dis_site, exon_start, segment_end, strand)
            if segid == num_segments -1 :
                pos_tp = "Last"
            elif (strand == "+" and dis_site <= exon_end) or (strand == "-" and dis_site >= exon_end):
                pos_tp = "Upstream"
            elif (strand == "+" and dis_site >= exon_end and dis_site <= segment_end) or \
            (strand == "-" and dis_site <= exon_end and dis_site >= exon_start ):
                pos_tp = "Intron"
            else:
                pos_tp = "None"

            addflag = False
            if const.UTRREGIONS:
                if (tgene, segid) in utrsegments:
                    addflag = True
            else:
                addflag = True
            if addflag:
                table_rc[tgene][segid].append((dis_site, rel_site, tgene , pos_tp, chrom+strand+str(dis_site)))
                #rc = countreads.count_forexon_scaledbyl_justonesite(rel_site, this_profile, abs(exon_end-exon_start), pos_tp, interval = False, interval_len = 200)
                #temp = "\t".join(map(str, [gene , pos_tp, chrom+strand+str(dis_site), rc, rc/totalreads[sample][tgene]]))
                #rcfile[sample].write(temp + "\n")

    not_marked_genes = []
    for gene_torc in table_rc:
        sites_rc, updated_counts = countreads.analyze_sites(table_rc[gene_torc], upcov[gene_torc], ct = const.CT_ONESEG)


        if not const.DOUBLEDETECT:
            sites_rc = []
        known_rc = []
        for _sample in const.SAMPLES:
            #known_rc = []
            for si_torc in usedpas_sg[_sample][gene_torc]:
                known_rc +=  [f[0] for f in usedpas_sg[_sample][gene_torc][si_torc]]
        ## FIXME:
        #known_rc_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(known_rc)),segmentfeats[gene_torc][0][-1])
        #known_rc_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(known_rc)),segmentfeats[gene_torc][0][-1])
        known_rc_merged =set(known_rc)
        #print("double", updated_counts)
        #print("double", set(sites_rc), known_rc_merged)
        evalMaybe[_sample].compare_with_used(set(sites_rc), known_rc_merged, gene_torc, segmentfeats[gene_torc][0][0], segmentfeats[gene_torc][0][-1], minDist = const.EVAL_MIN)



    return evalMaybe
