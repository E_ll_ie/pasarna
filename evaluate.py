from collections import defaultdict
import logging

class EVAL():
    def __init__(self):
        self.TP_count = 0
        self.FP_count = 0
        self.TN_count = 0
        self.FN_count = 0
        self.tempTP = ""
        self.tempFP = ""
        self.tempTN = ""
        self.tempFN = ""
        self.trueSiteCounter = 0
        self.predictedSiteCounter = 0


    def print_eval(self):
        print(self.TP_count, self.FP_count, self.FN_count, self.trueSiteCounter, self.predictedSiteCounter)

    def compare_with_TAPAS(self, tapasfile, geneiddict):
        # The format pf each line
        #ACAP3   chr1    -       1227763,1228478 0.331248,0.10135        390,47
        tapaslast = defaultdict(list)
        for line in tapasfile:
            fds = line.split()
            genename = fds[0]
            strand = fds[2]
            if genename in geneiddict:
                geneid = geneiddict[genename]
                temp = zip(list(map(int, fds[3].split(","))), list(map(float, fds[4].split(","))))
                #tapaslast[geneid] = sorted(temp, key = lambda x : x[1])
                tapaslast[geneid] = list(map(int, fds[3].split(",")))
                '''
                if strand == "+":
                    tapaslast[geneid] = fds[3]
                    tapaslast[geneid] = max(map(int, fds[3].split(","))) - gstart
                else:
                    tapaslast[geneid] = gstart - min(map(int, fds[3].split(",")))
                '''
        return tapaslast


    def compare_with_used(self, predicted_set, known_set, gene, chrom, strand, minDist = 200):

        #print(predicted_set, known_set)
        for true_site in known_set:
            self.trueSiteCounter += 1
            flag = False
            for pred_site in predicted_set:
                if abs(true_site - pred_site) < minDist:
                    flag = True
                    self.tempTP += "\t".join([chrom , str(true_site), str(true_site), gene, ".", strand]) +"\n"
                    self.TP_count +=1
                    break
            if not flag:
                self.tempFN += "\t".join([chrom , str(true_site), str(true_site), gene, ".", strand]) +"\n"
                self.FN_count +=1

        for pred_site in predicted_set:
            self.predictedSiteCounter += 1
            flag = False
            for true_site in known_set:
                if abs(true_site - pred_site) < minDist:
                    flag = True
                    break
            if not flag:
                self.tempFP += "\t".join([chrom , str(pred_site), str(pred_site), gene, ".", strand]) +"\n"
                self.FP_count +=1
