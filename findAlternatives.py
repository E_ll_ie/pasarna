import sys
import countreads
import changepoints as changepointsmethods
import segment
import genes
import generate_profile

# Here we are considering different scenarios
# Two alternative sites on last exon
# Two alternative sites on upstream exon
# Two alternative sites one on last exon, one on upstream exon
# Two alternative sites one on one upstream site, one on other upstream site
def limit_to_two(this_seg, profile, psites):
    for site in psites:
        counts[site] = count_forexon_scaledbyl_justonesite(site, profile, exon_end, pos_tp)
