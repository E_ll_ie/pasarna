#This is a code for using RNASeq in PAS Analysis
#Some parts are for validation and not part of this pipeline
from __future__ import print_function
import sys
from collections import defaultdict, namedtuple
import gzip
import numpy
import math
import re
from joblib import Parallel, delayed
import multiprocessing
import gc
import random
import logging

random.seed(7)


import subprocess
from matplotlib import pyplot as plt
from sklearn import tree
from sklearn.ensemble import AdaBoostRegressor
import find_changepoint
from pybedtools import BedTool
import pybedtools
import pandas
#from tensorly.decomposition import robust_pca
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, Matern, ExpSineSquared
sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/')
import ruptures as rpt
sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/changepy/')
from changepy import pelt
from changepy.costs import *
from changepy.costs import poisson
from scipy import stats
from scipy.interpolate import UnivariateSpline
from scipy.signal import wiener, filtfilt, butter, gaussian, freqz
from scipy.ndimage import filters
import scipy.optimize as op
from sklearn.mixture import GaussianMixture
from sklearn.mixture import BayesianGaussianMixture
from scipy.stats import pearsonr, spearmanr
import pymc3 as pm
#sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/matlabengine/lib/python3.6/site-packages/')
#import matlab.engine
#eng = matlab.engine.start_matlab()

import genes
import polya
import const
import sequence
import changepoints as changepointsmethods
import evaluate
import countreads
import segment_process
import segment
import evaluate
import cnn_model
import detectround
'''
def parse_junctionfile(jfiledir):
    jclist = []
    with open(jfiledir, 'r') as jfile:
        for line in jfile:
            fds = line.split()
            chrm = fds[0]
            jstart = fds[1]
            jend = fds[2]
            strand = fds[3]
            jc = fds[]
'''

# TODO:
# Find a way to make coverage part faster
# What if we have it for all genes from the beginning
# Seems like we can use bedtools coverage
# So first extend the regions
logging.basicConfig(level=logging.DEBUG)

def make_fullprofiles():
    with open(fileforfindpeak, 'w') as matlabfile:
        for gene in gene_list:
            num_exons = len(segmentfeats[gene])
            for sample in allgenecoverage:
                totalreads = max(0.01 , sum(allgenecoverage[sample][gene])/100.0)
                for segmentind in range(len(segmentfeats[gene])):
                    profile = [x/totalreads for x in coverages[sample][gene][segmentid]]
                    profile = coverages[sample][gene][segmentid]
                    if segmentfeats[gene][segmentid][-1] == "-":
                        reverseflag = True
                    # I want to find the peaks usng MATLAB here
                    # Not the fastest way
                    if not reverseflag:
                        matlabfile.write(gene + "\t" + str(segmentid) + sample +
                                         "\t" + ",".join(map(str,profile)) + "\n" )

                    else:
                        matlabfile.write(gene + "\t" + str(segmentid) + sample +
                                         "\t" + ",".join(map(str,profile[::-1])) + "\n" )

    #Here run findpeaks for all!
    process.call()


def realcoord(real_start, this_loc, strand):
    if strand == "+":
        return real_start +  this_loc
    elif strand == "-":
        return real_start - this_loc

def call_known_changepoints(processedgenes, allgenecoverage, coverages, segmentfeats, sites_known = None):

    yes_table_rc = defaultdict(dict)
    rcfile = {}
    for sample in allgenecoverage:
        yes_table_rc[sample] = defaultdict(dict)
        rcfile[sample] = open(sample + "_readcounts.txt", "w")
    # This is for testing mainly
    ggcounter = 0
    for gene in processedgenes:

        ggcounter += 1
        if ggcounter % 1000 == 0:
            print(ggcounter, " is done")
        num_exons = len(segmentfeats[gene])
        if num_exons > 0:
            chrom = segmentfeats[gene][0][0]
            strand = segmentfeats[gene][0][-1]
            genecoverageflag = False
            # TODO :
            # Make sure about read coverage threshold
            for sample in allgenecoverage:
                genecovtocheck = allgenecoverage[sample][gene]
                #if sum(genecovtocheck)/(len(genecovtocheck) - genecovtocheck.count(0) + 1) > 2:
                if sum(genecovtocheck)/(len(genecovtocheck) - numpy.count_nonzero(genecovtocheck == 0) + 1) > 0:
                    genecoverageflag = True

            if genecoverageflag:
                #print(gene)
                sprofile = defaultdict(dict)
                totalreads = {}
                for sample in allgenecoverage:
                    sprofile[sample] = defaultdict(list)
                    yes_table_rc[sample][gene] = defaultdict(dict)
                these_segs_info  = {}

                for sample in allgenecoverage:
                    totalreads[sample] = max(0.01 , sum(allgenecoverage[sample][gene])/100.0)
                    num_segments = max(segmentfeats[gene].keys()) + 1

                    if sites_known is not None:
                        if gene in sites_known:
                            sites_known_g = sites_known[gene]
                        else:
                            sites_known_g = {}
                    else:
                        sites_known_g = {}

                    num_cores = min(num_segments, multiprocessing.cpu_count())

                    for segmentid in range(num_segments):
                        segcoverageflag = False
                        for sample_other in allgenecoverage:
                            segcovtocheck = coverages[sample_other][gene][segmentid]
                            #if sum(segcovtocheck)/(len(segcovtocheck) - segcovtocheck.count(0) + 1) > 2:
                            if sum(segcovtocheck)/(len(segcovtocheck) - numpy.count_nonzero(segcovtocheck == 0) + 1) > 2:
                                segcoverageflag = True

                        if segmentid in sites_known_g and segcoverageflag:
                            this_profile = coverages[sample][gene][segmentid]
                            if strand == "-":
                                this_profile = this_profile[::-1]
                            exon_start = segmentfeats[gene][segmentid][1]
                            exon_end = segmentfeats[gene][segmentid][2]
                            segment_end =  segmentfeats[gene][segmentid][3]
                            these_known_sites = set(sites_known_g[segmentid])
                            yes_table_rc[sample][gene][segmentid] = {}

                            for known_site in these_known_sites:
                                flag = True
                                rel_site = countreads.change_coord_to_rel(known_site, exon_start, segment_end, strand)
                                if segmentid == num_segments -1 :
                                    pos_tp = "Last"
                                elif (strand == "+" and known_site <= exon_end) or (strand == "-" and known_site >= exon_end):
                                    pos_tp = "Upstream"
                                elif (strand == "+" and known_site >= exon_end and known_site <= segment_end) or \
                                (strand == "-" and known_site <= exon_end and known_site >= exon_start ):
                                    pos_tp = "Intron"
                                else:
                                    flag = False
                                    #print(segmentfeats[gene][segmentid], known_site, rel_site)
                                if flag:
                                    #print("we are here")
                                    rc = countreads.count_forexon_scaledbyl_justonesite(rel_site, this_profile, abs(exon_end-exon_start), pos_tp, interval = False, interval_len = 200)
                                    yes_table_rc[sample][gene][segmentid][known_site] = (pos_tp, rc, rc/totalreads[sample])

                                    temp = "\t".join(map(str, [gene , pos_tp, chrom+strand+str(known_site), rc, rc/totalreads[sample]]))
                                    rcfile[sample].write(temp + "\n")
                            '''
                            for known_site in these_known_sites:
                                flag = True
                                rel_site = [countreads.change_coord_to_rel(known_site, exon_start, segment_end, strand)]
                                if segmentid == num_segments -1 :
                                    pos_tp = "Last"
                                elif (strand == "+" and known_site <= exon_end) or (strand == "-" and known_site >= exon_end):
                                    pos_tp = "Upstream"
                                elif (strand == "+" and known_site >= exon_end and known_site <= segment_end) or \
                                (strand == "-" and known_site <= exon_end and known_site >= exon_start ):
                                    pos_tp = "Intron"
                                    #rc = countreads.count_read_basedonsites_intron_m2(rel_site, this_profile, abs(exon_end - exon_start))[rel_site[0]]
                                else:
                                    flag = False
                                these_sites.append((known_site, rel_site, pos_tp))
                            '''



    for sample in allgenecoverage:
        rcfile[sample].close()






def call_changepoints(processedgenes, allgenecoverage, coverages, segmentfeats, AAAreadsdict = None,
                      sites_known = None, apa_known = None, GMM = True, MPeaks = True, tsalg ="BottomUp",
                      sjsets = None, usedpas = None, seqmodel = None, utrsegments = None):

    evalYes = {}
    evalMaybe = {}
    for _sample in const.SAMPLES:
        evalYes[_sample] = evaluate.EVAL()
        evalMaybe[_sample] = evaluate.EVAL()

    tttfile = open("genelist.txt", 'w')
    gmmpeaks = defaultdict(dict) # For GMM peaks
    mmpeaks = defaultdict(dict) # For matlab findpeaks
    all_segment_yesset = defaultdict(dict)
    all_segment_maybeset = defaultdict(dict)
    maybe_table_rc = defaultdict(dict)
    yes_table_rc = defaultdict(dict)
    totalreads = defaultdict(dict)
    sjsets_sg = defaultdict(dict)
    usedpas_sg = defaultdict(dict)
    AAAreadsdict_sg = defaultdict(dict)
    '''
    for sample in allgenecoverage:
        for gene in allgenecoverage[sample]:
            print(gene, sample, allgenecoverage[sample][gene][1:10], len(allgenecoverage[sample][gene]))
    '''
    samples = []
    sprofile = defaultdict(dict)
    for sample in allgenecoverage:
        totalreads[sample] = defaultdict(dict)

        samples.append(sample)
        sprofile[sample] = defaultdict(dict)
        sjsets_sg[sample] = defaultdict(dict)
        usedpas_sg[sample] = defaultdict(dict)
        AAAreadsdict_sg[sample] = defaultdict(dict)

    # This is for testing mainly
    ggcounter = 0
    this_gene_set = []
    these_segs_info  = []
    this_loop_list = []
    that_loop_list =  []

    for gene in processedgenes:
        sample = "sample1"
        genecovtocheck = allgenecoverage[sample][gene]

        if sum(genecovtocheck)/(len(genecovtocheck) - numpy.count_nonzero(genecovtocheck == 0) + 1) > 0:
            if gene in usedpas[sample]:
                for sid in usedpas[sample][gene]:
                    for sss in usedpas[sample][gene][sid]:
                        tttfile.write(gene + "\t" + str(sid) + "\t" +str(sss) + "\n")
    tttfile.close()
    for gene in processedgenes:
        maybe_table_rc[gene] = defaultdict(dict)
        yes_table_rc[gene] = defaultdict(dict)

        genecoverageflag = False
        for sample in allgenecoverage:
            genecovtocheck = allgenecoverage[sample][gene]

            if sum(genecovtocheck)/(len(genecovtocheck) - numpy.count_nonzero(genecovtocheck == 0) + 1) > 0:
                genecoverageflag = True
        if genecoverageflag:
            this_gene_set.append(gene)
        these_segs_info = []
        if len(this_gene_set) == const.GENEBATCH:
            for gene in this_gene_set:
                ggcounter += 1
                if ggcounter % 100 == 0:
                    print(ggcounter, " is done")
                num_exons = len(segmentfeats[gene])
                chrom = segmentfeats[gene][0][0]
                strand = segmentfeats[gene][0][-1]
                # TODO :
                # Make sure about read coverage threshold
                if sites_known is None:
                    sites_known[gene] = defaultdict(list)

                num_segments = max(segmentfeats[gene].keys()) + 1


                for sample in samples:
                    sprofile[sample][gene] = defaultdict(dict)
                    maybe_table_rc[sample][gene] = defaultdict(list)
                    yes_table_rc[sample][gene] = defaultdict(list)
                    # Initialization of dictionaries
                    if GMM:
                        if sample not in gmmpeaks:
                            gmmpeaks[sample] = defaultdict(dict)

                        if gene not in gmmpeaks[sample]:
                            gmmpeaks[sample][gene] = defaultdict(dict)
                    if MPeaks:
                        if sample not in mmpeaks:
                            mmpeaks[sample] = defaultdict(dict)

                        if gene not in mmpeaks[sample]:
                            mmpeaks[sample][gene] = defaultdict(dict)

                    if sample not in all_segment_yesset:
                        all_segment_yesset[sample] = defaultdict(dict)

                    if sample not in all_segment_maybeset:
                        all_segment_maybeset[sample] = defaultdict(dict)

                    if gene not in all_segment_yesset[sample]:
                        all_segment_yesset[sample][gene] = defaultdict(list)
                        all_segment_maybeset[sample][gene] = defaultdict(list)

                    totalreads[sample][gene] = max(0.01 , sum(allgenecoverage[sample][gene])/100.0)

                    ## This is for test
                    if usedpas is not None:
                        if gene in usedpas[sample]:
                            usedpas_sg[sample][gene] = usedpas[sample][gene]
                        else:
                            usedpas_sg[sample][gene] = {}
                    else:
                        usedpas_sg[sample][gene] = {}

                    ## This is for removing junctions
                    if sjsets is not None:
                        if gene in sjsets[sample]:
                            sjsets_sg[sample][gene] = sjsets[sample][gene]
                        else:
                            sjsets_sg[sample][gene] = {}
                    else:
                        sjsets_sg[sample][gene] = {}

                    ## This is for adding AAA reads
                    if AAAreadsdict  is not None:
                        if gene in AAAreadsdict[sample]:
                            AAAreadsdict_sg[sample][gene] = AAAreadsdict[sample][gene]
                        else:
                            AAAreadsdict_sg[sample][gene] = {}
                    else:
                        AAAreadsdict_sg[sample][gene] = {}

                    ## This is for adding info from database of sites
                    for thisid in range(num_segments):
                        maybe_table_rc[gene][thisid] = defaultdict(dict)
                        yes_table_rc[gene][thisid] = defaultdict(dict)

                        if thisid not in sjsets_sg[sample][gene]:
                            sjsets_sg[sample][gene][thisid] = []

                        if thisid not in AAAreadsdict_sg[sample][gene]:
                            AAAreadsdict_sg[sample][gene][thisid] = set()

                        if thisid not in sites_known[gene]:
                            sites_known[gene][thisid] = set()

                        if max(coverages[sample][gene][thisid]) >= 0 and sum(coverages[sample][gene][thisid]) >= 0:
                            this_loop_list.append((sample, gene, thisid))
                            if (gene, thisid) not in that_loop_list:
                                that_loop_list.append((gene, thisid))

                #num_cores = min(2*num_segments, multiprocessing.cpu_count())
                #print("pre-part1", gene, num_segments, num_cores)
            num_cores = const.NUMCORES #multiprocessing.cpu_count()

            #print(len(this_loop_list))
            #print(this_gene_set)
            templist = Parallel(n_jobs=num_cores, verbose=const.VRB, backend="multiprocessing", mmap_mode="r+", temp_folder="/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/joblibtemp/")(delayed(segment_process.process_segment_1)(sample1, gene1, segmentid1,
                                            segmentfeats[gene1][segmentid1],
                                            coverages[sample1][gene1][segmentid1],
                                            sjsets_sg[sample1][gene1][segmentid1],
                                            AAAreadsdict_sg[sample1][gene1][segmentid1],
                                            sites_known[gene1][segmentid1],
                                            totalreads[sample1][gene1],
                                            len(segmentfeats[gene1].keys()),
                                            tsalg,
                                            GMM)
                                            for (sample1, gene1, segmentid1) in this_loop_list)

            these_segs_info += templist
            #del templist[:]
            '''
            for (sample, gene, segmentid) in this_loop_list:
                print(sample, gene, segmentid)
                these_segs_info.append(segment_process.process_segment_1(sample, gene, segmentid,
                                            segmentfeats[gene][segmentid],
                                            coverages[sample][gene][segmentid],
                                            sjsets_sg[sample][gene][segmentid],
                                            AAAreadsdict_sg[sample][gene][segmentid],
                                            sites_known[gene][segmentid],
                                            totalreads[sample][gene],
                                            max(segmentfeats[gene].keys()) + 1,
                                            tsalg,
                                            GMM))
            '''
            #these_segs_info += templist
            #del templist[:]
            #print("Initial signal analysis for this batch is done.")
            gc.collect()
            #print(getsizeof(templist))
            maybeseqbed = ""
            yesseqbed = ""
            mayabee = defaultdict(dict)
            mayabee2 = defaultdict(list)
            that_loop_list = []
            for seg in these_segs_info:
                #print(seg)
                if not const.TESTR:
                    if ((seg[3], seg[0])) not in that_loop_list:
                        that_loop_list.append((seg[3], seg[0]))
                    thissample2 = seg[2]
                    gene2 = seg[3]
                    if not const.SEQCHECK:
                        all_segment_yesset[thissample2][gene2][seg[0]] = seg[1].segment_yesset
                        all_segment_maybeset[thissample2][gene2][seg[0]] = seg[1].segment_maybeset
                    else:
                        all_segment_yesset[thissample2][gene2][seg[0]] = []
                        all_segment_maybeset[thissample2][gene2][seg[0]] = []
                        maybeseqbed += seg[4]
                        yesseqbed += seg[5]
                    sprofile[thissample2][gene2][seg[0]] = seg[1].sprofile

                if const.TESTR:
                    gene2 = seg[2]
                    thissample2 = seg[1]
                    if gene2 not in mayabee:
                        mayabee[gene2] = defaultdict(list)
                    if seg[0] not in mayabee[gene2]:
                        mayabee[gene2][seg[0]] = []
                    mayabee[gene2][seg[0]] += seg[3]+seg[4]
                    if gene2 not in mayabee2:
                        mayabee2[gene2] = []
                    mayabee2[gene2] +=  seg[3]+seg[4]


            # if const.TESTR:
            #     for ogene in mayabee:
            #         this_known_set =[]
            #         #for osample in ["sample1", "sample2"] :
            #         for osample in ["sample1"]:
            #             for osid in usedpas_sg[osample][ogene]:
            #                 this_known_set +=  [f[0] for f in usedpas_sg[osample][ogene][osid]]
            #
            #         #if sid == len(segmentfeats[gene].keys()) - 1 :
            #         #print(list(set(mayabee[ogene][osid])), set(this_known_set) )
            #         #print(ogene, mayabee[ogene], this_known_set)
            #         evalMaybe.compare_with_used(list(set(mayabee[ogene])), set(this_known_set), ogene, segmentfeats[ogene][0][0], segmentfeats[ogene][0][-1], minDist = 200)


            if const.TESTR:
                for ogene in mayabee:
                    for _sample in const.SAMPLES:
                        all_this_known_set = []
                        for osidd  in usedpas_sg[_sample][ogene]:
                            all_this_known_set += [f[0] for f in usedpas_sg[_sample][ogene][osidd]]
                            #if sid == len(segmentfeats[gene].keys()) - 1 :
                            #print(list(set(mayabee[ogene][osid])), set(this_known_set) )
                        evalMaybe[_sample].compare_with_used(list(set(mayabee2[ogene])), set(all_this_known_set), ogene, segmentfeats[ogene][0][0], segmentfeats[ogene][0][-1], minDist = const.EVAL_MIN)

                        #print(_sample, ogene, sorted(mayabee2[ogene]), set(all_this_known_set) )
                for _sample in const.SAMPLES:
                    print("TP", evalMaybe[_sample].TP_count, "FP", evalMaybe[_sample].FP_count, "FN", evalMaybe[_sample].FN_count, "Trues", evalMaybe[_sample].trueSiteCounter, "Predicted", evalMaybe[_sample].predictedSiteCounter)
                this_gene_set = []
                this_loop_list = []
                that_loop_list = []

            if not const.TESTR:
                #print("Initial signal analysis results for this batch are merged.")

                if const.SEQCHECK:
                    maybeseqdict = sequence.extract_seq_frombed(maybeseqbed, const.genomefa)
                    yesseqdict = sequence.extract_seq_frombed(yesseqbed, const.genomefa)
                    seqp = 20
                    seqpshift = 10
                    if len(maybeseqbed) > 0:
                        site_or_nots = cnn_model.test_by_trained_model(seqmodel, maybeseqdict.values(),seqp)
                        seq_names = list(maybeseqdict.keys())
                        for this_pos_ind in range(len(seq_names)):
                            tsi, gi, si, pi = seq_names[this_pos_ind].split("|")
                            maxxseq = max(site_or_nots[this_pos_ind*seqp:this_pos_ind*seqp+seqp])
                            #all_others[tsi][gi][int(si)].append(int(pi), maxxseq)
                            ## FIXME:
                            if maxxseq >= const.SEQCHECKTHRESH:
                                #shift_ind = 0
                                shift_ind = list(site_or_nots[this_pos_ind*seqp:this_pos_ind*seqp+seqp]).index(maxxseq) - int(seqp/2)
                                all_segment_maybeset[tsi][gi][int(si)].append(int(pi) + shift_ind*seqpshift)
                    seqp = 20
                    if len(yesseqbed) > 0:
                        site_or_nots = cnn_model.test_by_trained_model(seqmodel, yesseqdict.values(), seqp)
                        seq_names = list(yesseqdict.keys())
                        for this_pos_ind in range(len(seq_names)):
                            tsi, gi, si, pi = seq_names[this_pos_ind].split("|")
                            maxxseq = max(site_or_nots[this_pos_ind*seqp:this_pos_ind*seqp+seqp])
                            # FIXME:
                            if maxxseq >= 0:
                            #if maxxseq >= const.SEQCHECKTHRESH:
                                shift_ind = 0
                                #shift_ind = list(site_or_nots[this_pos_ind*seqp:this_pos_ind*seqp+seqp]).index(maxxseq) - int(seqp/2)
                                all_segment_yesset[tsi][gi][int(si)].append(int(pi) + shift_ind*seqpshift)



                ## Here we use the info from both samples
                num_cores = const.NUMCORES
                #print("Sequences were scored for this batch", len(that_loop_list))
                #print(that_loop_list)
                these_segs_rc_info = Parallel(n_jobs=num_cores, verbose=const.VRB, backend="multiprocessing", mmap_mode="r+", temp_folder="/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/joblibtemp/")(
                delayed(segment_process.process_segment_2)(gene3, segmentid3,
                                                len((segmentfeats[gene3].keys())) ,
                                                segmentfeats[gene3][segmentid3],
                                                [sjsets_sg[_sample][gene3][segmentid3] for _sample in const.SAMPLES],
                                                [all_segment_maybeset[_sample][gene3][segmentid3] for _sample in const.SAMPLES],
                                                [all_segment_yesset[_sample][gene3][segmentid3] for _sample in const.SAMPLES],
                                                [coverages[_sample][gene3][segmentid3] for _sample in const.SAMPLES])
                                                for (gene3, segmentid3) in that_loop_list)

                gc.collect()
                upcov = defaultdict(dict)
                table_rc = defaultdict(dict)

                #print("these_segs_rc_info", len(these_segs_rc_info))

                for seg in these_segs_rc_info:
                    segid = seg[0]
                    yes_merged = seg[3]
                    #print(yes_merged)
                    maybe_merged = seg[4]
                    this_known_set = []
                    tgene = seg[5]
                    if tgene not in upcov:
                        upcov[tgene] = defaultdict(dict)
                        table_rc[tgene] = defaultdict(list)
                    upcov[tgene][seg[0]] = defaultdict(list)
                    table_rc[tgene][seg[0]] = []
                    #yes_merged = []
                    ## FIXME:
                    all_merged = []
                    #for _sample in const.SAMPLES[0]:
                    #for _sample in ["sample1"]:
                    #    all_merged += list(set(all_segment_maybeset[_sample][tgene][segid]))
                    #    all_merged += list(set(all_segment_yesset[_sample][tgene][segid]))
                    all_merged = list(set(yes_merged + maybe_merged))
                    ##all_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(all_merged)),segmentfeats[tgene][0][-1], minDist = const.MERGECONSDIST)
                    ##all_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(all_segment_maybeset[_sample][tgene][segid])),segmentfeats[tgene][0][-1])
                    #this_known_set_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(this_known_set)),segmentfeats[tgene][0][-1])
                    #if len(all_merged) >= 1:
                    #    print(tgene, "all_merged", all_merged)
                    #print("aftermerge", len(all_cd))


                    exon_start = segmentfeats[tgene][segid][1]
                    exon_end = segmentfeats[tgene][segid][2]
                    segment_end =  segmentfeats[tgene][segid][3]
                    these_known_sites = set(this_known_set)

                    for _sample in const.SAMPLES:
                        this_profile = coverages[_sample][tgene][segid]
                        if strand == "-":
                            this_profile = this_profile[::-1]
                        upcov[tgene][segid][_sample] = this_profile
                    for dis_site in all_merged:
                        rel_site = countreads.change_coord_to_rel(dis_site, exon_start, segment_end, strand)
                        if segid == num_segments -1 :
                            pos_tp = "Last"
                        elif (strand == "+" and dis_site <= exon_end) or (strand == "-" and dis_site >= exon_end):
                            pos_tp = "Upstream"
                        elif (strand == "+" and dis_site >= exon_end and dis_site <= segment_end) or \
                        (strand == "-" and dis_site <= exon_end and dis_site >= exon_start ):
                            pos_tp = "Intron"
                        else:
                            pos_tp = "None"
                        addflag = False
                        if const.UTRREGIONS:
                            if (tgene, segid) in utrsegments:
                                addflag = True
                        else:
                            addflag = True
                        if addflag:
                            table_rc[tgene][segid].append((dis_site, rel_site, tgene , pos_tp, chrom+strand+str(dis_site)))
                        #rc = countreads.count_forexon_scaledbyl_justonesite(rel_site, this_profile, abs(exon_end-exon_start), pos_tp, interval = False, interval_len = 200)
                        #temp = "\t".join(map(str, [gene , pos_tp, chrom+strand+str(dis_site), rc, rc/totalreads[sample][tgene]]))
                        #rcfile[sample].write(temp + "\n")
                        #trackers.add(tgene)
                #print("rightbeforeanalyze ", hahacount,  len(hohocounts.keys()), len(trackers), kala, kala1)
                not_marked_genes = []
                for gene_torc in table_rc:
                    #tttfile.write(gene_torc+ "\n")
                    #print(gene_torc)
                    #print(gene_torc, len(table_rc[gene_torc]))
                    sites_rc, updated_counts = countreads.analyze_sites(table_rc[gene_torc], upcov[gene_torc], ct = const.CT_ALLSEG)
                    #for sample in coverages:



                    maxformark = 0
                    for _sample in const.SAMPLES:
                        maxformark = max(totalreads[_sample][gene_torc], maxformark)
                    if len(sites_rc) == 0 and maxformark > const.DETECTTHRESH:
                        not_marked_genes.append(gene_torc)

                    else:
                        known_rc = []
                        for _sample in const.SAMPLES:
                            #known_rc = []
                            for si_torc in usedpas_sg[_sample][gene_torc]:
                                known_rc +=  [f[0] for f in usedpas_sg[_sample][gene_torc][si_torc]]
                        ## FIXME:
                        #known_rc_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(known_rc)),segmentfeats[gene_torc][0][-1])
                        known_rc_merged =set(known_rc)
                        #print(known_rc_merged)
                        #print(gene_torc, "main", sorted(list(set(sites_rc))),  sorted(list(set(known_rc_merged))))
                        evalMaybe[_sample].compare_with_used(list(set(sites_rc)), list(known_rc_merged), gene_torc, segmentfeats[gene_torc][0][0], segmentfeats[gene_torc][0][-1], minDist = const.EVAL_MIN)



                tempevalMaybe = detectround.detect_round(not_marked_genes, allgenecoverage, coverages, segmentfeats, totalreads, utrsegments, AAAreadsdict_sg = AAAreadsdict_sg, sites_known = sites_known , apa_known = None, GMM = False, MPeaks = False, tsalg ="Binary", sjsets_sg = sjsets_sg, usedpas_sg = usedpas_sg, seqmodel = seqmodel)
                #evalMaybe["sample2"].print_eval()
                #tempevalMaybe["sample2"].print_eval()
                for _sample in const.SAMPLES:
                    evalMaybe[_sample].TP_count += tempevalMaybe[_sample].TP_count
                    evalMaybe[_sample].FP_count += tempevalMaybe[_sample].FP_count
                    evalMaybe[_sample].FN_count += tempevalMaybe[_sample].FN_count
                    evalMaybe[_sample].trueSiteCounter += tempevalMaybe[_sample].trueSiteCounter
                    evalMaybe[_sample].predictedSiteCounter += tempevalMaybe[_sample].predictedSiteCounter



                #for _sample in const.SAMPLES:
                print(round(evalMaybe[_sample].TP_count*100.0/evalMaybe[_sample].trueSiteCounter,2), round(100 - evalMaybe[_sample].FP_count*100.0/evalMaybe[_sample].predictedSiteCounter,2) )
                print("TP", evalMaybe[_sample].TP_count, "FP", evalMaybe[_sample].FP_count, "FN", evalMaybe[_sample].FN_count, "Trues", evalMaybe[_sample].trueSiteCounter, "Predicted", evalMaybe[_sample].predictedSiteCounter)

                del this_loop_list[:]
                del that_loop_list[:]
                this_loop_list = []
                that_loop_list = []
                this_gene_set = []
                gc.collect()
                #print("garbage collected")
            #
            # lstyle = {'sample1' : "--", "sample2" : "-"}

            #at this point we should have a set of information for each gene available
            # flag = False
            #
            # if flag:
            #     for segmentid in segmentfeats[gene]:
            #         usedpasflag = False
            #         uu = []
            #         if gene in usedpas[sample]:
            #             if segmentid in usedpas[sample][gene]:
            #                 usedpasflag = True
            #                 uu = usedpas[sample][gene]
            #         if usedpasflag:
            #         #if len(all_segment_maybeset[sample][gene][segmentid]) != len(uu):
            #         #if len(all_segment_maybeset[sample][gene][segmentid]) > 0 or \
            #         #len(all_segment_yesset[sample][gene][segmentid]) > 0 or usedpasflag :
            #
            #
            #             fig, axs = plt.subplots(2, 1)
            #             #print(segmentfeats[gene][segmentid])
            #             start = segmentfeats[gene][segmentid][1]
            #             end = segmentfeats[gene][segmentid][3]
            #             axind = -1
            #             for sample in allgenecoverage:
            #                 axind+=1
            #                 axs[axind].set_xlim(start, end)
            #                 #plt.plot(range(start, end), [x/totalreads[sample] for x in coverages[sample][gene][segmentid]], color = "grey", label = sample)
            #                 #plt.plot(range(start, end), coverages[sample][gene][segmentid], color = "grey", label = sample, ls = lstyle[sample])
            #                 axs[axind].plot(range(start, end), sprofile[sample][segmentid], color = "grey", label = sample, ls = lstyle[sample])
            #                 #for p in mmpeaks[sample][gene][segmentid] :
            #                 #    axs[axind].axvline(p.pos, color = "red", ls = lstyle[sample], label = "findpeaks")
            #                 #if GMM:
            #                 #    for p in gmmpeaks[sample][gene][segmentid] :
            #                 #        axs[axind].axvline(p.pos, color = "yellow", ls = lstyle[sample], label = "findpeaks")
            #                 for ys in all_segment_yesset[sample][gene][segmentid]:
            #                     axs[axind].axvline(ys, color = "green", label = "yesset", ls = lstyle[sample])
            #                 for ms in all_segment_maybeset[sample][gene][segmentid]:
            #                     axs[axind].axvline(ms, color = "cyan", label = "maybeset", ls = lstyle[sample])
            #                 if segmentid in sjsets[sample][gene]:
            #                     for sjj in sjsets[sample][gene][segmentid]:
            #                         axs[axind].axvline(sjj[0], color = "purple", label = "sj", ls = lstyle[sample])
            #                         axs[axind].axvline(sjj[1], color = "purple", label = "sj", ls = lstyle[sample])
            #
            #                 if usedpas is not None:
            #                     if gene in usedpas[sample]:
            #                         if segmentid in usedpas[sample][gene]:
            #                             these_gene_psites = usedpas[sample][gene][segmentid]
            #                             for p_site in these_gene_psites:
            #                                     axs[axind].axvline(p_site[0], color = "pink", label = "used_site", alpha = 0.5)
            #             if apa_known is not None:
            #                 if gene in apa_known:
            #                     if segmentid in apa_known[gene]:
            #                         these_gene_apa = apa_known[gene][segmentid]
            #                         for apa_site in these_gene_apa:
            #                             for axind_ in range(axind+1):
            #                                 axs[axind_].axvline(apa_site, color = "orange", label = "known_apa")
            #             '''
            #             if sites_known is not None:
            #                 if gene in sites_known:
            #                     if segmentid in sites_known[gene]:
            #                         these_gene_psites = sites_known[gene][segmentid]
            #                         for p_site in these_gene_psites:
            #                             for axind_ in range(axind+1):
            #                                 axs[axind_].axvline(p_site, color = "black", label = "known_site")
            #             '''
            #             plt.show()

    print("used site comparison")
    #print("TP", evalYes.TP_count, "FP", evalYes.FP_count, "FN", evalYes.FN_count, "Trues", evalYes.trueSiteCounter, "Predicted", evalYes.predictedSiteCounter)
    #print("TP", evalMaybe.TP_count, "FP", evalMaybe.FP_count, "FN", evalMaybe.FN_count, "Trues", evalMaybe.trueSiteCounter, "Predicted", evalMaybe.predictedSiteCounter)

def cross_compare_withsjs(setA, otherSetJ, real_start, strand, minDist):
    newsetA = []
    for siteA in setA:
        removeflag = False
        for siteJ in otherSetJ:
            if strand == "+":
                rel_siteJ = siteJ[0]
            else:
                rel_siteJ = siteJ[1]
            dist = abs(rel_siteJ - siteA)
            if dist < minDist:
                #print("removed in compare")
                removeflag = True

        if not removeflag:
            newsetA.append(siteA)
    #print(newsetA, setA, otherSetJ)
    return newsetA



def compare_withsjs(setA, setJ, coverage, real_start, strand, minDist, minUp, factor = const.SJRATIO):
    tempsites = []
    if len(setA) == 1:
        minDist = 2.5*minDist
        factor = 0.5
    else:
        minDist = 2*minDist
    for siteA in setA:
        flag = True
        for siteJ in setJ:
            if strand == "+":
                rel_siteJ = abs(real_start - siteJ[0])
            else:
                rel_siteJ = abs(real_start - siteJ[1])
            dist = abs(rel_siteJ - siteA)
            #print(siteA, siteJ, rel_siteJ, dist, siteJ[-1]+siteJ[-2], sum(coverage[siteA - minUp: siteA]), factor*sum(coverage[siteA - minUp: siteA])/minUp )
            if dist < minDist:
                if siteJ[-1]+siteJ[-2]  >=  factor*sum(coverage[max(0, siteA - minUp): siteA])/min(siteA,minUp) :
                #if siteJ[-1]+siteJ[-2]  >=  sum(coverage[rel_siteJ - minUp: rel_siteJ])/minUp :
                    flag = False
            if siteA < rel_siteJ:
                if sum(coverage[siteA :rel_siteJ])/(rel_siteJ - siteA) < 1:
                    flag = False

            if siteA < rel_siteJ:
                if sum(coverage[rel_siteJ:siteA])/(siteA-rel_siteJ) < 1:
                    flag = False

            #For end of intron
            if strand == "+":
                other_siteJ = abs(real_start - siteJ[1])
            else:
                other_siteJ = abs(real_start - siteJ[0])
            other_dist = abs(other_siteJ - siteA)
            #print(other_siteJ,other_dist)
            if other_dist < minDist:
                flag = False

        #if not flag:
        #    print("removed in cross compare")

        if flag:
            tempsites.append(siteA)
    return tempsites



def analyze_peaks(setA, setB, minDist = 40, minPerc = 20):
    """
    connect a set of peaks
    """
    commonflag = []
    onlyA = []
    onlyB = []
    commons = []
    commoninB = []
    for peakA in setA:
        commonflag = False
        for peakB in setB:
            if abs(peakA.pos - peakB.pos) < minDist:
                commons.append((peakA, peakB))
                commonflag = True
                commoninB.append(peakB)
        if not commonflag :
            if peakA.score > minPerc:
                onlyA.append(peakA)

    for peakB in setB:
        if peakB not in commoninB:
            if peakB.score > minPerc:
                onlyB.append(peakB)

    return onlyA, onlyB, common

## This doesn't mean anything
def analyze_peaks_topbottom(topsetA, topsetB, othersetA, othersetB, minDist = 40, minPerc = 20):
    """
    connect a set of peaks,
    takes advantage of top and bottom info
    """
    commonflag = []
    onlyA = []
    onlyB = []
    commons = []
    commoninB = []


    for peakA in topsetA:
        commonflag = False
        for peakB in topsetB:
            if abs(peakA.pos - peakB.pos) < minDist:
                commons.append((peakA, peakB))
                commonflag = True
                commoninB.append(peakB)
        if not commonflag :
            if peakA.score > minPerc:
                onlyA.append(peakA)

    for peakB in setB:
        if peakB not in commoninB:
            if peakB.score > minPerc:
                onlyB.append(peakB)

    onlyAmatched = []
    onlyAnotmatched = []
    onlyBmatched = []
    onlyBnotmatched = []
    for peakA in onlyA:
        matched = False
        for peakB in othersetB:
            if abs(peakB.pos - peakA.pos) < minDist:
                onlyAmatched.append((peakA, peakB))
                matched = True
        if not matched:
            onlyAnotmatched.append(peakA)

    for peakB in onlyB:
        matched = False
        for peakA in othersetA:
            if abs(peakB.pos - peakA.pos) < minDist:
                onlyAmatched.append((peakA, peakB))
                matched = True
        if not matched:
            onlyBnotmatched.append(peakB)

    return onlyAnotmatched, onlyBnotmatched, onlyAmatched, onlyBmatched, commons





def compare_two_sets(setA, setP, addrest = False, minDist= const.MERGEMINDIST):
    yesset = []
    maybeset = []
    for siteA in setA:
        yesflag = False
        for siteP in setP:
            if abs(siteA-siteP) < minDist:
                yesset.append(siteP)
                yesflag  = True
                break
        if not yesflag:
        #if True:
            maybeset.append(siteA)
    if addrest :
        for siteP in setP:
            if siteP not in yesset:
                maybeset.append(siteP)


    return yesset, maybeset





def pipeline(pr_dict, ps_dict, used_ps_dict, gnamelist, gannot, sjstarts, sjends, tapaslastdict):
    """
    vis_gene right now is not efficient but the inputs are as follow:
    pr_dict: Dict of profiles
    ps_dict: Dict of polyA sites
    used_ps_dict: Dict of used polyA sites
    gnamelist: A list of gene names
    gannot: exon coordinates
    sjstarts: Reads on junction starts
    sjends: Reads on junction ends
    tapaslist: This is purely for evaluation right now
    """
    afterbeforeratio = []
    dists = []
    tapasdists = []
    for gname in gnamelist[:]:
        genechangepoints = []
        ginfo = pr_dict[gname]
        Ys = defaultdict(list)
        maxes = {}

        #chrm and strand
        coordpart1 = (gannot[gname][0], gannot[gname][1])
        gstart = gannot[gname][4]
        gend = gannot[gname][5]

        """
        The next couple of lines are used to scale the gene profiles
        So the profiles from different conditions are comparable.
        """
        for expr in ginfo:
            """
            Because I kept the coordinate and the coverage as list of tuples
            So they need to be separated.
            """
            Xs = [x[0] for x in ginfo[expr]]
            Ys[expr] = [x[1] for x in ginfo[expr]]
            maxes[expr] = max(1/100, sum(Ys[expr])/100)


        """
        Simply segmenting genes to segments of exon and downstream intron
        For further analysis.
        """
        segments = genes.segment_genes(Xs, Ys, gannot[gname][2], gannot[gname][3])

        #print("This gene was segmented")
        # Of course this line is just for test too
        if gname in used_ps_dict:
            changepoints = defaultdict(list)
            """
            We are looping over segments here.
            This is the part that we can parallelize
            """
            for segmentind in range(len(segments)-1, len(segments)):
                processflag = False
                segment = segments[segmentind]

                """
                See if this is the last exon
                """
                if segmentind == len(segments)-1:
                    lastexon = [1]
                else:
                    lastexon = [0]
                showflag = const.showflag
                plt.gcf().clear()
                covs = defaultdict(list)
                """
                This is coverage control.So raw numbers are used here and not the scaled ones.
                """
                if sum(segment[1]["sample1"])*1000/len(segment[1]["sample1"]) > 1 or sum(segment[1]["sample2"])*1000/len(segment[1]["sample2"]) >1:

                    """
                    start and end of this segment are in segment[0]
                    the coverage is in segment[1]
                    """
                    startnt = segment[0][0]
                    endnt = segment[0][-1]

                    """
                    This is mostly for test and visualization
                    If I have something there in the used sites we visualize it
                    """
                    for site in used_ps_dict[gname]:
                        if site <= endnt and site >= startnt:
                            tempsegment["usepasdict"].append(site)
                            processflag = True
                            if showflag:
                                plt.axvline(site, color="r")
                            #if len(changepoints) > 0:
                            #    dists.append(min(abs(site-changepoints["sample1"][0]), 2000))

                    exon_coords = []
                    """
                    Again mostly for visualization for now.
                    These are exon end and starts
                    Planning to use them later as features
                    """
                    for site in gannot[gname][2]:
                        if site < endnt and site >= startnt:
                            if showflag:
                                plt.axvline(site, color="k", ls='--', lw = 0.2)
                            exon_coords.append(site)
                    for site in gannot[gname][3]:
                        if site < endnt and site >= startnt:
                            if showflag:
                                plt.axvline(site, color="k", ls='--', lw = 0.2)
                            exon_coords.append(site)

                        exon_coords.sort()
                    strand = coordpart1[1]
                    """
                    Process Flag is for test right now
                    """
                    if processflag :
                        print(gname)
                        segmentforcomparison = defaultdict(list)
                        """
                        Here we just visualize the scaled profiles from different samples
                        """
                        for expr in segment[1]:
                            #covs[expr] = [i/maxes[expr] for i in segment[1][expr]]
                            covs[expr] = [i for i in segment[1][expr]]
                            if showflag:
                                plt.plot(segment[0], [i/maxes[expr] for i in segment[1][expr]], color = const.PR_COLORS[expr], alpha = 0.5)

                        print("Let's Smooth this")
                        tempchangepoints = defaultdict(list)
                        fl = defaultdict(list)
                        """
                        Sequencing data is noisy most of the times.
                        Here we use a butterworth(?) filter to smooth it out.
                        The parameters are used with test and try.
                        """
                        for expr in segment[1]:
                            b, a = butter(1, 60/len(segment[0]))
                            tempfl = filtfilt(b, a, [i/maxes[expr] for i in segment[1][expr]])
                            fl[expr] = filtfilt(b, a, tempfl)
                            if showflag:
                                plt.plot(segment[0], fl[expr], label = expr, color=const.PR_COLORS[expr])
                        print("Finding the very last one")

                        """
                        Somehow here I'm only trying to keep the last ones with different methods
                        """
                        minns, drops, maxslops = changepointsmethods.find_only_lastone(segment[0], fl)
                        #last_site_candidates = merge_candidates(minns, drops, maxslops)
                        #print(last_site_candidates,gstart-gend)
                        print("Last ones were found")

                        if const.CP_METHOD == "Binary":
                            sampledists = defaultdict(list)
                            """
                            Here Im changing the profile to distribution for some of the analysis
                            """
                            for expr in segment[1]:
                                for nt in range(len(segment[0])):
                                    #if segment[0][nt] < min(exon_coords[1]+1000, max(segment[0])):
                                    for freq in range(segment[1][expr][nt]):
                                    #for freq in range(int(fl[expr][nt]*maxes[expr])):
                                        segmentforcomparison[expr].append([segment[0][0]+nt])

                                with open(expr+"file.txt", 'w') as tformatlabfile:
                                    for hh in segment[1][expr]:
                                        tformatlabfile.write(str(hh) + "\n")
                                tformatlabfile.close()
                                arg1 = "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/" + expr + "file.txt"
                                command = "matlab  -nodesktop -nosplash  -r \"try; find_peaks(\'" + arg1  + "\'); catch; end;exit;\" $1 >> nothing $2>> copyrightmsg"
                                subprocess.call([command],shell=True);

                                pks = [i for i in map(int, open("pks.txt", "r").read().splitlines())]
                                locs = [i+segment[0][0] for i in map(int, open("locs.txt", "r").read().splitlines())]
                                widths = list(map(float, open("widths.txt", "r").read().splitlines()))
                                proms = list(map(float, open("proms.txt", "r").read().splitlines()))

                                pks = [i[0] for i in sorted(zip(pks, proms), key = lambda x : x[1], reverse=True)]
                                locs = [i[0] for i in sorted(zip(locs, proms), key = lambda x : x[1], reverse=True)]
                                widths = [i[0] for i in sorted(zip(widths, proms), key = lambda x : x[1], reverse=True)]
                                proms = sorted( proms, reverse=True)

                                for loc in locs[:2]:
                                    if showflag:
                                        plt.axvline(loc, label = expr, color="aqua")
                                print(pks[:2],locs[:2],widths[:2],proms[:2])
                                    #else:
                                    #    break
                                """
                                The next commented part was a test for gaussian mixture model
                                It's actually not bad
                                """


                                '''
                                maxloglike = float("-inf")
                                minbic = float("inf")
                                numcomp = -1
                                for nc in range(1,6):
                                    gmm = GaussianMixture(n_components=nc, covariance_type ="tied")
                                    gmm.fit(segmentforcomparison[expr])
                                    gmmeans = [int(m[0]) for m in gmm.means_]
                                    gmvar = [round(math.sqrt(mv),2) for mv in gmm.covariances_]
                                    gmw = [round(mw,2) for mw in gmm.weights_]
                                    gmmeansweight = sorted(zip(gmmeans, gmw), key = lambda x : x [0])
                                    #print("gmmeanW",gmmeansweight)
                                    loglike = gmm.score(segmentforcomparison[expr])
                                    thisbic = gmm.bic(np.array(segmentforcomparison[expr]))
                                    #if loglike > maxloglike:
                                    if thisbic < minbic:
                                        numcomp = nc
                                        #maxloglike = loglike
                                        minbic = thisbic
                                '''
                                #print(segmentforcomparison[expr])
                                #if len()segmentfo
                                '''
                                We are not really using this GM right now but we should
                                '''
                                gmm = GaussianMixture(n_components=5, covariance_type ="full")
                                gmm.fit(segmentforcomparison[expr])

                                gmvar = [round(math.sqrt(mv),2) for mv in gmm.covariances_]
                                gmmeans = [int(m[0]) for m in gmm.means_]
                                gmw = [int(mw*100) for mw in gmm.weights_]
                                gmmeansweight = sorted(zip(gmmeans, gmw), key = lambda x : x [0])
                                gmmeansvars = sorted(zip(gmmeans, gmvar), key = lambda x : x [0])

                                print("gmmeanW",gmmeansweight)
                                #print("var", gmvar)
                                print("gmvar", gmmeansvars)
                                #clusters=gmm.predict([[i] for i in segment[0]])
                                #pre = clusters[0]
                                #for ch in range(1,len(clusters)):
                                #    if clusters[ch]!=pre :
                                #        plt.axvline(ch + segment[0][0], color=const.PR_COLORS[expr])
                                #        pre=clusters[ch]

                                """
                                This is to visualize the means from mixture model
                                I also have the weights
                                But I'm not sure about the number of components
                                And it's to extensive to try a range of components
                                """
                                for me in gmm.means_:
                                    plt.axvline(me[0], color=const.PR_COLORS[expr], ls="--", lw=1)
                                sampledists[expr] = gmmeansweight
                                #algo = rpt.Window(model="l2", width=100, jump=5).fit(numpy.array([[iii] for iii in segment[1][expr][:-1]]))

                                """
                                This is kinda the main thing.
                                This is also for finding change points, but mo weights provided here
                                """
                                print("before algo")
                                algo = rpt.BottomUp(model="l2", jump=5).fit(numpy.array([[iii] for iii in fl[expr]]))
                                print("after algo")
                                #algo = rpt.BottomUp(model="l2", min_size=80, jump=10).fit(numpy.array([[iii] for iii in segment[1][expr][:-1]))
                                tempchangepoints[expr] = [ss for ss in algo.predict(pen=0.0001)][:-1] #n_bkps=2

                        changepoints = defaultdict(list)
                        """
                        Here we produce the coordinates of change points from relative ones
                        """
                        print("initial change points were found!")
                        for expr in tempchangepoints:
                            changepoints[expr] = [k+segment[0][0] for k in tempchangepoints[expr]]
                            changepoints[expr] = sorted(changepoints[expr], key = lambda x : x)
                        '''
                        plt.plot(segment[0], Ytree, alpha=0.8, c='purple')
                        '''
                        #plt.show()
                        # TODO:
                        """
                        Info on connect dist
                        """
                        print(connect_dists(sampledists["sample2"], sampledists["sample1"]))

                        changepoints = defaultdict(list)
                        temp = ""
                        keeptrack = []
                        """
                        Here we produce the genomic coords
                        And we put them in a bed file
                        """
                        for expr in tempchangepoints:
                            changepoints[expr] = [k+segment[0][0] for k in tempchangepoints[expr]]
                            changepoints[expr] = sorted(changepoints[expr], key = lambda x : x)
                            for site in changepoints[expr]:
                                chrm = coordpart1[0]
                                strand = coordpart1[1]
                                if strand == "+":
                                    upsite = gstart + site
                                else:
                                    upsite = gstart - site
                                keeptrack.append(site)
                                temp+= "\t".join([chrm, str(max(0,int(upsite)-200)), str(int(upsite)+200), \
                                                 chrm+strand+str(upsite),".", "+"])+"\n"
                        blacks = []
                        """
                        Im using bedtools to extract sequence too
                        """
                        tempbed = pybedtools.BedTool(temp, from_string=True)
                        tempseqfile = tempbed.sequence(fi=const.genomefa, s=True)
                        tempseq = open(tempseqfile.seqfn).read().splitlines()

                        """
                        We can look further into the sequence.
                        """
                        for thissiteind in range(len(temp.splitlines())):
                            #print(temp.splitlines()[thissiteind],tempseq[2*thissiteind+1])
                            notblackflag = False
                            for signal in ['ATTAAA', 'AATAAA']:
                                locs = [m.start() for m in re.finditer(signal, tempseq[2*thissiteind+1][50:250])]
                                if len(locs) > 0:
                                    notblackflag = True
                                    break
                            if not notblackflag:
                                blacks.append(keeptrack[thissiteind])
                        #print(changepoints)

                        '''
                        for expr in changepoints:
                            for site in changepoints[expr]:
                                if site not in blacks:
                                    plt.axvline(site, ls="-.", color = const.PR_COLORS[expr], label = expr)
                        '''
                        '''
                        prepoint = Ytree[0]
                        changepoints = []
                        for k in range(1, len(Ytree)-1):
                            if Ytree[k] < prepoint:
                                changepoints.append((k+segment[0][0], prepoint, Ytree[k]))
                            prepoint = Ytree[k]
                        changepoints = sorted(changepoints, key = lambda x : x[2])
                        '''
                        #print(changepoints)
                        #    rightsum += segment[1][k]
                        #    leftsum -= segment[1][k]
                        #    if (rightsum/k) > 4*(leftsum/(len(segment[1])-k)):
                        #        plt.axvline(segment[0][k], color="pink", ls ='--', lw=0.8)
                        #        break
                        passites = []
                        for site in ps_dict[gname]:
                            if site <= endnt and site >= startnt:
                                if showflag:
                                    plt.axvline(site, color="pink", ls ='--', lw=0.4)
                                passites.append(site)

                        """
                        This part is about the observed junction reads spanning over the junctions
                        """

                        jcreads = []
                        for nt in range(startnt, endnt+1):
                            if nt in sjends[gname] :
                                if showflag:
                                    plt.axvline(nt, color="pink", ls='-.', lw = 0.2)
                                jcreads.append((nt, sjends[gname][nt]))
                        for nt in range(startnt, endnt+1):
                            if nt in sjstarts[gname] :
                                if showflag:
                                    plt.axvline(nt, color="pink", ls='-.', lw = 0.2)
                                jcreads.append((nt, sjstarts[gname][nt]))

                        #if len(used_ps_dict[gname])> 0 :
                        #    minx = min(used_ps_dict[gname])
                        #    maxx = max(used_ps_dict[gname])
                        #    plt.xlim(minx-5000, maxx+5000)i

                        #if showflag:
                        print("start filtering...")
                        """
                        So we have a set of change points from each sample
                        Here are different filtering steps applied
                        """
                        # TODO
                        if len(changepoints["sample1"]) > 0 and len(changepoints["sample2"]) > 0:
                            filtered_changepoints, expr_info = changepointsmethods.evaluate_separate_changepoints(changepoints, passites, fl, startnt)
                            #for site in filtered_changepoints:
                                #if showflag:
                                #    plt.axvline(site, ls="-", color = "orange" , label = expr, alpha =0.7)
                            #if showflag :
                            #print(filtered_changepoints)
                            double_filtered_changepoints = changepointsmethods.compare_changepoints_withdrops(filtered_changepoints, minns, drops, maxslops)
                            #print(sorted(double_filtered_changepoints))
                            if showflag:
                                for site in double_filtered_changepoints:
                                    plt.axvline(site, ls="-.", color = "purple" , label = expr)
                            if showflag:
                                plt.legend()
                                plt.show()

                        triple_filtered_changepoints = double_filtered_changepoints
                        '''
                        for site in double_filtered_changepoints:
                            chrm = coordpart1[0]
                            strand = coordpart1[1]
                            if strand == "+":
                                upsite = gstart + site
                            else:
                                upsite = gstart - site
                            #keeptrack.append(site)
                            temp = "\t".join([chrm, str(max(0,int(upsite)-200)), str(int(upsite)+200), \
                                             chrm+strand+str(upsite),".", "+"])+"\n"
                            tempbed = pybedtools.BedTool(temp, from_string=True)
                            tempseqfile = tempbed.sequence(fi=const.genomefa, s=True)
                            tempseq = open(tempseqfile.seqfn).read().splitlines()
                            this_seq = tempseq[1]
                            triple_filtered_changepoints.append(site)

                            #triple_filtered_changepoints.append(site + extract_motifs(this_seq))
                        '''
                        '''
                        four_filtered_changepoints = []
                        for site in triple_filtered_changepoints:
                            jcflag = True
                            for k in jcreads:
                                if (site - k[0]) < 100 and (site -k[0])>0:
                                    #print(k, site, covs)
                                    if covs["sample2"][site-int(segment[0][0])] - int(k[1]) < 0.5*covs["sample2"][site-int(segment[0][0])]:
                                        jcflag = False
                                        break
                            if jcflag:
                                four_filtered_changepoints.append(site)
                        '''
                        four_filtered_changepoints = triple_filtered_changepoints
                        tempdist = []
                        temptapasdict = []
                        thereisone = False
                        for site in [sorted(used_ps_dict[gname])[-1]]:
                            if True:
                            #if abs(site - (abs(gstart-gend) - 5000)) > 100:
                                if site <= endnt and site >= startnt:
                                    if showflag:
                                        plt.axvline(site, color="r")
                                    if len(four_filtered_changepoints) > 0 :
                                        foundsite = sorted(four_filtered_changepoints)[-1]
                                        tempdist.append(abs(site-foundsite))
                                    if gname in tapaslastdict:
                                        if len(tapaslastdict[gname]) > 0:
                                            if strand == "+":
                                                #print(site, [i - gstart for i in tapaslastdict[gname]])
                                                #temptemp = []
                                                #for thistapa in tapaslastdict[gname]:
                                                #    temptemp.append(abs(site - (thistapa[0] - gstart)))
                                                #temptapasdict.append(min(temptemp))
                                                #temptapasdict.append(abs(site - (tapaslastdict[gname][-1][0] - gstart)))
                                                temptapasdict.append(abs(site-(max(tapaslastdict[gname])-gstart)))
                                            else:
                                                #temptemp = []
                                                #for thistapa in tapaslastdict[gname]:
                                                #    temptemp.append(abs(site - (gstart- thistapa[0])))
                                                #temptapasdict.append(min(temptemp))
                                                #print(site, [ gstart -i for i in tapaslastdict[gname]])
                                                #temptapasdict.append(abs(site - (gstart - tapaslastdict[gname][-1][0])))
                                                temptapasdict.append(abs(site-(gstart- min(tapaslastdict[gname]))))

                                thereisone = True
                        if len(tempdist) > 0 and thereisone:
                            dists.append(min(tempdist))
                        elif len(tempdist) == 0 and  thereisone:
                            dists.append(-100)

                        if len(temptapasdict) > 0 and thereisone:
                            tapasdists.append(min(temptapasdict))
                        elif len(temptapasdict) == 0 and thereisone:
                            #print(gname)
                            tapasdists.append(-100)
                        #genechangepoints.append(prepare_features(double_filtered_changepoints, expr_info, covs, chrm, strand, gstart, startnt, exon_coords, lastexon))

            #print(len(genechangepoints[0]))
            #pair_up(genechangepoints[0])
        plt.gcf().clear()

    print(len(dists))
    closest = 0
    tclosest = 0
    notexist = 0
    tnotexist = 0
    for kd in dists:
        if kd < 100 and kd >= 0:
            closest+=1
        if kd < 0:
            notexist+=1
    for kd in tapasdists:
        if kd < 100 and kd >= 0:
            tclosest+=1
        if kd < 0:
            tnotexist+=1

    print(closest, notexist, tclosest, tnotexist)
    plt.hist((dists,tapasdists), 50, range=[-200,2000], normed=False, color = ["salmon", "grey"], alpha=0.7, label = ["this", "other"])
    #plt.hist(afterbeforeratio,50, alpha=0.8, normed=True)
    plt.legend()
    plt.show()

# To merge the distribution found i
def connect_dists(sample1_dists, sample2_dists):
    matched_means = defaultdict(list)
    for this_dist in sample2_dists:
        minfasel = 1000000
        for that_dist in sample1_dists:
            if abs(this_dist[0] - that_dist[0])< minfasel:
                minfasel = abs(this_dist[0] - that_dist[0])
                closestdist = that_dist
        matched_means[closestdist].append((this_dist, minfasel))
    pairs =[]
    for that_dist in sample1_dists:
        closest_dists = sorted(matched_means[that_dist], key = lambda x : x[1])
        for this_other in closest_dists:
            pairs.append((that_dist, this_other[0], that_dist[1] - this_other[0][1]))

    peaks_from1 = defaultdict(list)
    peaks_from2 = defaultdict(list)

    for peak_1 in sample1_dists:
        for peak_2 in sample2_dists:
            if abs(peak_1[0] - peak_2[0]) < 150:
                print(peak_1, peak_2)
                peaks_from1[peak_1].append(peak_2)
    for peak_2 in sample2_dists:
        for peak_1 in sample1_dists:
            if abs(peak_1[0] - peak_2[0]) < 150:
                if peak_2 not in peaks_from1[peak_1]:
                    peaks_from2[peak_2].append(peak_1)

    '''
    final_peaks = []
    for pair in pairs:
        if pair[-1] < 150:
            final_peaks.append(((int(0.5*(pair[0][0] +pair[1][0]))), (pair[0][1], pair[1][1])))
    '''
    print(peaks_from1)
    print(peaks_from2)
    #print(final_peaks)
    print(pairs)
    if pairs[0][-1] < 0:
        pre_trend = "N"
    else:
        pre_trend = "P"
    pre_perc = [pairs[0][0][1], pairs[0][1][1]]
    already_checked = [pairs[0][0][0], pairs[0][1][0]]
    last = max(pairs[0][0][0], pairs[0][1][0])
    lasts = [last]
    percs = [pre_perc]
    #segment1 = True
    #segment2 = False
    post_perc = [0,0]
    for pair in pairs[1:]:
        if pair[-1] < 0 :
            this_trend = "N"
        else:
            this_trend = "P"
        if this_trend == pre_trend or abs(pair[-1]) < 2:
            if pair[0][0] not in already_checked:
                percs[-1][0] += pair[0][1]
                already_checked.append(pair[0][0])
            if pair[1][0] not in already_checked:
                percs[-1][1] += pair[1][1]
                already_checked.append(pair[1][0])
            last = max(pair[0][0], pair[1][0])
            lasts[-1] = last
        elif this_trend != pre_trend:
            percs.append([0,0])
            if pair[0][0] not in already_checked:
                percs[-1][0] += pair[0][1]
                already_checked.append(pair[0][0])
            if pair[1][0] not in already_checked:
                percs[-1][1] += pair[1][1]
                already_checked.append(pair[1][0])
            pre_trend = this_trend
            last = max(pair[0][0], pair[1][0])
            lasts.append(last)

    return lasts, percs

# To merge the distribution found i
def connect_dists_orig(sample1_dists, sample2_dists):
    matched_means = defaultdict(list)
    for this_dist in sample2_dists:
        minfasel = 1000000
        for that_dist in sample1_dists:
            if abs(this_dist[0] - that_dist[0])< minfasel:
                minfasel = abs(this_dist[0] - that_dist[0])
                closestdist = that_dist
        matched_means[closestdist].append((this_dist, minfasel))
    pairs =[]
    for that_dist in sample1_dists:
        closest_dists = sorted(matched_means[that_dist], key = lambda x : x[1])
        for this_other in closest_dists:
            pairs.append((that_dist, this_other[0], that_dist[1] - this_other[0][1]))
    print(pairs)
    if pairs[0][-1] < 0:
        pre_trend = "N"
    else:
        pre_trend = "P"
    pre_perc = [pairs[0][0][1], pairs[0][1][1]]
    already_checked = [pairs[0][0][0], pairs[0][1][0]]
    last = max(pairs[0][0][0], pairs[0][1][0])
    lasts = [last]
    percs = [pre_perc]
    #segment1 = True
    #segment2 = False
    post_perc = [0,0]
    for pair in pairs[1:]:
        if pair[-1] < 0 :
            this_trend = "N"
        else:
            this_trend = "P"
        if this_trend == pre_trend or abs(pair[-1]) < 2:
            if pair[0][0] not in already_checked:
                percs[-1][0] += pair[0][1]
                already_checked.append(pair[0][0])
            if pair[1][0] not in already_checked:
                percs[-1][1] += pair[1][1]
                already_checked.append(pair[1][0])
            last = max(pair[0][0], pair[1][0])
            lasts[-1] = last
        elif this_trend != pre_trend:
            percs.append([0,0])
            if pair[0][0] not in already_checked:
                percs[-1][0] += pair[0][1]
                already_checked.append(pair[0][0])
            if pair[1][0] not in already_checked:
                percs[-1][1] += pair[1][1]
                already_checked.append(pair[1][0])
            pre_trend = this_trend
            last = max(pair[0][0], pair[1][0])
            lasts.append(last)

    return lasts, percs

#def look_around(pos, area):
#    for nt in range(pos-area,pos+area):




#def look_around(suggested_pos, sequence, closest_splicing):

'''
    #First do a round of mixtures
    gmm = GaussianMixture(n_components=1, covariance_type ="full")
    gmm.fit(segmentforcomparison[expr])
    gmvar = [round(math.sqrt(mv),2) for mv in gmm.covariances_]
    gmmeans = [int(m[0]) for m in gmm.means_]
    gmw = [int(mw*100) for mw in gmm.weights_]
    gmmeansweight = sorted(zip(gmmeans, gmw), key = lambda x : x [0])
    gmmeansvars = sorted(zip(gmmeans, gmvar), key = lambda x : x [0])
    #print("gmmeanW",gmmeansweight)
    #print("gmvar", gmmeansvars)
    #clusters=gmm.predict([[i] for i in segment[0]])
    #pre = clusters[0]
    for ch in range(1,len(clusters)):
        if clusters[ch]!=pre :
            plt.axvline(ch + segment[0][0], color=const.PR_COLORS[expr])
            pre=clusters[ch]

    for me in gmm.means_:
        plt.axvline(me[0], color=const.PR_COLORS[expr], ls="--", lw=1)
    sampledists[expr] = gmmeansweight

'''

#def merge_up_and_last():

#def extract_feats():

#def score_bests():

#def learn_from_bests():

#def rediscover_others():

##Initialization

'''
pasfile = open(sys.argv[1],'r').read().splitlines() #Table of All sites
#covfiledir = sys.argv[2]
genedir =  "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/" + sys.argv[2]  #Bed annotation
tempbamdirs = sys.argv[3].split(",")  #bam dirs
bamdirs = {"sample1": tempbamdirs[1], "sample2" : tempbamdirs[0]}
usedpasfile = open(sys.argv[4],'r').read().splitlines() #Table of sites with counts
ginfo = genes.parse_geneinfo(sys.argv[5]) #gtf annotation
jinfo = "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/"+sys.argv[6]
jbedfiledir = jinfo + ".bed"

##Parse files
paslist, nameiddict = polya.parse_pasfile(pasfile)
polya.parse_polyA_file(usedpasfile)
usedpaslist = polya.parse_pasfile_withcov(usedpasfile)
print("PASs are parsed")

tapasfile = open(sys.argv[7], 'r').read().splitlines()
tapaslastdict = evaluate.compare_with_TAPAS(tapasfile, nameiddict)
# I make a mistake of getting coverage here and not before.
#profiledict, pasdict = gen_profile(covfiledir, paslist)
genes.reformat_junctionf(jinfo, jbedfiledir)
read_on_start, read_on_end, gjdict = genes.map_junctionongenes(jbedfiledir, genedir)

print("Junctions are parsed")

profiledict, pasdict, usedpasdict, thisglistforvis, sjstartdict, sjenddist = \
        genes.gen_profile_bygene(genedir, bamdirs, paslist, usedpaslist, read_on_start, read_on_end)

print("profiles were generated")

pipeline(profiledict, pasdict, usedpasdict, thisglistforvis, ginfo,  sjstartdict, sjenddist, tapaslastdict)
'''
