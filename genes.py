import sys
import multiprocessing
from collections import defaultdict
import pybedtools
import subprocess
import os
import numpy as np
from joblib import Parallel, delayed
import countAAAreads
from sys import getsizeof
import const
import gc
import logging


class GENES():
    def __init__(self):
        self.segmentsfeats = defaultdict(dict)
        self.read_on_start = defaultdict(dict)
        self.read_on_end = defaultdict(dict)
        self.sjdict = defaultdict(dict)
        self.allgenecoverages = defaultdict(dict)
        self.allsegmentcoverages = defaultdict(dict)
        self.testspdict = defaultdict(dict)
        self.spdict = None
        self.supdict = None
        self.processedgenelist = []
        self.allaaareads = defaultdict(dict)
        self.utrsegments = []

    def reformat_junctionf(self, jfiledir, jbedfiledir):
        jfile = open(jfiledir, 'r')
        sjlist = defaultdict(list)
        with open(jbedfiledir,'w') as jbedfile:
            for line in jfile:
                fds = line.strip().split()
                chrom = fds[0]
                first_base = int(fds[1])
                last_base = int(fds[2])
                strandnum = fds[3]
                if strandnum == "1":
                    strand = "+"
                elif strandnum == "2":
                    strand = "-"
                motiftype = fds[4]
                newornot = fds[5]
                readcount = fds[6]
                mulreadcount = fds[7]
                templine = "\t".join([chrom, str(first_base), str(last_base), \
                    motiftype + "|" + newornot + "|" + readcount + "|" + mulreadcount, \
                    ".", strand]) + "\n"
                jbedfile.write(templine)

    def map_junctionongenes(jbedfiledir, gfiledir):
        # This will eat up all your memory
        jinitfile = pybedtools.example_bedtool(jbedfiledir)
        sjinitfile = jinitfile.sort()
        ginitfile = pybedtools.example_bedtool(gfiledir)
        sginitfile = ginitfile.sort()
        # Intersection of junctions and genes
        j_on_g = sjinitfile.intersect(sginitfile, wo=True, s=True)
        gjdict= defaultdict(list)
        # Assign hunctions to a dict
        for ins in j_on_g:
            fds = str(ins).split()
            chrom, first_base, last_base, strand = fds[0], int(fds[1]), int(fds[2]), fds[5]
            motiftype, newornot, readcount = fds[3].split("|")
            gene = fds[9]
            gjdict[gene].append((first_base, last_base, motiftype, newornot, readcount))
        read_on_start = defaultdict(dict)
        read_on_end = defaultdict(dict)
        # But I also need to merge the cutting sites
        for thisg in gjdict:
            read_on_start[thisg] = {}
            read_on_end[thisg] = {}
            splice_sites = sorted(gjdict[thisg], key = lambda x : x[0])
            for ss in splice_sites:
                if ss[0] not in read_on_start[thisg]:
                    read_on_start[thisg][ss[0]] = int(ss[4])
                else:
                    read_on_start[thisg][ss[0]] += int(ss[4])
                if ss[0] not in read_on_end[thisg]:
                    read_on_end[thisg][ss[1]] = int(ss[4])
                else:
                    read_on_end[thisg][ss[1]] += int(ss[4])
        return read_on_start, read_on_end, gjdict


    def parse_geneinfo(annotfile):
        ginfodict = {}
        with open(annotfile, 'r') as annot:
            for line in annot:
                fds = line.split()
                gname = fds[3]

                starts = list(map(int, fds[11].split(",")[:-1]))
                lens = list(map(int, fds[10].split(",")[:-1]))
                if fds[5] == "+":
                    shift = 0
                    gstart = fds[1]
                    gend = int(fds[2]) + 5000
                else:
                    shift = 5000
                    gstart = fds[2]
                    gend = int(fds[1]) -5000
                starts = [i+shift for i in starts]
                ends = [starts[i] + lens[i]  for i in range(len(starts))]
                if fds[5] == "-":
                    ref = ends[-1]
                    startscopy = starts
                    starts = [ref-i for i in ends[::-1]]
                    ends = [ref-i for i in startscopy[::-1]]
                #chrom, strand, exon starts and ends, gene start and end
                ginfodict[gname] = (fds[0], fds[5],  starts, ends, int(gstart), gend)

        return ginfodict


    def gen_profile(prfiledir, plist):
        '''
        To make profiles out of bed coverage output
        You should have ran that liek this:
        bedtools coverage -a my_region.bed -b Rep1.bam -bed -d -s
        '''
        profile_dict = defaultdict(list)
        pas_dict = defaultdict(list)
        line_counter = 0
        with gzip.open(prfiledir, 'r') as prfile:
            for line in prfile:
                line_counter += 1
                fds = line.strip().split()
                gname = fds[3]
                readcount = int(fds[-1])
                posong = int(fds[-2])
                coord = fds[0] + fds[5] + fds[2] #chrx+100
                if coord in plist[gname]:
                    pas_dict[gname].append(posong)
                profile_dict[gname].append((posong, readcount))
                if line_counter % 1000000 == 0 :
                    print(str(line_counter), "  was parsed from coverage file.")
            return profile_dict, pas_dict


    def gen_profile_bygene(genedir, bamfiledirs, plist, usedplist, jstarts, jends):
        # For each experiment we keep a list as coverage in profile_dict
        profile_dict = defaultdict(dict)

        # This is here just for test. All the pas in masterlist
        pas_dict = defaultdict(list)

        # Just for test too. Pas with some specific criteria from specific experiment
        used_pas_dict = defaultdict(list)

        #starts and ends of annotated exons
        j_startsdict = defaultdict(dict)
        j_endsdict = defaultdict(dict)

        glistforvis = []
        gene_counter = 0

        with open(genedir, 'r') as gfile:
            for line in gfile:
                #gene_counter += 1
                fds = line.split()
                if fds[3] in plist and fds[0] == "chr1":
                    gname = fds[3]
                    if fds[5] == "+":
                        region = fds[0] + ":"+ fds[1] + "-" + str(int(fds[2])+5000)
                        start = int(fds[1])
                        end = int(fds[2])+5000
                    else:
                        region = fds[0] + ":"+ str(int(fds[1])-5000) + "-" + fds[2]
                        start = int(fds[1])-5000
                        end = int(fds[2])
                    #covs = [(pos,0) for pos in range(end-start)]

                    j_startsdict[gname] = {}
                    j_endsdict[gname] = {}
                    alltempnums = defaultdict(dict)
                    covs = defaultdict(list)

                    for coord in plist[gname]:
                        #print(coord)
                        if "+" in coord:
                            scoord = "+"
                        else:
                            scoord = "-"
                        coordpos = coord.split(scoord)[1]
                        pas_dict[gname].append(int(coordpos)-start)
                        if coord in usedplist:
                            used_pas_dict[gname].append(int(coordpos)-start)


                    ## This F is just for test
                    #if True:
                    if len(used_pas_dict[gname]) > 0:
                        gene_counter += 1
                        for expr in bamfiledirs:
                            this_command = "samtools depth -r \"" + region + "\" " + str(bamfiledirs[expr])
                            output = subprocess.Popen(this_command, shell=True, stdout=subprocess.PIPE).stdout.read()
                            tempnums = {}
                            for templine in output.splitlines():
                                tempfds = templine.split()
                                tempnums[int(tempfds[1])] = int(tempfds[2])
                            alltempnums[expr] = tempnums
                            covs[expr] = [(pos,0) for pos in range(end-start)]

                        for i in range(end-start):
                            for expr in alltempnums:
                                if start+i in alltempnums[expr]:
                                    covs[expr][i] = (i, alltempnums[expr][start+i])
                            #if fds[0] + fds[5] + str(start+i) in plist[gname]:
                            #    pas_dict[gname].append(i)
                            #if fds[0] + fds[5] + str(start+i) in usedplist:
                            #    used_pas_dict[gname].append(i)
                            if fds[5] == "+" :
                                if start+i in jstarts[gname]:
                                    j_startsdict[gname][i] = jstarts[gname][start+i]
                                if start+i in jends[gname] :
                                    j_endsdict[gname][i] = jends[gname][start+i]
                            elif fds[5] == "-" :
                                if start+i in jends[gname]:
                                    j_startsdict[gname][end-i] = jends[gname][start+i]
                                if start+i in jstarts[gname] :
                                     j_endsdict[gname][end-i] = jstarts[gname][start+i]
                        if fds[5] == "-":
                            inds = [i[0] for i in covs[expr]]
                            for expr in covs:
                                nums = [i[1] for i in covs[expr][::-1]]
                                covs[expr] = [(inds[i], nums[i]) for i in range(len(covs[expr]))]
                            pas_dict[gname] = np.array([max(inds)-i for i in pas_dict[gname]], dtype=int)
                            used_pas_dict[gname] = np.array([max(inds)-i for i in used_pas_dict[gname]], dtype=int)

                        profile_dict[gname] = covs
                        glistforvis.append(gname)
                        #if len(used_pas_dict[gname]) > 0:
                        #    print(gname)
                    print("genecounter", gene_counter)
                    if gene_counter % 100 == 0:
                        print(gene_counter)
                    #if fds[0] != "chr1":
                    #    break
                    if gene_counter >= 10 :
                        print(str(gene_counter), "  genes were parsed from coverage file.")
                        break
        return profile_dict, pas_dict, used_pas_dict, glistforvis, j_startsdict, j_endsdict


    def segment_genes(Xs, Ys, startes, endes):
        '''
        Here start and end come from annotation
        So I mean start or end of exons
        I will write another function with start and end of sjs
        '''
        allparts = []
        posone= 0
        thispart = defaultdict(list)
        nt = 0
        #if strand == "-" :
        #    ref = endes[-1]
        #    startes = [ref-i for i in startes[::-1]]
        #    endes = [ref-i for i in endses[::-1]]
        #    Ys = Ys[::-1]
        thisstarte = startes[0]
        if len(startes) > 1:
            thisende = startes[1]
        else:
            thisende = len(Xs)-1

        while nt < len(Xs) :
            if nt <= thisende:
                for expr in Ys:
                    thispart[expr].append(Ys[expr][nt])
                nt+=1
            elif posone < len(startes)-1:
                posone+=1
                thistarte = startes[posone]
                if len(startes) > posone+1:
                    thisende = startes[posone+1]
                else:
                    thisende = len(Xs)-1
                for this_key in thispart:
                    thisreglen = len(thispart[this_key])
                thisXs = range(nt-thisreglen, nt)
                allparts.append((thisXs,thispart))
                nt = thistarte
                thispart = defaultdict(list)

        for this_key in thispart:
            thisreglen = len(thispart[this_key])
        thisXs = range(nt-thisreglen, nt)
        allparts.append((thisXs,thispart))
        return allparts


    def segment_exons_upanddown(this_segment, possible_psites):
        """
        After predicting some sites, this function is supposed to help
        to get a better value that represents the the polyA sites on the exons.
        """
        # TODO:
        # Multiple options:
        # upstream only on intronic region, or upstream also covers exon
        ratios = {}
        for this_site in range(len(possible_psites)):
            up_len = len(this_segment[:this_site])
            down_len = len(this_segment[this_site:])
            # TODO:
            # Not sure if the normalization by length is necessary
            this_upstream = sum(this_segment[:this_site])  #/ up_len
            this_downstream = sum(this_segment[this_site:]) #/ down_len
            this_ratio = this_upstream - this_downstream
            ratios[this_site] = this_ratio

        return ratios

    def segment_introns_upanddown(this_segment, possible_psites, exon_endpos):
        """
        After predicting some sites, this function is supposed to help to avoid intron retention
        """
        # TODO:
        # Multiple options:
        # upstream only on intronic region, or upstream also covers exon
        ratios = {}
        for this_site in range(len(possible_psites)):
            up_len = len(this_segment[exon_endpos:this_site])
            down_len = len(this_segment[this_site:])
            # TODO:
            # Not sure if the normalization by length is necessary
            this_upstream = sum(this_segment[exon_endpos:this_site])  #/ up_len
            this_downstream = sum(this_segment[this_site:]) #/ down_len
            this_ratio = this_upstream - this_downstream
            ratios[this_site] = this_ratio

        return ratios

    def calculate_ratio_perupstreamexon(
                                        this_segment, possible_psites,
                                        exon_endpos, next_segment, nextexon_endpos
                                        ):
        """
        After predicting some sites, this function is supposed
        to help with calculating the ratio to upstream exon.
        """
        toupratios = {}
        todownratios = {}
        upstream_exon = sum(this_segment[:exon_endpos])
        downstream_exon = sum(next_segment[:nextexon_endpos])
        for this_site in range(len(possible_psites)):
            up_len = len(this_segment[exon_endpos:this_site])
            down_len = len(this_segment[this_site:])
            # TODO:
            # Not sure if the normalization by length is necessary
            this_upstream = sum(this_segment[exon_endpos:this_site])  #/ up_len
            this_downstream = sum(this_segment[this_site:]) #/ down_len
            this_ratio = this_upstream - this_downstream
            toupratios[this_site] = this_ratio/upstream_exon
            todownratios[this_site] = this_ratio/downstream_exon

        return toupratios, todownratios


    def splicedReads_method1():
        """
        Report reads spliced at each site
        """
    ################################################################################################
    ################################################################################################
    ################################################################################################
    def profile_process(self, geneannotfile, segmentfiledir, bamfilesdir, covfiles = None ,
                        jfiledir = None, jbedfiledir = None, paslistdir = None,
                        test_knownpasdir = None, aaareaddirs = None,
                        usedpasfiledir = None, utrfiledir = None):

        #print(bamfilesdir)
        extendedbedfile = "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/extendedgenes.bed"
        extensiondict  = self.extend_genes(geneannotfile,
        "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/tempsixcolumnsgenes.bed",
        extendedbedfile)

        #self.get_gene_coverage(extendedbedfile , bamfilesdir, covfiles )
        #print("coverage calculated")
        self.processedgenelist = self.parse_thiscov_forgene_bigwig(extendedbedfile , covfiles)
        #self.processedgenelist = self.parse_thiscov_forgene(covfiles )

        print("coverage calculated")

        self.segment_allgenes_tobed(geneannotfile, segmentfiledir, extensiondict)
        print("genes were segmented to exons and downstream intron")

        self.trim_gene()
        print("genes were trimmed")

        self.filtersegbyutr(utrfiledir, segmentfiledir)
        #TODO:
        # Replicates
        if jfiledir is not None:
            for sample in self.allgenecoverages:
                print("junction information was available")
                self.reformat_junctionf(jfiledir[sample][0], jbedfiledir[sample][0])
                print("junction file was reformated")
                self.mapjunctionsonsegments(sample, jbedfiledir[sample][0], segmentfiledir)
                print("junction information was mapped to genes")

        if paslistdir is not None:
            print("List of pas sites was available")
            self.spdict = defaultdict(dict)
            self.mapknownpasonsegments(paslistdir, segmentfiledir, test = False)
            print("These pas sites were mapped to genes")

        if test_knownpasdir is not None:
            print("known pas information was available")
            self.mapknownpasonsegments(test_knownpasdir, segmentfiledir, test = True)
            print("known pas information was mapped to genes")


        if aaareaddirs is not None:
            print("AAA reads are here! Hurray!")
            self.allaaareads = countAAAreads.aaa_process(aaareaddirs, extendedbedfile)

        if usedpasfiledir is not None:
            print("Used pas files are available for test")
            self.supdict = defaultdict(dict)
            self.mapusedpasonsegments(usedpasfiledir, segmentfiledir)

    ################################################################################################
    ################################################################################################
    ################################################################################################
    def extend_genes(self, gfiledir, gtempfiledir, gextfile):
        with open(gtempfiledir, 'w') as gtempdir:
            with open(gfiledir, 'r') as gfile:
                for line in gfile:
                    temp = "\t".join(line.split()[:6])
                    gtempdir.write(temp + "\n")
        gtempdir.close()
        ginitfile = pybedtools.example_bedtool(gtempfiledir)
        sginitfile = ginitfile.sort()
        extensiondict = {}
        #find closest genes downstream
        closestdown = sginitfile.closest(sginitfile, iu=True, io=True, s=True, D="a", d=True)
        with open(gextfile, "w") as gef:
            for pair in closestdown:
                gene1chrom, gene1start, gene1end, gene1name, gene1info, gene1strand, \
                gene2chrom, gene2start, gene2end, gene2name, gene2info, gene2strand, dist = str(pair).split()
                dist = int(dist)
                if gene2chrom != "." :
                    dist = min(dist, const.GENEXT)
                else:
                    dist = const.GENEXT
                extensiondict[gene1name] = dist
                if gene1strand == "+":
                    extgene = "\t".join([gene1chrom, gene1start, str(int(gene1end) + dist),
                                gene1name, gene1info, gene1strand] )
                else:
                    extgene = "\t".join([gene1chrom, str(int(gene1start)-dist), gene1end,
                                gene1name, gene1info, gene1strand])
                gef.write(extgene+"\n")


        return extensiondict

    def segment_genes_tobed(self, genename, genechrom, genestart, geneend, extension , genestrand, startes, lens):
        '''
        Here start and end come from annotation
        So I mean start or end of exons
        I will write another function with start and end of sjs
        '''
        segmentbed = ""
        segmentfeatures = defaultdict(list)
        segmentlengs = []
        if genestrand  == "+":
            for this_ind in range(len(startes)):
                if this_ind!= len(startes) - 1:
                    end = genestart+startes[this_ind+1]-1
                else:
                    end = geneend + extension
                segmentbed += "\t".join([genechrom, str(genestart+startes[this_ind]), str(end),
                                        genename+"|"+str(this_ind), ".", genestrand]) + "\n"
                segmentfeatures[this_ind] = [genechrom, genestart+startes[this_ind],
                                               genestart+startes[this_ind]+lens[this_ind], end, genestrand]
                segmentlengs.append(end - genestart-startes[this_ind])


        elif genestrand == "-":
            for this_ind in range(len(startes)):
                if this_ind!= 0 :
                    start = genestart + startes[this_ind-1] +lens[this_ind-1]
                else:
                    start = genestart - extension
                segmentbed += "\t".join([genechrom, str(start ) ,
                                        str(genestart + startes[this_ind] - 1 + lens[this_ind]),
                                        genename+"|"+str(len(startes)-this_ind - 1 ), ".", genestrand])+"\n"

                segmentfeatures[len(startes) - this_ind - 1] = [genechrom, start ,
                                                     genestart + startes[this_ind] - 1  ,
                                                     genestart + startes[this_ind] - 1  + lens[this_ind],
                                                     genestrand]
                segmentlengs.append( genestart + startes[this_ind] - 1 + lens[this_ind] -start)
        return segmentbed, segmentfeatures, segmentlengs

    def segment_allgenes_tobed(self, geneannotfile, segmentfilesdir, extensiondict):
        for sample in self.allgenecoverages:
            self.allsegmentcoverages[sample] = defaultdict(list)
        genedict = defaultdict(list)
        genecounter = 0
        with open(segmentfilesdir, 'w') as segmentfile:
            with open(geneannotfile,'r') as gannot:
                for line in gannot:
                    genecounter += 1
                    fds = line.split()
                    if fds[3] in self.processedgenelist:
                        chrm = fds[0]
                        start = int(fds[1])
                        end = int(fds[2])
                        strand = fds[5]
                        starts = list(map(int, fds[11].split(",")[:-1]))
                        lens = list(map(int, fds[10].split(",")[:-1]))
                        genename = fds[3]
                        genedict[fds[3]] = [chrm, start, end, strand, starts, lens]
                        extension = extensiondict[genename]
                        bedstr, self.segmentsfeats[genename], segmentlengs = \
                                                            self.segment_genes_tobed(genename, chrm,
                                                            start, end, extension , strand, starts, lens)


                        for sample in self.allgenecoverages:
                            genecoverage = self.allgenecoverages[sample][genename]
                            slind = 0
                            thisstart = 0
                            segmentcoverages = [[] for sl in segmentlengs]
                            for segmentleng in segmentlengs:
                                segmentcoverages[slind] = np.array(genecoverage[thisstart : thisstart + segmentleng])
                                slind += 1
                                thisstart = thisstart + segmentleng
                            if strand  == "+" :
                                self.allsegmentcoverages[sample][genename] = segmentcoverages
                            else:
                                self.allsegmentcoverages[sample][genename] = segmentcoverages[::-1]
                            #print(genename, [i[:5] for i in self.allsegmentcoverages[sample][genename]])
                        if genecounter %15000 == 0:
                            print(genecounter, " genes were segmented sucessfully")
                        segmentfile.write(bedstr)

    def mapjunctionsonsegments(self, sample, jbedfiledir, segmentfiledir):

        self.sjdict[sample] = defaultdict(dict)

        # This will eat up all your memory
        jinitfile = pybedtools.example_bedtool(jbedfiledir)
        sjinitfile = jinitfile.sort()
        sinitfile = pybedtools.example_bedtool(segmentfiledir)
        ssinitfile = sinitfile.sort()

        # Intersection of junctions and genes
        j_on_s = sjinitfile.intersect(ssinitfile, wo=True, s=True)

        # Assign hunctions to a dict
        for ins in j_on_s:
            fds = str(ins).split()
            chrom, first_base, last_base, strand = fds[0], int(fds[1]), int(fds[2]), fds[5]
            motiftype, newornot, readcount, mulreadcount = fds[3].split("|")
            gene, segmentid = fds[9].split("|")
            if gene not in self.sjdict[sample]:
                self.sjdict[sample][gene] = defaultdict(list)
            if segmentid not in self.sjdict[sample][gene]:
                self.sjdict[sample][gene][int(segmentid)] = []
            self.sjdict[sample][gene][int(segmentid)].append((int(first_base), int(last_base), motiftype, newornot, int(readcount), int(mulreadcount)))

        '''
        read_on_start = defaultdict(dict)
        read_on_end = defaultdict(dict)

        # But I also need to merge the cutting sites
        for thisg in self.sjdict[sample]:
            read_on_start[thisg] = defaultdict(dict)
            read_on_end[thisg] = defaultdict(dict)
            for thiss in self.sjdict[sample][thisg]:
                read_on_start[thisg][thiss] = {}
                read_on_end[thisg][thiss] = {}
                splice_sites = sorted(self.sjdict[sample][thisg][thiss], key = lambda x : x[0])
                for ss in splice_sites:
                    if ss[0] not in read_on_start[thisg][thiss]:
                        read_on_start[thisg][thiss][ss[0]] = int(ss[4])
                    else:
                        read_on_start[thisg][thiss][ss[0]] += int(ss[4])
                    if ss[0] not in read_on_end[thisg][thiss]:
                        read_on_end[thisg][thiss][ss[1]] = int(ss[4])
                    else:
                        read_on_end[thisg][thiss][ss[1]] += int(ss[4])
        return read_on_start, read_on_end
        '''
    def mapknownpasonsegments(self, pasfiledir, segmentfiledir, test):
        # This will eat up all your memory
        pinitfile = pybedtools.example_bedtool(pasfiledir)
        spinitfile = pinitfile.sort()
        sinitfile = pybedtools.example_bedtool(segmentfiledir)
        ssinitfile = sinitfile.sort()

        # Intersection of junctions and genes
        p_on_s = spinitfile.intersect(ssinitfile, wo=True, s=True)
        for ins in p_on_s:
            fds = str(ins).split()
            chrom, first_base, last_base, strand = fds[0], int(fds[1]), int(fds[2]), fds[5]
            gene, segmentid = fds[9].split("|")
            if test:
                if gene not in self.testspdict:
                    self.testspdict[gene] = defaultdict(set)
                self.testspdict[gene][int(segmentid)].add(first_base)
            else:
                if gene not in self.spdict:
                    self.spdict[gene] = defaultdict(set)
                self.spdict[gene][int(segmentid)].add(first_base)


    def filtersegbyutr(self, utrfiledir, segmentfiledir):
        sinitfile = pybedtools.example_bedtool(segmentfiledir)
        ssinitfile = sinitfile.sort()
        uinitfile = pybedtools.example_bedtool(utrfiledir)
        usinitfile = uinitfile.sort()
        s_on_utr = ssinitfile.intersect(usinitfile, u=True, s=True)
        for ins in s_on_utr:
            fds = str(ins).split()
            gene, segmentid = fds[3].split("|")
            self.utrsegments.append((gene, int(segmentid)))


    def mapusedpasonsegments(self, usedpasfiledir, segmentfiledir):
        # This will eat up all your memory
        ## TODO:
        # Take care of reps
        for sample in const.SAMPLES:
            usedpasfile = usedpasfiledir[sample]
            print(usedpasfile)
            pinitfile = pybedtools.example_bedtool(usedpasfile[0])
            spinitfile = pinitfile.sort()
            sinitfile = pybedtools.example_bedtool(segmentfiledir)
            ssinitfile = sinitfile.sort()
            self.supdict[sample] = defaultdict(dict)
            # Intersection of junctions and genes
            p_on_s = spinitfile.intersect(ssinitfile, wo=True, s=True)
            for ins in p_on_s:
                fds = str(ins).split()
                chrom, first_base, last_base, strand = fds[0], int(fds[1]), int(fds[2]), fds[5]
                readcount = int(fds[3].split("|")[1])
                gene, segmentid = fds[9].split("|")
                if gene not in self.supdict[sample]:
                    self.supdict[sample][gene] = defaultdict(set)
                self.supdict[sample][gene][int(segmentid)].add((first_base, readcount))




    def parse_thiscov(self, covlist):
        genelist = set()
        coveragedict = defaultdict(dict)
        numlines = len(covlist)
        linecounter = 0
        for line in covlist:
            fds = str(line).split()
            linecounter += 1
            genename, segmentID = fds[3].split("|")
            if genename not in coveragedict:
                coveragedict[genename] = defaultdict(list)
            if segmentID not in coveragedict[genename]:
                coveragedict[genename][segmentID] = []
            coveragedict[genename][segmentID].append(int(fds[-1]))
            if linecounter % 10000 == 0:
                print(linecounter, " lines were read from this cov file")
        return coveragedict


    def parse_thiscov_forgene_bigwig(self, extendedbedfile, bamfiledirs):
        num_cores = max(1,multiprocessing.cpu_count())
        print(num_cores)
        genelist = set()
        extendedbed = open(extendedbedfile, 'r').read().splitlines()[:const.GENEC]
        ## TODO:
        # Think about reps
        othertemplist = []
        for sample in bamfiledirs:
        #for sample in ["sample2"]:
            self.allgenecoverages[sample] = defaultdict(list)
            for bamfiledir in bamfiledirs[sample]:
                counter = 0
                for thresh in range(int(len(extendedbed)/1000)+1):
                    if thresh > 0 :
                        print(thresh*1000, " genes were processed" )
                    templist = []
                    this_segment = extendedbed[thresh*1000 : thresh*1000+1000]
                    templist = Parallel(n_jobs=8, verbose=const.VRB, backend="multiprocessing", mmap_mode="r+", temp_folder="/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/joblibtemp/")(
                              delayed(unwrap_parse_thiscov_forgene_bigwig_insideloop)((self, line_ind, line, bamfiledir, sample))
                              for line_ind, line in enumerate(this_segment))
                    othertemplist += templist
                    #print(getsizeof(othertemplist))
                    del templist[:]
                    gc.collect()

            print(sample, "This sample was done")
        for g in othertemplist:
            genelist.add(g[0])
            this_sample = g[2]
            self.allgenecoverages[g[2]][g[0]] = g[1]

            #del templist[:]
            #del othertemplist[:]
            #gc.collect()
        #print(self.allgenecoverages[sample][genelist[10]])
        return list(genelist)

    def parse_thiscov_forgene_bigwig_insideloop(self, line_ind, line, bamfiledir, sample):
        fds = line.split()
        #if line_ind % 1000 == 0:
            #print(line_ind, " genes were processed")
        region = fds[0] + " " + fds[1] + " " + fds[2]
        genename = fds[3]
        this_start = int(fds[1])
        this_end = int(fds[2])
        this_command = "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/bigWigSummary " +  str(bamfiledir)  + " " + region  + " " +  str(this_end - this_start + 1) + " 2> templog"
        output = str(subprocess.Popen(this_command, shell=True, stdout=subprocess.PIPE).stdout.read(), "utf-8")
        #temp = [0 for zind in range(this_end - this_start + 1)]
        #print(output[0:20], output[-20:-1])

        if "n/a" in output:
            output = output.replace("n/a", "0")
        if len(output) < 3:
            temp = [0 for zind in range(this_end - this_start + 1)]
        else:
            temp = [int(nt) for nt in  list(map(float, output.split()))]

        #self.allgenecoverages[sample][genename] = temp

        return (genename, np.array(temp), sample)




    def parse_thiscov_forgene_samtools(self, extendedbedfile, bamfiledirs):
        genelist = set()
        extendedbed = open(extendedbedfile, 'r').read().splitlines()
        for sample in bamfiledirs:
            genecoveragedict = defaultdict(list)
            for bamfiledir in bamfiledirs[sample]:
                counter = 0
                for line in extendedbed:
                    counter +=1
                    fds = line.split()
                    region = fds[0] + ":" + fds[1] + "-" + fds[2]
                    genename = fds[3]
                    this_command = "samtools depth -r \"" + region + "\" " + str(bamfiledir)
                    output = subprocess.Popen(this_command, shell=True, stdout=subprocess.PIPE).stdout.read()
                    this_start = int(fds[1])
                    this_end = int(fds[2])
                    temp = [0 for zind in range(this_end - this_start + 1)]
                    for templine in output.splitlines():
                        tempfds = templine.split()
                        this_site = int(tempfds[1])
                        temp[this_site - this_start] = int(tempfds[-1])
                    genecoveragedict[genename] = temp
                    genelist.add(genename)
                    if counter % 1000 == 0:
                        print(linecounter, " genes were read from this cov file")
                    if counter % const.GENEC == 0:
                        print("Genes processed")
                        break

            # TODO:
            # Take care of reps

            #print(genelist)
            self.allgenecoverages[sample] = genecoveragedict
            #print(self.allgenecoverages[sample].keys())
        genelist = sorted(genelist)
        return genelist

    def parse_thiscov_forgene(self, covlist):
        genelist = []
        print(covlist.keys())
        #num_cores = max(1,multiprocessing.cpu_count())
        #print(num_cores)
        cov_parsed = Parallel(n_jobs=2, backend="multiprocessing")(
                  delayed(unwrap_parse_thiscov_forgene_insideloop)((self, covlist[sample], sample))
                  for sample in covlist.keys())

        for cov_info in cov_parsed:
            genelist += list(cov_info[2])
            self.allgenecoverages[cov_info[0]] = cov_info[1]
        genelist = sorted(set(genelist))
        return genelist


    def parse_thiscov_forgene_insideloop(self, this_covlist, sample):
        genecoveragedict = defaultdict(list)
        genelist = set()
        #numlines = len(covlist)
        linecounter = 0
        prename = ""
        temp = []
        covlistfile = open(this_covlist[0], 'r')
        bufsize = 65536*4
        with open(this_covlist[0], 'r') as covlistfile:
            while True:
                lines = covlistfile.readlines(bufsize)
                if not lines:
                    break

                for line in lines:
                    fds = line.split()
                    genename = fds[3]
                    if genename != prename:
                        genelist.add(genename)
                        linecounter += 1
                        if linecounter % 1000 == 0:
                            print(linecounter, " genes were read from this cov file")
                        #if prename != "" :
                        genecoveragedict[prename] = temp
                        prename = genename
                        temp = []
                    temp.append(int(fds[-1]))

                if linecounter % const.GENEC == 0:
                    print("Genes processed")
                    break

        genelist.add(genename)
        genecoveragedict[genename] = temp
        self.allgenecoverages[sample] = genecoveragedict
        return (sample, genecoveragedict, genelist)



    def make_multiple_beds(self,segmentbedfiledir, tempfolder):
        this_command = "awk \'{print $0 >> \"" + tempfolder + "\"$1\".bed\"}\' " + segmentbedfiledir
        #this_command = r"awk '{print $0 >> '{}'$1'.bed'}' {}".format(tempfolder, segmentbedfiledir)
        print(this_command)
        output = subprocess.Popen(this_command, shell=True, stdout=subprocess.PIPE).stdout.read()
        #output = subprocess.call([this_command])
        print(output)
        allbedfiles = os.listdir(tempfolder)
        print(tempfolder, allbedfiles)
        return allbedfiles

    def get_coverage(self, bamfile, bedfiledir):
        sbinitfile = pybedtools.example_bedtool(bedfiledir)
        sbsinitfile = sbinitfile.sort()
        thiscov = bamfile.coverage(sbsinitfile, s=True, d=True, nonamecheck=True)
        print("We are done here")
        return thiscov

    def get_gene_coverage(self, extendedbedfile, bamfilesdir, premade_coverage):
        #allbedfiles = self.make_multiple_beds(segmentbedfiledir, "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/tempbeds/")
        #print(allbedfiles)
        for sample in bamfilesdir:
            if sample not in self.allgenecoverages:
                self.allgenecoverages[sample] = defaultdict(dict)
            for bamfiledir in bamfilesdir[sample]:
                if premade_coverage is None:
                    baminitfile =  pybedtools.example_bedtool(bamfiledir)
                    results = get_coverage(baminitfile, extendedbedfile)
                    #results = Parallel(n_jobs=16, verbose=1, backend="threading")(
                    #                    delayed(unwrap_get_coverage)((self, baminitfile, "/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/tempbeds/"+thisbedfiledir))
                    #                    for thisbedfiledir in allbedfiles)
                else:
                    # TODO:
                    # add replicates info here
                    results = open(premade_coverage[sample][0], 'r')
                self.allgenecoverages[sample] = self.parse_thiscov_forgene(results)


    def trim_gene(self, stop_thresh = const.TRIMTRESH):
        for gid in self.processedgenelist:
            lastindex = dict()
            init_num_seg = len(self.segmentsfeats[gid])
            for this_sample in self.allgenecoverages:
                total = sum(self.allgenecoverages[this_sample][gid])
                pre = 0
                post = total
                lastindex[this_sample] =  init_num_seg - 1
                for segment_id in range(init_num_seg):

                    this_coverage =  sum(self.allsegmentcoverages[this_sample][gid][segment_id])
                    pre += this_coverage
                    #if post - pre <  stop_thresh:
                    if post < stop_thresh:
                        lastindex[this_sample] = segment_id
                        break
                    post -= this_coverage

            #print(gid, init_num_seg, lastindex)
            final_lastindex = max(lastindex.values())
            for segment_id in range(init_num_seg):
                if segment_id > final_lastindex:
                    print(gid, "we are trimming")
                    del self.segmentsfeats[gid][segment_id]

def unwrap_get_coverage(arg, **kwarg):
    return GENES.get_coverage(*arg, **kwarg)

def unwrap_parse_thiscov_forgene_insideloop(arg, **kwarg):
    return GENES.parse_thiscov_forgene_insideloop(*arg, **kwarg)

def unwrap_parse_thiscov_forgene_bigwig_insideloop(arg, **kwarg):
    return GENES.parse_thiscov_forgene_bigwig_insideloop(*arg, **kwarg)
