import argparse
from collections import defaultdict
import countAAAreads
import genes
import generate_profile
import cnn_model
import logging
import const

class PASARNA():
    def __init__(self):
        self.bamdirs = defaultdict(list)
        self.genesdir = defaultdict(list)
        self.jinfofiles = defaultdict(list)
        self.samplereadsfiles = defaultdict(list)
        self.usedpasfiles = defaultdict(list)
        self.otherpasfiles = defaultdict(list)
        self.othergenefile = ""
        self.otherapafile = ""
        self.usedpasflag = False
        self.otherapaflag = False
        self.othergeneflag = False
        self.otherpasflag = False
        self.sjflag = False
        self.analyzeAAAflag = False
        self.jbedfiledir = defaultdict(list)
        self.passiteavailable = False
        self.pasfile = ""
        self.genomefa = ""
        self.AAAdict = defaultdict(list)
        self.geneprofilesdict = defaultdict(list)
        self.segmentfiledir = ""
        self.samplecovfiles = defaultdict(list)
        self.pasfile = None
        self.readCountForKnownSites = False
        self.utrfile = None



    def parse_arguments(self):
        parser = argparse.ArgumentParser(description='This program uses RNA-Seq data to find APA genes.'
                                        'If available it can take advantage of availabe list of polyA sites,'
                                        'AAA tail reads, read counts on splice junctions called by STAR,'
                                        'and genome sequence. Gene annotation is required.')
        parser.add_argument("-v", "--verbosity", action="count", default=0)

        # Choose Module
        parser.add_argument("--analyzeAAA", action="store_true", help="This module helps to analyze "
                            "AAA reads, you need the reads, genome, and STAR")
        parser.add_argument("--makeGeneProfile", action="store_true", help="Makes a profile of genes."
                            "Segment them and add info from annotation, junctions, available polyA sites,"
                            "and AAA reads")
        parser.add_argument("--markUsedSites", action="store_true", help="Does various analysis to find"
                            " new polya sites or mark the used ones from the list")
        parser.add_argument("--markAPAprofiles", action="store_true", help="Looks at generated profiles to"
                            " mark genes with APA")
        parser.add_argument("--markUpstreamUsedSites", action="store_true", help="Focuses on changed"
                            " upstream polyA sites.")

        parser.add_argument("--readCountForKnownSites", action="store_true", help="Uses a list of PolyA sites")


        # Input files
        parser.add_argument('--sampleendbed', action='append', help="A bam file with the mapped reads"
                            " of samples, use multiple sample1endbed for reps", required=True)

        parser.add_argument('--geneannot', action='store', help="directory to gene annotation in bed12",
                            required=True)
        parser.add_argument('--segmentfiledir', action='store', help="directory to write segmentfile",
                            required=True)

        parser.add_argument('--genomefa', action='store', help="directory to genome sequence",
                            default = None)
        parser.add_argument('--polyalistfile', action='store', help="directory to polya list",
                            default = None)
        parser.add_argument('--starsjtab', action='append', help="directory to sj tab file from STAR",
                            default = [])

        parser.add_argument('--samplereads', action='append', help="directory to sample1 reads fastq",
                            default = [])

        parser.add_argument('--samplecovs', action='append', help="directory to sample1 coverage file",
                            default = [])

        # To test arguments
        parser.add_argument('--usedpassample', action='append', help="directory to read counts from pas",
                            default = [])


        parser.add_argument('--comparePASswithPASSeq', action='store_true', help=
                                                        "compare with a list of pas")
        parser.add_argument('--comparePASswithOther', action='store_true', help=
                                                        "compare with a list of pas")
        parser.add_argument('--compareAPAGeneswithOther', action='store_true', help=
                                                        "compare with a list of genes")
        parser.add_argument('--compareAPASiteswithOther', action='store_true', help=
                                                        "compare with a list of polyA pairs")

        parser.add_argument('--otherpaslist', action='append', help=
                                                        "a list of sites", default = [])

        parser.add_argument('--othergenelist', action='store', help=
                                                        "a list of genes", default = None)
        parser.add_argument('--otherpaspairlist', action='store', help=
                                                        "a list of pair of sites", default = None)

        parser.add_argument('--utrfile', action='store', help=
                                                        "a list of annotated termination sites", default = None)
        args = parser.parse_args()

        ###############################################################################################
        ###############################################################################################
        ###############################################################################################
        #required args
        num_samples = len(args.sampleendbed)
        for _sampleid in range(num_samples):

            _samplename = "sample" + str(int(_sampleid+1))
            const.SAMPLES.append(_samplename)
            self.bamdirs[_samplename] = [args.sampleendbed[_sampleid]]

        self.genesdir = args.geneannot
        self.segmentfiledir = args.segmentfiledir
        #ginfo = genes.parse_geneinfo(args.geneannot)

        ###############################################################################################
        ###############################################################################################
        ###############################################################################################
        # optional args

        if len(args.starsjtab) > 0:
            self.sjflag = True
            num_samples = len(args.starsjtab)
            for _sampleid in range(num_samples):
                _samplename = "sample" + str(int(_sampleid+1))
                self.jinfofiles[_samplename] = [args.starsjtab[_sampleid]]
                self.jbedfiledir[_samplename] = [args.starsjtab[_sampleid] + ".bed"]
        else:
            self.jinfofiles = None

        if len(args.samplecovs) > 0:
            num_samples = len(args.samplecovs)
            for _sampleid in range(num_samples):
                _samplename = "sample" + str(int(_sampleid+1))
                self.samplecovfiles[_samplename] = [args.samplecovs[_sampleid]]
        else:
            self.samplecovfile =  None

        if args.polyalistfile is not None:
            self.passiteavailable = True
            self.pasfile = args.polyalistfile

        if args.genomefa is not None:
            self.genomefa = args.genomefa

        if args.analyzeAAA :
            self.analyzeAAAflag = True
            num_samples = len(args.samplereads)
            for _sampleid in range(num_samples):
                _samplename = "sample" + str(int(_sampleid+1))
                self.samplereadsfiles[_samplename] = [args.samplereads[_sampleid]]
        else:
            self.samplereadsfiles = None

        ###############################################################################################
        ###############################################################################################
        ###############################################################################################
        # optional test args

        if len(args.usedpassample) > 0 :
            self.usedpasflag = True
            num_samples = len(args.usedpassample)
            for _sampleid in range(num_samples):
                _samplename = "sample" + str(int(_sampleid+1))
                self.usedpasfiles[_samplename] = [args.usedpassample[_sampleid]]
        else:
            self.usedpasfiles = None

        self.otherpasfiles = defaultdict(list)
        if args.comparePASswithOther :
            self.otherpasflag = True
            if len(args.otherpaslist) > 0:
                num_samples = len(args.otherpaslist)
                for _sampleid in range(num_samples):
                    _samplename = "sample" + str(int(_sampleid+1))
                    self.otherpasfiles[_samplename] = [args.otherpaslist[_sampleid]]
            else:
                try:
                    raise ValueError('Other pas file for sample1 is missing, use --otherpaslist directory')
                except ValueError as err:
                    print(err.args)


        if args.compareAPAGeneswithOther:
            self.othergeneflag = True
            if args.otheregenelist is not None:
                self.othergenefile = args.othergenelist
            else:
                try:
                    raise ValueError('Other gene file is missing, use --otheregenelist directory')
                except ValueError as err:
                    print(err.args)

        if args.compareAPASiteswithOther:
            self.otherapaflag = True
            if args.otherpaspairlist is not None:
                self.otherapafile = args.otherpaspairlist
            else:
                try:
                    raise ValueError('Other apa file is missing, use --otherpaspairlist directory')
                except ValueError as err:
                    print(err.args)
        else:
            self.otherapafile = None


        self.makeGeneProfileflag = args.makeGeneProfile
        self.markUsedSitesflag = args.markUsedSites
        self.markAPAprofilesflag = args.markAPAprofiles
        self.markUpstreamUsedSitesflag = args.markUpstreamUsedSites
        self.readCountForKnownSites =  args.readCountForKnownSites
        self.utrfile = args.utrfile

    # TODO :
    # Fix all the below functions
    def choose_pipeline(self):
        #if self.analyzeAAAflag :
        #    self.AAAdict = countAAAreads.aaa_process()
        if self.passiteavailable:
            print("We are making a cnn model from sequences.")
            thismodel = cnn_model.CNN_MODEL()
            #thismodel.make_a_model(self.pasfile, self.samplecovfiles)
            thismodel = cnn_model.load_a_model("/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/Elf_test2.hdf5")
        if self.makeGeneProfileflag :
            print("We are making the profiles")
            thesegenes = genes.GENES()
            #self.geneprofilesdict =
            thesegenes.profile_process(self.genesdir, self.segmentfiledir, self.bamdirs,
                                       self.samplecovfiles,
                                       paslistdir = self.pasfile,
                                       test_knownpasdir = self.otherapafile,
                                       jfiledir = self.jinfofiles,
                                       jbedfiledir = self.jbedfiledir,
                                       aaareaddirs = self.samplereadsfiles,
                                       usedpasfiledir = self.usedpasfiles,
                                       utrfiledir = self.utrfile)


        if self.markUsedSitesflag :
            self.usedsitesdict = generate_profile.call_changepoints(thesegenes.processedgenelist,
                                                                    thesegenes.allgenecoverages,
                                                                    thesegenes.allsegmentcoverages,
                                                                    thesegenes.segmentsfeats,
                                                                    AAAreadsdict = thesegenes.allaaareads,
                                                                    sites_known = thesegenes.spdict,
                                                                    apa_known = thesegenes.testspdict,
                                                                    GMM = False,
                                                                    MPeaks = False,
                                                                    tsalg ="Bayes",
                                                                    sjsets = thesegenes.sjdict,
                                                                    usedpas = thesegenes.supdict,
                                                                    seqmodel = thismodel,
                                                                    utrsegments = thesegenes.utrsegments)

        if self.readCountForKnownSites :
            generate_profile.call_known_changepoints(thesegenes.processedgenelist,
                                                     thesegenes.allgenecoverages,
                                                     thesegenes.allsegmentcoverages,
                                                     thesegenes.segmentsfeats,
                                                     sites_known = thesegenes.spdict)



        if self.markAPAprofilesflag:
            self.apadict = generate_profile.pipeline()

        if self.markUpstreamUsedSitesflag:
            self.upstreamused = generate_profile.pipeline()


def main():
    pasarna = PASARNA()
    pasarna.parse_arguments()
    print("arguments were parsed")
    pasarna.choose_pipeline()

if __name__== "__main__":
    main()
