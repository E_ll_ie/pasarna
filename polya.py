import sys
from collections import defaultdict

clusters = defaultdict(list, (("only-site-used-LEX" , []),
            ("only-site-used-INT", []),
            ("only-site-used-UPEX", []),
            ("distal-LEX-withplex" , []),
            ("proximal-LEX-withdlex", []),
            ("distal-LEX-withpint" ,[]),
            ("proximal-INT-withdlex", []),
            ("proximal-UPEX-withdlex" , []),
            ("distal-LEX-withpupex", [])))

def parse_polyAsiteinfo(polyAsite):

    #5S_rRNA 5S_rRNA ENSG00000271924.1       LastExon        chr2+162268228  13
    fields = polyAsite.split()
    genename = fields[1]
    tp = fields[3]
    coords = fields[4]
    readcount = fields[5]
    if "+" in coords:
        strand = "+"
        chrm, pos = coords.split("+")
    elif "-" in coords:
        strand = "-"
        chrm, pos = coords.split("-")

    return genename, chrm, strand, pos, tp, int(readcount)




def parse_polyA_file(polyAtable):
    gene_polyA_dict = defaultdict(list)
    for this_site in polyAtable[1:]:
        genename, chrm, strand, pos, tp, readcount = parse_polyAsiteinfo(this_site)
        if readcount > 5:
            gene_polyA_dict[genename].append((chrm, strand, pos, tp, readcount))


    for gene in gene_polyA_dict:
        this_gene_info = sorted(gene_polyA_dict[gene],  key=lambda x: x[4])
        sum_all_reads = sum([pas[4] for pas in this_gene_info])
        this_gene_info_perc = [pas[4]*100/sum_all_reads for pas in this_gene_info]

        if this_gene_info_perc[0] >= 90 :
            if this_gene_info[0][3] == "LastExon":
                clusters["only-site-used-LEX"].append(("".join(this_gene_info[0][:3])))


            elif this_gene_info[0][3] == "UpstreamExon":
                clusters["only-site-used-UPEX"].append(("".join(this_gene_info[0][:3])))

            elif this_gene_info[0][3] == "Intron":
                clusters["only-site-used-INT"].append(("".join(this_gene_info[0][:3])))

        if this_gene_info_perc[0] > 40 and  this_gene_info_perc[0] < 90 :
            top_2_info = sorted(this_gene_info[:2],  key=lambda x: int(x[2]))
            if top_2_info[0][1] == "+":
                distal = top_2_info[1]
                proximal = top_2_info[0]
            else:
                distal = top_2_info[0]
                proximal = top_2_info[1]

            if distal[3] == "LastExon":
                if proximal[3] == "LastExon":
                    clusters["distal-LEX-withplex"].append(("".join(distal[:3])))
                    clusters["proximal-LEX-withdlex"].append(("".join(proximal[:3])))

                elif proximal[3] == "UpstreamExon":
                    clusters["distal-LEX-withpupex"].append(("".join(distal[:3])))
                    clusters["proximal-UPEX-withdlex"].append(("".join(proximal[:3])))

                elif proximal[3] == "Intron":
                    clusters["distal-LEX-withpint"].append(("".join(distal[:3])))
                    clusters["proximal-INT-withdlex"].append(("".join(proximal[:3])))




def parse_pasfile(pfile):
    paslist = defaultdict(set)
    nameid = {}
    for line in pfile[1:]:
        fds = line.strip().split()
        gid = fds[2]
        coord = fds[4]
        paslist[gid].add(coord)
        nameid[fds[0]] = fds[2]
    return paslist, nameid


def parse_pasfile_withcov(pfile):
        paslist = set()
        greads  = {}
        for line in pfile[1:]:
                fds = line.strip().split()
                gid = fds[1]
                coord = fds[4]
                #if coord in clusters["only-site-used-LEX"]:
                if gid not in greads:
                    greads[gid] = 0
                if int(fds[-1]) > 1:
                    greads[gid] += int(fds[-1])

                #if int(fds[-1]) > 5 :
                #    paslist.add(coord)
        for line in pfile[1:]:
            fds = line.strip().split()
            gid = fds[1]
            coord = fds[4]
            #if fds[3] == "UpstreamExon" or fds[3] == "Intron":
            if fds[3] =="LastExon":
                if gid in greads:
                    if int(fds[-1]) > 1 and greads[gid] > 2:
                        if float(fds[-1])/greads[gid] > 0.01:
                            paslist.add(coord)
        #print(len(paslist))
        return paslist