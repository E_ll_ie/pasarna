import const
import changepoints as changepointsmethods
import countreads
import polya
from collections import defaultdict
class SEGMENT():
    def __init__(self, segmentid, feats, profile, sjsets_g, aaadict_g, sites_known_g):
        self.segment_maybeset = []
        self.segment_yesset = []
        self.maybe_table_rc = defaultdict(dict)
        self.yes_table_rc = defaultdict(dict)
        self.sprofile = []
        self.profile = []
        self.chrom = feats[0]
        self.ex_end = feats[2]
        self.start = feats[1]
        self.end = feats[3]
        self.strand = feats[-1]
        if self.strand == "+":
            self.real_start = self.start
            self.profile = profile
        else:
            self.real_start = self.end
            self.profile = profile[::-1]
        self.sjset = sjsets_g


        self.aaa = aaadict_g
        self.knownsites = sites_known_g
        self.mmpeaks = []
        self.gmmpeaks = []
