#This is a code for using RNASeq in PAS Analysis
#Some parts are for validation and not part of this pipeline
from __future__ import print_function
import sys
from collections import defaultdict, namedtuple
import gzip
import numpy
import math
import re
from joblib import Parallel, delayed
import multiprocessing
import numpy as np
import gc
import subprocess
import random
import logging
random.seed(7)
#import find_changepoint
from pybedtools import BedTool
import pybedtools
import time
#import pandas
#from tensorly.decomposition import robust_pca
sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/ruptures/')
import ruptures as rpt
#sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/changepy/')
#from changepy import pelt
#from changepy.costs import *
#from changepy.costs import poisson
from scipy import stats
#from scipy.interpolate import UnivariateSpline
from scipy.signal import wiener, filtfilt, butter, gaussian, freqz
from scipy.ndimage import filters
#import scipy.optimize as op
#from sklearn.mixture import GaussianMixture
#from sklearn.mixture import BayesianGaussianMixture
from scipy.stats import pearsonr, spearmanr
#import pymc3 as pm
#sys.path.append('/cbcl/forouzme/Projects/PolyA/FinalPolyACode/astimeseries/matlabengine/lib/python3.6/site-packages/')
#import matlab.engine
#eng = matlab.engine.start_matlab()


import bayesloop as bl
import genes
import polya
import const
import sequence
import changepoints as changepointsmethods
import evaluate
import countreads
import segment
import generate_profile
import cnn_model

logging.basicConfig(level=logging.WARNING)
psl = logging.getLogger("segment-process-1")

##This models the general analysis
def process_segment_1(this_sample, gene,
                    segmentid,
                    segment_feats, #segmentfeats[gene][segmentid]
                    profile, #coverages[sample][gene][segmentid]
                    sjsets_sg, #sjsets[sample][gene] check if gene is in sample
                    AAAreadsdict_sg, #AAAreadsdict[sample][gene]
                    sites_known_g, #sites_known[gene]
                    totalreads,
                    num_segments,
                    tsalg,
                    GMM,
                    MPeaks = False,
                    simple = False):

    #start = time.clock()
    psl.debug("Before Segment Generation: Sample: {}, Gene: {}, Seg_ID: {}, LEN_Prof: {} ".format(this_sample, gene, segmentid, len(profile)))
    seg = segment.SEGMENT(segmentid, segment_feats, profile, sjsets_sg, AAAreadsdict_sg, sites_known_g)
    psl.debug("After Segment Generation: Sample: {}, Gene: {}, Seg_ID: {}, LEN_Prof: {} ".format(this_sample, gene, segmentid, len(profile)))

    segment_yesset = []
    segment_maybeset = []
    #profile = [x/totalreads[sample] for x in coverages[sample][gene][segmentid]]

    #b, a = butter(2, 0.1)
    #b, a = butter(1, 60/max(61,len(seg.profile)))
    #bundled_up = filtfilt(b, a, [i/totalreads for i in seg.profile])
    #bundled_up = filtfilt(b, a, seg.profile)
    #flsignal = numpy.expand_dims( numpy.array(filtfilt(b, a, tempsmooth)), axis=1)
    #bundled_up = filtfilt(b, a, tempsmooth)
    #flsignal = bundled_up
    flsignal = seg.profile.astype(int)
    if not simple:
        '''
        bundled =  [ip  if ip>0 else float("nan") for ip in seg.profile]
        bundled_up = eng.fillgaps(matlab.double(bundled), 10, 2, nargout = 1)
        print(segmentid, "gapsfilled")
        if type(bundled_up) != float:
            bundled_up = [max(0, float(this_cp)) for this_cp  in bundled_up[0]]
        else:
            bundled_up = [max(0,bundled_up)]

        #brange = [ip[0] for ip in bundled]
        #bprofile = [ip[1] for ip in bundled]

        if numpy.isnan(bundled_up).any():
            seg.sprofile = seg.profile
            return (segmentid, seg)
        '''


        seg.sprofile = seg.profile.astype(int)
        #print("before model")
        #flsignal = numpy.array(filtfilt(b, a, tempsmooth))
        if tsalg == "mcpt":
            tempfromatcpt = eng.findchangepts(matlab.double(list(flsignal)), 'Statistic','linear','MaxNumChanges', 5, nargout = 1)
            if type(tempfromatcpt) != float:
                tempchangepoints = [int(this_cp) for this_cp  in tempfromatcpt[0] if int(this_cp) < len(flsignal)]
            else:
                tempchangepoints = [int(tempfromatcpt)]
        elif tsalg == "BottomUp":
            psl.debug("Before BottomUp: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
            algo = rpt.BottomUp(model="l2", jump=5).fit(np.array([kk*100/totalreads for kk in flsignal]))
            tempchangepoints =  algo.predict(pen=0.0001,n_bkps=5)[:]
            tempchangepoints = [int(this_cp) for this_cp  in tempchangepoints if int(this_cp) < len(flsignal)]
            psl.debug("After BottomUp: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))

        elif tsalg == "Pelt":
            psl.debug("Before Pelt: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
            '''
            if len(flsignal) > 20000:
                tempfl = flsignal[::4]
                factor = 4
            elif len(flsignal) > 10000:
                tempfl = flsignal[::2]
                factor = 2
            else:
                tempfl = flsignal
                factor = 1
            '''
            algo = rpt.Pelt(model="l1", min_size=80 ).fit(np.array([kk*100/totalreads for kk in flsignal]))
            tempchangepoints =  algo.predict(pen=3)
            tempchangepoints = [int(this_cp) for this_cp  in tempchangepoints if int(this_cp) < len(flsignal)]
            psl.debug("After Pelt: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))

        elif tsalg == "Window":
            psl.debug("Before Window: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
            algo = rpt.Window(model="l2", width=160, min_size = 80).fit(np.array([kk*100/totalreads for kk in flsignal]))
            tempchangepoints =  algo.predict(n_bkps=5, pen=0.01, epsilon=0.0001)
            tempchangepoints = [int(this_cp) for this_cp  in tempchangepoints if int(this_cp) < len(flsignal)]
            psl.debug("After Window: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))

        elif tsalg == "Binary":
            psl.debug("Before Binary: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
            #c = rpt.costs.CostRbf()
            if max(flsignal) == 0:
                tempchangepoints = []
            algo = rpt.Binseg(model="l2", jump=1).fit(np.array([kk*100/totalreads for kk in flsignal]))
            tempchangepoints =  algo.predict(pen=0.0001, n_bkps=5)[:] #n_bkps=2
            tempchangepoints = [int(this_cp) for this_cp  in tempchangepoints if int(this_cp) < len(flsignal)]
            psl.debug("After Binary: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))

        elif tsalg == "Bayes":
            psl.debug("Before Bayes: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
            #tempchangepoints = changepointsmethods.use_bayes(flsignal)
            tempchangepoints = changepointsmethods.findChangepointsByBl(flsignal)
            psl.debug("After Bayes: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))




        drops = []
        # return relative
        minns, drops, maxslops = \
        changepointsmethods.find_only_lastone(seg.start,
                                              seg.end,
                                              seg.strand,
                                              flsignal)




        #add known sitesinfo
        #if len(seg.knownsites) > 0:
        #    tempchangepoints += [countreads.change_coord_to_rel(this_site, seg.start, seg.end, seg.strand) for this_site in seg.knownsites]
        #tpc = changepointsmethods.make_sure_about_last_one(seg.start, seg.end, seg.strand, seg.profile)
        #if tpc!=None:

        #if segmentid == num_segments -1 :
        #    if len(tempchangepoints) < 2:
        #        #print(tempchangepoints, seg.start, seg.ex_end, seg.end, seg.strand, seg.real_start, abs(seg.ex_end - seg.real_start))
        #        tempchangepoints += [abs(seg.ex_end - seg.real_start)]

        #    tempchangepoints.append(tpc)
        tempchangepoints = sorted(tempchangepoints+[i[0] for i in drops])

        psl.debug("Before Merge: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
        #tempchangepoints = changepointsmethods.basic_filters(tempchangepoints,seg.profile, seg.profile, abs(seg.ex_end -seg.real_start))

        if const.KNOWNSITEADD:
        #seg.segment_maybeset = []
            if len(seg.knownsites) > 0 and (segmentid == num_segments - 1) :
                seg.segment_maybeset += [countreads.change_coord_to_rel(site, seg.start, seg.end, seg.strand) for site in list(seg.knownsites)]


        #if len(tempchangepoints) == 0:
        #    return (segmentid, seg, this_sample, gene, "", "")
        if const.MERGCONSEC:
            tempchangepoints = \
            changepointsmethods.merge_consec_changepoints(tempchangepoints,
                                              flsignal,
                                              maxMergeDist = const.MERGEMINDISTPRE,
                                              regSizeforRatio = 100)


        #print(tempchangepoints)
        psl.debug("After Merge: Sample: {}, Gene: {}, Seg_ID: {}, candidates: {}".format(this_sample, gene, segmentid,
        [generate_profile.realcoord(seg.real_start, tcp, seg.strand) for tcp in tempchangepoints]))



        if const.BASICFILT:
            tempchangepoints = changepointsmethods.basic_filters(tempchangepoints,seg.profile, seg.profile, seg.ex_end)


        psl.debug("After Basic Filter: Sample: {}, Gene: {}, Seg_ID: {}, candidates: {}".format(this_sample, gene, segmentid,
        [generate_profile.realcoord(seg.real_start, tcp, seg.strand) for tcp in tempchangepoints]))


        # FIXME:
        if const.KNOWNSJADJUST:
            if segmentid != num_segments - 1:
                if len(seg.sjset) > 0:

                    tempchangepoints = \
                    generate_profile.compare_withsjs(tempchangepoints,
                                    seg.sjset,
                                    seg.profile,
                                    seg.real_start,
                                    seg.strand,
                                    minDist = const.SJMAXDIST,
                                    minUp = 100)

                tt= []
                for t in tempchangepoints:
                    if abs(t - abs(seg.end - seg.real_start)) > const.SJMAXDIST:
                        tt.append(t)
                tempchangepoints = tt


        seg.segment_maybeset = [generate_profile.realcoord(seg.real_start, tcp, seg.strand)
                                  for tcp in tempchangepoints] #[sample][gene][segmentid]]


        #return abs
        # if (segmentid != num_segments - 1):
        #     if max(flsignal) > 0:
        #         final_drop = changepointsmethods.make_sure_about_last_one(seg.start,
        #                                                     seg.end,
        #                                                     seg.strand,
        #                                                     flsignal)
        #
        #         tempchangepoints.append(final_drop)


        psl.debug("After SJ: Sample: {}, Gene: {}, Seg_ID: {}, candidates: {}".format(this_sample, gene, segmentid, seg.segment_maybeset))


        ### Should we compare info here?
        ### Or should I just keep this info for later?
        if const.USEAAA:
            if len(seg.aaa) > 0:
                seg.segment_yesset, seg.segment_maybeset = \
                             generate_profile.compare_two_sets(seg.segment_maybeset,
                                                seg.aaa, addrest = False)


        if const.KNOWNSITEADJUST:
            if len(seg.knownsites) > 0:
                tempyes, seg.segment_maybeset = \
                             generate_profile.compare_two_sets(seg.segment_maybeset,
                                             seg.knownsites, minDist= const.MERGEKNOWNDIST)

                seg.segment_yesset += tempyes


        # In case this is the last exon and because we know there is enough coverage
        # # TODO:
        # This is probably not the best way
        '''
        if (segmentid == num_segments - 1):
            if max(flsignal) > 0:
                final_drop = changepointsmethods.make_sure_about_last_one(seg.start,
                                                            seg.end,
                                                            seg.strand,
                                                            flsignal)
                seg.segment_maybeset.append(final_drop)
            seg.segment_maybeset.append(seg.ex_end)

            #print("drops", final_drop, seg.ex_end)
        '''


        if (len(seg.segment_yesset) + len(seg.segment_maybeset) == 0) and (segmentid == num_segments - 1) :
            if max(flsignal) > 0 :
                #algo = rpt.Binseg(model="l2", jump=2).fit(np.array([kk*100/totalreads for kk in flsignal]))
                #tempchangepoints =  algo.predict(pen=0.0001, n_bkps=1)
                #seg.segment_maybeset =  [generate_profile.realcoord(seg.real_start, tcp, seg.strand)
                #                          for tcp in tempchangepoints]

                final_drop = changepointsmethods.make_sure_about_last_one(seg.start,
                                                                seg.end,
                                                                seg.strand,
                                                                flsignal)
                seg.segment_maybeset.append(final_drop)
                seg.segment_maybeset.append(seg.ex_end)


        psl.debug("After SJ: Sample: {}, Gene: {}, Seg_ID: {}, candidates: {}".format(this_sample, gene, segmentid, seg.segment_maybeset))

        ############FOR TESTTTTT
        if const.TESTR:
            #seg.segment_maybeset = [generate_profile.realcoord(seg.real_start, tcp, seg.strand)
            #                          for tcp in tempchangepoints]

            return (segmentid, this_sample, gene, seg.segment_maybeset, seg.segment_yesset)
        #sequence check
        maybeseqs = ""
        yesseqs = ""

        maybeseqdict = {}
        yesseqdict = {}

        if const.SEQCHECK:
            for this_pos in seg.segment_maybeset:
                this_pos_name  = "|".join([this_sample, gene, str(segmentid), str(this_pos)])
                temp = "\t".join([seg.chrom, str(max(0,int(this_pos)-250)), str(int(this_pos)+250), \
                                 this_pos_name ,".", seg.strand])+"\n"
                maybeseqs += temp

            #maybeseqdict = sequence.extract_seq_frombed(maybeseqs, const.genomefa)


            for this_pos in seg.segment_yesset:
                this_pos_name  = "|".join([this_sample, gene, str(segmentid), str(this_pos)])
                temp = "\t".join([seg.chrom, str(max(0,int(this_pos)-250)), str(int(this_pos)+250), \
                                 this_pos_name ,".", seg.strand])+"\n"
                yesseqs += temp


            #gc.collect()

        if GMM:
            seg.gmmpeaks = []
            gpeak = namedtuple('gpeak', 'pos score std border')
            segmentforcomparison = []
            for nt in range(end - start):
                segmentforcomparison += [[nt]]*seg.profile[nt]
            gmm = BayesianGaussianMixture(n_components=5, covariance_type ="full")
            gmm.fit(segmentforcomparison)
            gmvar = [round(math.sqrt(mv),2) for mv in gmm.covariances_]
            gmmeans = [int(m[0]) for m in gmm.means_]
            gmw = [int(mw*100) for mw in gmm.weights_]
            for pk_ind in range(len(gmmeans)):
                this_pos = generate_profile.realcoord(seg.real_start, gmmeans[pk_ind], seg.strand)
                this_border = generate_profile.realcoord(seg.real_start, gmmeans[pk_ind] + int(gmvar[pk_ind]) , seg.strand)
                this_peak =  gpeak(pos = this_pos,
                                   score = gmw[pk_ind],
                                   std = gmvar[pk_ind],
                                   border =  this_border)
                seg.gmmpeaks.append(this_peak)
            psl.debug("GMM was done: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))



        # Run peak finding module in matlab
        if MPeaks:
            seg.mmpeaks = []
            mpeak = namedtuple('mpeak', 'pos score std border')
            shift = 1
            reversesignal = [1-(i/totalreads) - i for i in flsignal]
            pks, locs, widths, proms = eng.findpeaks(matlab.double(list(reversesignal)),
             'MinPeakProminence',20, 'MinPeakDistance', 80 ,nargout = 4)
            #pks, locs, widths, proms = eng.findpeaks(matlab.double(profile), nargout = 4)
            if type(pks) == float:
                pks = [pks]
                locs = [locs]
                widths = [widths]
                proms = [proms]
            else:
                pks = pks[0]
                locs = locs[0]
                widths = widths[0]
                proms = proms[0]

            if pks != "":
                for pk_ind in range(len(pks)):
                    this_pos = generate_profile.realcoord(seg.real_start, locs[pk_ind]*shift,seg.strand)
                    this_border = generate_profile.realcoord(seg.real_start, locs[pk_ind]*shift + int(widths[pk_ind]*0.5), seg.strand)
                    this_peak = mpeak(pos = this_pos ,
                                      score = proms[pk_ind],
                                      std = int(widths[pk_ind]*0.5),
                                      border = this_border)
                    seg.mmpeaks.append(this_peak)
            psl.debug("Mpeaks was done: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))


        psl.debug("Initial process was done: Sample: {}, Gene: {}, Seg_ID: {}".format(this_sample, gene, segmentid))
        if seg.strand == "-":
            seg.sprofile = seg.sprofile[::-1]

        #if (len(seg.segment_yesset) + len(seg.segment_maybeset) == 0) and (segmentid == num_segments - 1):
        #    print("NOTHING")

        seg.segment_maybeset = list(set(seg.segment_maybeset))
        seg.segment_yesset = list(set(seg.segment_yesset))
        return (segmentid, seg, this_sample, gene, maybeseqs, yesseqs)
    else:
        if (segmentid == num_segments - 1):
            final_drop = [changepointsmethods.make_sure_about_last_one(seg.start,
                                                        seg.end,
                                                        seg.strand,
                                                        flsignal)]
            #algo = rpt.Binseg(model="l2", jump=2).fit(np.array([kk*100/totalreads for kk in flsignal]))
            #final_drop =  algo.predict(pen=0.01, n_bkps=1)
            if len(final_drop) > 0:
                seg.segment_maybeset.append(seg.ex_end)
                seg.segment_maybeset.append(final_drop[0])
            else:
                print("still NOTHING")
            #print("drops", final_drop, seg.ex_end)
        return (segmentid, this_sample, gene, seg.segment_maybeset)



def process_segment_2(gene,
                      segmentid,
                      numsegments,
                      feats, #segmentfeats[gene][segmentid]
                      sjsets,
                      all_segment_maybesets,
                      all_segment_yessets,
                      sprofile_ss):
    start = feats[1]
    end = feats[3]
    strand = feats[-1]
    sprofile  = defaultdict(list)
    sidi = {}
    if segmentid == numsegments - 1:
        md = -1
    else:
        md = -1 #const.SJMAXDIST
    if strand == "+":
        real_start = start
        for _sid, _sample in enumerate(const.SAMPLES):
            sprofile[_sample] = sprofile_ss[_sid]
    else:
        real_start = end
        for _sid, _sample in enumerate(const.SAMPLES):
            sprofile[_sample] = sprofile_ss[_sid][::-1]


    for _sid, _sample in enumerate(const.SAMPLES):
        sidi[_sample] = _sid
    maybe_table_rc = defaultdict(dict)
    yes_table_rc = defaultdict(dict)
    # cross in case only one site was found
    # remove it if it's too close to sj site in other sample
    #print(gene, "start" , list(set(all_segment_maybesets[0])), all_segment_yessets)
    '''
    for _sample in const.SAMPLES:
        for _othersample in const.SAMPLES:
            if _sample!= _othersample:
                if segmentid in sjsets[sidi[_othersample]]:
                    all_segment_maybesets[sidi[_sample]] = \
                    generate_profile.cross_compare_withsjs(all_segment_maybesets[sidi[_sample]],
                                                            sjsets[sidi[_othersample]],
                                                            real_start, strand, minDist= md)


    '''
    tempyes = []
    tempmaybe = []

    ## FIXME:
    ##yes_merged = changepointsmethods.merge_consec_changepoints_simple(tempyes, strand)
    #yes_merged = changepointsmethods.merge_consec_changepoints_simpleboth(list(set(all_segment_yesset_s1)), list(set(all_segment_yesset_s2)), strand)
    #all_segment_yesset_s1 = yes_merged

    #print("part2", len(all_segment_maybeset_s1), len(all_segment_maybeset_s2), len(all_segment_yesset_s1), len(all_segment_yesset_s2))
    if segmentid == numsegments - 1:
        pthresh = const.inBoth_OneSampLastEx_PERC
    else:
        pthresh = const.inBoth_OneSampUpEx_PERC
    ##maybe_merged = changepointsmethods.merge_consec_changepoints_simple(tempmaybe, strand)
    if const.inBoth:
        tempmaybe = changepointsmethods.merge_consec_changepoints_simpleboth(list(set(all_segment_maybesets[sidi["sample1"]] + all_segment_yessets[sidi["sample1"]] )),  list(set(all_segment_maybesets[sidi["sample2"]] + all_segment_yessets[sidi["sample2"]])), strand, minDist = const.MERGEMINDIST)
        this_maybe_merged = changepointsmethods.merge_consec_changepoints_simple(list(set(all_segment_maybesets[sidi["sample1"]] + all_segment_yessets[sidi["sample1"]] + all_segment_maybesets[sidi["sample2"]] + all_segment_yessets[sidi["sample2"]])), strand, minDist = const.MERGEMINDIST)
        for _sample in const.SAMPLES:
            tempyes += list(all_segment_yessets[sidi[_sample]])

        ###############################
        rel_maybe = [countreads.change_coord_to_rel(site, start, end, strand) for site in this_maybe_merged]
        #rel_yes = [countreads.change_coord_to_rel(site, start, end, strand) for site in tempyes]
        maybe_table_rc_perc = countreads.count_read_basedonsites_lastexon_m1_scaledbyl_percout(sorted(rel_maybe), sprofile, ct=const.CT_ONESEG)
        additions = []
        for prc_site in maybe_table_rc_perc:
            if max(maybe_table_rc_perc[prc_site]) >= pthresh :
                additions.append(generate_profile.realcoord(real_start,prc_site, strand))
        #tempmaybe = changepointsmethods.merge_consec_changepoints_simple(tempmaybe+tempyes+additions, strand, minDist = const.MERGEMINDIST) ## this is the one on all runs
        tempmaybe = list(set(tempmaybe+tempyes+additions))
        for _sample in const.SAMPLES:
            tempyes += list(all_segment_yessets[sidi[_sample]])
    else:
        for _sample in const.SAMPLES:
            tempyes += list(all_segment_yessets[sidi[_sample]])
            tempmaybe += list(all_segment_maybesets[sidi[_sample]]) #+list(all_segment_yessets[sidi[_sample]])
    #maybe_merged = changepointsmethods.merge_consec_changepoints_simpleboth(list(set(all_segment_maybeset_s1 + all_segment_yesset_s1)), list(set(all_segment_maybeset_s2 + all_segment_yesset_s2)), strand)


    #print(gene, "end", list(set(tempyes)), list(set(tempmaybe)))

    '''
    #print("part3", gene, segmentid, len(maybe_merged), len(yes_merged))
    filteredmaybe =
    filteredyes = []
    for site in maybe_merged:
        temp_count = 0
        rel_site = countreads.change_coord_to_rel(site, start, end, strand)
        for _sample in const.SAMPLES:
            temp_count += maybe_table_rc[_sample][rel_site]
            #print(site, rel_site, sample, temp_count)
        # # FIXME:
        if temp_count >= 0:
            filteredmaybe.append(site)
    maybe_merged = filteredmaybe

    for site in yes_merged:
        temp_count = 0
        rel_site = countreads.change_coord_to_rel(site, start, end, strand)
        for _sample in const.SAMPLES:
            temp_count += yes_table_rc[_sample][rel_site]
            #print(site, rel_site, sample, temp_count)
        # # FIXME:
        if temp_count >= 0:
            filteredyes.append(site)
    yes_merged = filteredyes
    #print("part3", gene, segmentid, len(maybe_merged), len(yes_merged))
    #print("5",gene, segmentid, maybe_merged, yes_merged)
    #print(segmentid, maybe_table_rc, yes_table_rc, yes_merged, maybe_merged)
    '''
    #return (segmentid, [], [], tempyes, tempmaybe, gene)
    return (segmentid, [], [], tempyes, tempmaybe, gene)
    #return (segmentid, maybe_table_rc, yes_table_rc, yes_merged, maybe_merged, gene)
