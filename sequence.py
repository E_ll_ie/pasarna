import re
import const
import pybedtools
from collections import defaultdict

###################################################################################################
###################################################################################################
###################################################################################################

def extract_coords_aroundsites(chrom, site, start, end,  strand, name):
    "This just makes bedtools entry"
    temp = ""
    keeptrack = []
    """
    Here we produce the genomic coords
    And we put them in a bed file
    """

    if strand == "+":
        real_site = start + site
    else:
        real_site = end - site
    keeptrack.append(site)
    temp = "\t".join([chrom, str(max(0,int(real_site)-200)), str(int(real_site)+200), \
                     name ,".", "+"])+"\n"
    return temp

###################################################################################################
###################################################################################################
###################################################################################################

def extract_seq_frombed(bedfiledir, genomefa):
    segmentsequencedict = {}
    """
    Im using bedtools to extract sequence too
    """
    tempbed = pybedtools.BedTool(bedfiledir, from_string=True)
    tempseqfile = tempbed.sequence(fi=genomefa, s=True, name=True, tab=True)
    #print(str(tempseq.seqfn))
    tempseq = open(tempseqfile.seqfn).read().splitlines()

    """
    We can look further into the sequence.
    """
    #print(len(bedfiledir), len(tempbed), len(tempseq))
    nametracks = []
    trackcounter = 0
    for thissiteind in range(len(tempseq)):
        #print(temp.splitlines()[thissiteind],tempseq[2*thissiteind+1])
        this_name = tempseq[thissiteind].split()[0]
        this_sequence = tempseq[thissiteind].split()[1]
        if this_name not in nametracks:
            nametracks.append(this_name)
        else:
            #print(this_name)
            trackcounter +=1
        #gene_name, segmentid, this_pos  = this_name.split("|")
        segmentsequencedict[this_name] = this_sequence
    return segmentsequencedict

###################################################################################################
###################################################################################################
###################################################################################################
'''
        for signal in ['ATTAAA', 'AATAAA']:
            locs = [m.start() for m in re.finditer(signal, tempseq[2*thissiteind+1][50:250])]
            if len(locs) > 0:
                notblackflag = True
                break
'''
###################################################################################################
###################################################################################################
###################################################################################################

def get_cnts(seq):
    c_mm = []
    seqs = [seq[0:100], seq[100:160], seq[160:200], seq[200:240], seq[240:300], seq[300:400]]
    for ind  in range(len(seqs)):
        for subs in const.motifs[ind][:]:
            locs = [m.start() for m in re.finditer(subs, seqs[ind])]
            c_mm.append(len(locs))
    return c_mm

###################################################################################################
###################################################################################################
###################################################################################################

def prepare_features(filtered_ones, merged_sites_info, covs, chrm, strand, gstart, startnt, exon_coords, lastexon):
    entries = []
    for site in filtered_ones:
        observed = []
        this_ratio = []
        for expr in covs:
            this_ratio += [sum(covs[expr][site-startnt-100: site-startnt])/max(1,sum(covs[expr][site-startnt:site-startnt+100]))]
            if expr in merged_sites_info[site]:
                observed+=[1]
            else:
                observed+=[0]
        if strand == "+":
            upsite = gstart + site
        else:
            upsite = gstart - site
        temp= "\t".join([chrm, str(max(0,int(upsite)-200)), str(int(upsite)+200), \
                                             chrm+strand+str(upsite),".", "+"])+"\n"
        tempbed = pybedtools.BedTool(temp, from_string=True)
        tempseqfile = tempbed.sequence(fi=genomefa, s=True)
        tempseq = open(tempseqfile.seqfn).read().splitlines()[1]
        seqfeats = get_cnts(tempseq)
        if site > exon_coords[0] and site < exon_coords[1] :
            exon = [1]
        else:
            exon = [0]


        this_entry = [site, site-startnt] + observed + this_ratio + exon + lastexon + seqfeats
        #print(this_entry)
        entries.append(this_entry)
    print(len(entries))
    return entries

###################################################################################################
###################################################################################################
###################################################################################################

def pair_up(sitesinfo):
    print("Im pairing up", len(sitesinfo))
    for ind1 in range(len(sitesinfo)):
        for ind2 in range(ind1+1, len(sitesinfo)):
            prox1 = sitesinfo[ind1][0]
            dist1 = sitesinfo[ind2][0]

            prox2 = sitesinfo[ind1][1]
            dist2 = sitesinfo[ind2][1]

            #print(prox1, dist1, prox2, dist2)

###################################################################################################
###################################################################################################
###################################################################################################

def extract_signals(seq):
    wing = 300
    pos_sig = defaultdict(list)
    for sig in const.PAS:
        pos_sig[sig] = [m.start() for m in re.finditer(sig, seq)]

    #print(pos_sig["AATAAA"], pos_sig["AATAAA"])
    mainSig = None
    otherSig = None
    mainSigWeak = None
    otherSigWeak = None
    for sig in const.PAS:
        if sig == "AATAAA" or sig == "ATTAAA":
            for pos in pos_sig[sig]:
                if mainSig is not None:
                    break
                if pos < wing-100 and pos > wing-200:
                    mainSig = pos
                    change = 0
                    break
                elif pos < wing+50 and pos > wing :
                    mainSig = pos - 50
                    change = -50
                elif pos < wing-100 and pos > wing-150 :
                    mainSig = pos + 50
                    change = 50
                elif pos < wing:
                    mainSigWeak = pos - wing
                    change  = -1*wing
                elif pos > wing:
                    mainSigWeak = pos + wing
                    change  = wing

        elif mainSig is None:
            for pos in pos_sig[sig]:
                if otherSig is not None:
                    break
                if pos < wing-100 and pos > wing-200:
                    otherSig = pos
                    change = 0
                    break
                if pos < wing+50 and pos > wing :
                    otherSig = pos - 50
                    change = -50
                if pos < wing-100 and pos > wing-150 :
                    otherSig = pos + 50
                    change = 50
                elif pos < wing:
                    otherSigWeak = pos - wing
                    change  = -1*wing
                elif pos > wing:
                    otherSigWeak = pos + wing
                    change  = wing

    if mainSig is None and otherSig is None:
        change = 0
    #return mainSig, otherSig
    return mainSig, otherSig, mainSigWeak, otherSigWeak, change
